module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      borderColor: ['active'],
      colors: {
        green: {
          150: '#BFD834',
          250: '#72CC50',
          350: '#019875',
          450: '#00AEAD',
          550: '#178542',
          650: '#56A039',
        },
        gray: {
          50: '#F2F3F5',
          75: '#E1E1E1',
          150: '#C4C4C4',
          225: '#B7BABF',
          250: '#BEBEBE',
          325: '#999999',
          350: '#666666',
          450: '#585859',
          550: '#363740',
          650: '#2E2E2E',
          750: '#9FA2B4',
          850: '#363740',
          950: '#DDE2FF',
        },
        white: '#FFFFFF',
        yellow: {
          550: '#F29200',
        },
        blue: {
          150: '#11B4E1',
        },
        cream: {
          100: '#FDFFF6',
        },
      },
    },
    zIndex: {
      '-100': '-100',
    },
    backgroundImage: {
      picture: "url('/img/landing/galeri-karya.png')",
    },
    screens: {
      sm: '410px',
      md: '960px',
      lg: '1024px',
      xl: '1280px',
      '2xl': '1536px',
    },
  },
};
