# Asakarya by Team B(erbalik)
Asakarya is a crowdfunding platform to support the creative industry sector in Indonesia.

## Starting out
Install required dependencies using
```
yarn
```

Run project using
```
yarn dev
```

## Collaborators
- [@andikacp](https://gitlab.com/andikacp)
- [@rejaaa](https://gitlab.com/rejaaa)
- [@urmlhst](https://gitlab.com/urmlhst)