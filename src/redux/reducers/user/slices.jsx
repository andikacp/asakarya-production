/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
import { createSlice } from '@reduxjs/toolkit';
import { useDispatch, useSelector } from 'react-redux';
import { sessionUser } from '@/helpers/auth';
import { baseApiUrl } from '@/helpers/baseContents';

const initialState = {
  addresses: [],
  mainAddress: {},
  userDetail: {},
  addressDetail: {},
};

const slices = createSlice({
  initialState,
  name: 'User',
  reducers: {
    setAddress(state, action) {
      Object.assign(state, {
        ...state,
        addresses: action.payload,
      });
    },
    setAddressDetail(state, action) {
      Object.assign(state, {
        ...state,
        addressDetail: action.payload,
      });
    },
    setMainAddress(state, action) {
      Object.assign(state, {
        ...state,
        mainAddress: action.payload,
      });
    },
    setUser(state, action) {
      Object.assign(state, {
        ...state,
        userDetail: action.payload,
      });
    },
  },
});

export const {
  setAddress,
  setUser,
  setAddressDetail,
  setMainAddress,
} = slices.actions;

export const useUserDispatch = () => {
  const ApiUrl = baseApiUrl;
  const { user } = useSelector((state) => state);
  const dispatch = useDispatch();

  const token = sessionUser();

  const getUserDetail = async (localToken) => {
    try {
      const response = await fetch(`${ApiUrl}/user/detail`, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${localToken}`,
        },
      });
      const data = await response.json();
      // console.log(data);
      const userDetailData = await data.data;
      dispatch(setUser(userDetailData));
      if (data.code === '200') {
        localStorage.setItem('imgUrl', JSON.stringify(userDetailData.imgUrl));
        localStorage.setItem('name', JSON.stringify(userDetailData.fullName));
        if (userDetailData.roles[1] === 'ROLE_CREATOR') {
          localStorage.setItem('roleCreator', true);
          localStorage.setItem('roles', JSON.stringify(userDetailData.roles[1]));
        } else {
          localStorage.setItem('roleCreator', false);
          localStorage.setItem('roles', JSON.stringify(userDetailData.roles[0]));
        }
      }
    } catch (error) {
      dispatch(setUser({}));
    }
  };

  const getAllAddress = async () => {
    try {
      const response = await fetch(`${ApiUrl}/address/all`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      });
      const data = await response.json();
      const addressList = await data.data;
      dispatch(setAddress(addressList));
    } catch (error) {
      dispatch(setAddress([]));
    }
  };

  const getAddressDetail = async (id) => {
    try {
      const response = await fetch(`${ApiUrl}/address/detail/${id}`, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      const data = await response.json();
      const addressDetailData = await data.data;
      dispatch(setAddressDetail(addressDetailData));
    } catch (error) {
      dispatch(setAddressDetail({}));
    }
  };

  const getMainAddress = async () => {
    try {
      const response = await fetch(`${ApiUrl}/address/main`, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      const data = await response.json();
      console.log(data);
      dispatch(setMainAddress(data.data));
    } catch (error) {
      console.log(error);
      dispatch(setMainAddress({}));
    }
  };

  return {
    user, getUserDetail, getAllAddress, getAddressDetail, getMainAddress,
  };
};

export default slices.reducer;
