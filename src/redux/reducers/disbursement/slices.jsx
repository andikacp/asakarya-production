import { createSlice } from '@reduxjs/toolkit';
import { useDispatch, useSelector } from 'react-redux';
import { baseApiUrl } from '@/helpers/baseContents';

const initialState = {
  disbursements: [],
  loading: false,
  totalPages: 0,
  page: 0,
};

const slices = createSlice({
  initialState,
  name: 'Disbursement',
  reducers: {

    setDisbursements(state, action) {
      Object.assign(state, {
        ...state,
        disbursements: action.payload,
      });
    },

    setTotalPages(state, action) {
      Object.assign(state, {
        ...state,
        totalPages: action.payload,
      });
    },

    setLoading(state, action) {
      Object.assign(state, {
        ...state,
        loading: action.payload,
      });
    },

    setPage(state, action) {
      Object.assign(state, {
        ...state,
        page: action.payload,
      });
    },

  },
});

export const {
  setDisbursements,
  setTotalPages,
  setLoading,
  setPage,
  setHistory,
} = slices.actions;

export const useDisbursementDispatch = () => {
  const ApiUrl = baseApiUrl;
  const { disbursement } = useSelector((state) => state);
  const dispatch = useDispatch();

  const fetchDisbursements = async (params) => {
    const {
      page = 0, size = 10, sortBy = 'updatedAt', campaignId, token,
    } = params;
    dispatch(setLoading(true));
    dispatch(setPage(page));
    try {
      const endpoint = `${ApiUrl}/disbursement/all?campaignId=${campaignId}&page=${page}&size=${size}&sortBy=${sortBy}&sortType=desc`;
      const response = await fetch(endpoint, {
        method: 'GET',
        headers: {
          authorization: `Bearer ${token}`,
        },
      });
      const data = await response.json();
      const { totalElements } = await data.data.pageable;
      const loadedDisbursements = await data.data.content;
      const totalPages = Math.ceil(totalElements / size);
      dispatch(setDisbursements(loadedDisbursements));
      dispatch(setTotalPages(totalPages));
    } catch (error) {
      dispatch(setDisbursements([]));
    }
    dispatch(setLoading(false));
  };

  return {
    disbursement,
    fetchDisbursements,
  };
};
export default slices.reducer;
