/* eslint-disable no-unused-vars */
import { createSlice } from '@reduxjs/toolkit';
import { useDispatch, useSelector } from 'react-redux';
import { baseApiUrl } from '@/helpers/baseContents';

const initialState = {
  loading: false,
  campaigns: [],
  campaignDetail: {},
  totalPages: 0,
  page: 0,
  categories: [],
  rewards: [],
  news: [],
  newsDetail: {},
  faq: [],
  faqDetail: {},
  monitoring: [],
  bookmarks: [],
};

const slices = createSlice({
  initialState,
  name: 'Campaign',
  reducers: {

    setCampaigns(state, action) {
      Object.assign(state, {
        ...state,
        campaigns: action.payload,
      });
    },

    setCampaignDetail(state, action) {
      Object.assign(state, {
        ...state,
        campaignDetail: action.payload,
      });
    },

    setTotalPages(state, action) {
      Object.assign(state, {
        ...state,
        totalPages: action.payload,
      });
    },

    setLoading(state, action) {
      Object.assign(state, {
        ...state,
        loading: action.payload,
      });
    },

    setPage(state, action) {
      Object.assign(state, {
        ...state,
        page: action.payload,
      });
    },

    setCategories(state, action) {
      Object.assign(state, {
        ...state,
        categories: action.payload,
      });
    },

    setRewards(state, action) {
      Object.assign(state, {
        ...state,
        rewards: action.payload,
      });
    },

    setNews(state, action) {
      Object.assign(state, {
        ...state,
        news: action.payload,
      });
    },

    setNewsDetail(state, action) {
      Object.assign(state, {
        ...state,
        newsDetail: action.payload,
      });
    },

    setFaq(state, action) {
      Object.assign(state, {
        ...state,
        faq: action.payload,
      });
    },

    setFaqDetail(state, action) {
      Object.assign(state, {
        ...state,
        faqDetail: action.payload,
      });
    },
    setMonitoring(state, action) {
      Object.assign(state, {
        ...state,
        monitoring: action.payload,
      });
    },
    setBookmarks(state, action) {
      Object.assign(state, {
        ...state,
        bookmarks: action.payload,
      });
    },

  },
});

export const {
  setCampaigns,
  setCampaignDetail,
  setTotalPages,
  setLoading,
  setPage,
  setCategories,
  setCategory,
  setQuery,
  setRewards,
  setNews,
  setNewsDetail,
  setFaq,
  setFaqDetail,
  setMonitoring,
  setBookmarks,
} = slices.actions;

export const useCampaignDispatch = () => {
  const ApiUrl = baseApiUrl;
  const { campaign } = useSelector((state) => state);
  const dispatch = useDispatch();

  const fetchCampaigns = async (params) => {
    const {
      ctg = '', page = 0, size = 6, query = '', sortBy = 'updatedAt', sortType = 'desc',
    } = params;
    dispatch(setLoading(true));
    dispatch(setPage(page));
    try {
      const endpoint = `${ApiUrl}/campaign/all?page=${page}&size=${size}&categoryId=${ctg}&title=${query}&sortBy=${sortBy}&sortType=${sortType}`;

      const response = await fetch(endpoint, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      });
      const data = await response.json();
      const { totalElements } = await data.data.pageable;
      const loadedCampaigns = await data.data.content;
      const totalPages = Math.ceil(totalElements / size);
      dispatch(setCampaigns(loadedCampaigns));
      dispatch(setTotalPages(totalPages));
    } catch (error) {
      dispatch(setCampaigns([]));
    }
    dispatch(setLoading(false));
  };

  const fetchCampaignDetail = async (id) => {
    dispatch(setLoading(true));
    try {
      const endpoint = `${ApiUrl}/campaign/detail/${id}`;

      const response = await fetch(endpoint, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      });
      const data = await response.json();
      const loadedCampaigns = await data.data;
      dispatch(setCampaignDetail(loadedCampaigns));
    } catch (error) {
      dispatch(setCampaignDetail({}));
    }
    dispatch(setLoading(false));
  };

  const fetchRewards = async (id) => {
    dispatch(setLoading(true));
    try {
      const endpoint = `${ApiUrl}/reward/all?campaignId=${id}`;
      const response = await fetch(endpoint, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      });
      const data = await response.json();
      const rewardsData = await data.data;
      dispatch(setRewards(rewardsData));
    } catch (error) {
      dispatch(setRewards({}));
    }
    dispatch(setLoading(false));
  };

  const fetchNews = async (params) => {
    const {
      campaignId, page = 0, size = 50,
    } = params;
    dispatch(setLoading(true));
    dispatch(setPage(page));
    try {
      const endpoint = `${ApiUrl}/history/all?campaignId=${campaignId}&page=${page}&size=${size}&sortBy=updatedAt&sortType=desc`;
      const response = await fetch(endpoint,
        {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
          },
        });
      const data = await response.json();
      const { totalElements } = await data.data.pageable;
      const newsData = await data.data.content;
      const totalPages = Math.ceil(totalElements / size);
      dispatch(setNews(newsData));
      dispatch(setTotalPages(totalPages));
    } catch (error) {
      dispatch(setNews([]));
    }
  };

  const fetchNewsDetail = async (id) => {
    dispatch(setLoading(true));
    try {
      const endpoint = `${ApiUrl}/history/detail/${id}`;

      const response = await fetch(endpoint, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      });
      const data = await response.json();
      const newsData = await data.data;
      dispatch(setNewsDetail(newsData));
    } catch (error) {
      dispatch(setNewsDetail({}));
    }
    dispatch(setLoading(false));
  };

  const fetchFaq = async (id) => {
    dispatch(setLoading(true));
    try {
      const endpoint = `${ApiUrl}/faq/all?campaignId=${id}`;

      const response = await fetch(endpoint, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      });
      const data = await response.json();
      const faqData = await data.data;
      dispatch(setFaq(faqData));
    } catch (error) {
      dispatch(setFaq([]));
    }
    dispatch(setLoading(false));
  };

  const fetchFaqDetail = async (id) => {
    dispatch(setLoading(true));
    try {
      const endpoint = `${ApiUrl}/faq/detail/${id}`;

      const response = await fetch(endpoint, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      });
      const data = await response.json();
      const faqData = await data.data;
      dispatch(setFaqDetail(faqData));
    } catch (error) {
      dispatch(setFaqDetail({}));
    }
    dispatch(setLoading(false));
  };

  const fetchCategories = async () => {
    setLoading(true);
    try {
      const response = await fetch(`${ApiUrl}/category/all`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      });
      const data = await response.json();
      const appendedCategories = await data.data;
      const categoryList = [
        {
          id: '',
          categoryName: 'Semua',
        },
        ...appendedCategories,
      ];

      dispatch(setCategories(categoryList));
    } catch (error) {
      dispatch(setCategories([]));
    }
    setLoading(false);
  };

  const fetchMonitoringCampaigns = async (params) => {
    const {
      token, page = 0, size = 3, sortBy = 'updatedAt',
    } = params;
    dispatch(setLoading(true));
    dispatch(setPage(page));
    try {
      const endpoint = `${ApiUrl}/campaign/creator/monitoring?page=${page}&size=${size}&sortBy=${sortBy}&sortType=desc`;
      const response = await fetch(endpoint, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      });
      const data = await response.json();
      const { totalElements } = await data.data.pageable;
      const loadedCampaigns = await data.data.content;
      const totalPages = Math.ceil(totalElements / size);
      dispatch(setCampaigns(loadedCampaigns));
      dispatch(setTotalPages(totalPages));
    } catch (error) {
      dispatch(setCampaigns([]));
    }
    dispatch(setLoading(false));
  };

  const fetchBookmarks = async (params) => {
    const {
      token, page = 0, size = 9, sortBy = 'updatedAt',
    } = params;
    dispatch(setLoading(true));
    dispatch(setPage(page));
    try {
      const endpoint = `${ApiUrl}/bookmark/all?page=${page}&size=${size}&sortBy=${sortBy}&sortType=desc`;

      const response = await fetch(endpoint, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      });
      const data = await response.json();
      const { totalElements } = await data.data.pageable;
      const loadedCampaigns = await data.data.content;
      const totalPages = Math.ceil(totalElements / size);
      dispatch(setBookmarks(loadedCampaigns));
      dispatch(setTotalPages(totalPages));
    } catch (error) {
      dispatch(setBookmarks([]));
    }
    dispatch(setLoading(false));
  };

  return {
    campaign,
    fetchCampaigns,
    fetchCategories,
    fetchCampaignDetail,
    fetchRewards,
    fetchNews,
    fetchNewsDetail,
    fetchFaq,
    fetchFaqDetail,
    fetchMonitoringCampaigns,
    fetchBookmarks,
  };
};
export default slices.reducer;
