/* eslint-disable camelcase */
import { createSlice } from '@reduxjs/toolkit';
import { useDispatch, useSelector } from 'react-redux';
import { sessionUser } from '@/helpers/auth';
import { baseApiUrl } from '@/helpers/baseContents';

const initialState = {
  donations: {},
};

const slices = createSlice({
  initialState,
  name: 'Payment',
  reducers: {
    setDonation(state, action) {
      Object.assign(state, {
        ...state,
        donations: action.payload,
      });
    },
  },
});

export const { updatePayment, setDonation } = slices.actions;

export const usePaymentDispatch = () => {
  const { payment } = useSelector((state) => state);
  const dispatch = useDispatch();

  const token = sessionUser();

  const getDonations = async (id) => {
    if (token) {
      try {
        const response = await fetch(`${baseApiUrl}/donation/detail/${id}`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        });
        const data = await response.json();
        const dataDonation = await data.data;
        dispatch(setDonation(dataDonation));
      } catch (error) {
        dispatch(setDonation({}));
      }
    } else {
      try {
        const response = await fetch(`${baseApiUrl}/donation/detail/${id}`, {
          method: 'GET',
        });
        const data = await response.json();
        const dataDonation = await data.data;
        dispatch(setDonation(dataDonation));
      } catch (error) {
        dispatch(setDonation({}));
      }
    }
  };

  return {
    payment,
    getDonations,
  };
};

export default slices.reducer;
