import { createSlice } from '@reduxjs/toolkit';
import { useDispatch, useSelector } from 'react-redux';
import { baseApiUrl } from '@/helpers/baseContents';

const initialState = {
  donations: [],
  loading: false,
  totalPages: 0,
  page: 0,
  history: [],
  donationRewards: [],
};

const slices = createSlice({
  initialState,
  name: 'Donation',
  reducers: {

    setDonations(state, action) {
      Object.assign(state, {
        ...state,
        donations: action.payload,
      });
    },

    setTotalPages(state, action) {
      Object.assign(state, {
        ...state,
        totalPages: action.payload,
      });
    },

    setLoading(state, action) {
      Object.assign(state, {
        ...state,
        loading: action.payload,
      });
    },

    setPage(state, action) {
      Object.assign(state, {
        ...state,
        page: action.payload,
      });
    },

    setHistory(state, action) {
      Object.assign(state, {
        ...state,
        history: action.payload,
      });
    },

    setDonationRewards(state, action) {
      Object.assign(state, {
        ...state,
        donationRewards: action.payload,
      });
    },
  },
});

export const {
  setDonations,
  setTotalPages,
  setLoading,
  setPage,
  setHistory,
  setDonationRewards,
} = slices.actions;

export const useDonationDispatch = () => {
  const ApiUrl = baseApiUrl;
  const { donation } = useSelector((state) => state);
  const dispatch = useDispatch();

  const fetchDonation = async (params) => {
    const {
      page = 0, size = 10, sortBy = 'updatedAt', campaignId,
    } = params;
    dispatch(setLoading(true));
    dispatch(setPage(page));
    try {
      const endpoint = `${ApiUrl}/donation/all?campaignId=${campaignId}&page=${page}&size=${size}&sortBy=${sortBy}&sortType=desc`;
      const response = await fetch(endpoint, {
        method: 'GET',
      });
      const data = await response.json();
      const { totalElements } = await data.data.pageable;
      const loadedDonations = await data.data.content;
      const totalPages = Math.ceil(totalElements / size);
      dispatch(setDonations(loadedDonations));
      dispatch(setTotalPages(totalPages));
    } catch (error) {
      dispatch(setDonations([]));
    }
    dispatch(setLoading(false));
  };

  const fetchHistory = async (params) => {
    const {
      token, page = 0, size = 10, sortBy = 'updatedAt',
    } = params;
    dispatch(setLoading(true));
    dispatch(setPage(page));
    try {
      const endpoint = `${ApiUrl}/donation/all/user?page=${page}&size=${size}&sortBy=${sortBy}&sortType=desc`;

      const response = await fetch(endpoint, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      });
      const data = await response.json();
      const { totalElements } = await data.data.pageable;
      const loadedDonations = await data.data.content;
      const totalPages = Math.ceil(totalElements / size);
      dispatch(setHistory(loadedDonations));
      dispatch(setTotalPages(totalPages));
    } catch (error) {
      dispatch(setHistory([]));
    }
    dispatch(setLoading(false));
  };

  const fetchDonationReward = async (params) => {
    const {
      token, campaignId,
    } = params;
    dispatch(setLoading(true));
    try {
      const endpoint = `${ApiUrl}/donation-reward/all?campaignId=${campaignId}`;
      const response = await fetch(endpoint, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      const data = await response.json();
      const loadedDonations = await data.data.content;
      dispatch(setDonationRewards(loadedDonations));
    } catch (error) {
      dispatch(setDonationRewards([]));
    }
    dispatch(setLoading(false));
  };

  return {
    donation,
    fetchDonation,
    fetchHistory,
    fetchDonationReward,
  };
};
export default slices.reducer;
