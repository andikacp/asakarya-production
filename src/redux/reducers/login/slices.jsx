/* eslint-disable camelcase */
import { createSlice } from '@reduxjs/toolkit';
import { useDispatch, useSelector } from 'react-redux';
// import { useRouter } from 'next/router';
import { baseApiUrl } from '@/helpers/baseContents';

const initialState = { // peristiwa yang selalu berubah (state yang berubah)
  // popup, behavior, hide/show, loading, filter,
  loading: false,
  success: false,
};

const slices = createSlice({
  initialState,
  name: 'Login',
  reducers: {
    toggleLoading(state, action) {
      Object.assign(state, {
        loading: action.payload,
      });
    },
    toggleSuccess(state, action) {
      Object.assign(state, {
        ...state,
        success: action.payload,
      });
    },
  },
});

const { toggleLoading, toggleSuccess } = slices.actions;

export const useLoginDispatch = () => {
  const { login } = useSelector((state) => state);
  const dispatch = useDispatch();
  // const router = useRouter();

  const doLogin = async (values) => {
    try {
      dispatch(toggleLoading(true));
      dispatch(toggleSuccess(false));
      const response = await fetch(`${baseApiUrl}/user/login`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
        body: JSON.stringify(values),
      });
      const data = await response.json();
      // console.log(data);
      if (data.code === '200') {
        localStorage.setItem('token', JSON.stringify(data.data));
        dispatch(toggleSuccess(false));
        dispatch(toggleLoading(false));
      } else if (data.code === '400') {
        dispatch(toggleSuccess(true));
        dispatch(toggleLoading(false));
      }
    } catch (error) {
      // console.log(error);
      dispatch(toggleLoading(false));
      dispatch(toggleSuccess(true));
    }
  };

  return {
    login,
    doLogin,
  };
};

export default slices.reducer;
