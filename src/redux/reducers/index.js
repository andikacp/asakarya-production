import { combineReducers } from 'redux';
import login from './login/slices';
import campaign from './campaign/slices';
import payment from './payment/slices';
import user from './user/slices';
import donation from './donation/slices';
import disbursement from './disbursement/slices';
import admin from './admin/slices';
import creator from './creator/slices';
// masukin import list reducers di sini, tulis reducer yang dipake di dalem fungsi combineReducers

const rootReducers = combineReducers({
  login,
  campaign,
  payment,
  user,
  donation,
  disbursement,
  admin,
  creator,
});

export default rootReducers;
