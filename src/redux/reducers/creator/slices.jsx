import { createSlice } from '@reduxjs/toolkit';
import { useDispatch, useSelector } from 'react-redux';
import { baseApiUrl } from '@/helpers/baseContents';
import { sessionUser } from '@/helpers/auth';

const initialState = {
  rewardForm: {},
  creatorDetail: {},
};

const slices = createSlice({
  initialState,
  name: 'Creator',
  reducers: {
    setRewardForm(state, action) {
      Object.assign(state, {
        ...state,
        rewardForm: action.payload,
      });
    },
    setCreatorDetail(state, action) {
      Object.assign(state, {
        ...state,
        creatorDetail: action.payload,
      });
    },
  },
});

export const { setRewardForm, setCreatorDetail } = slices.actions;

export const useCreatorDispatch = () => {
  const { creator } = useSelector((state) => state);
  const dispatch = useDispatch();

  const doSetRewardForm = (values) => {
    dispatch(setRewardForm(values));
  };

  const token = sessionUser();

  const getCreatorDetail = async () => {
    try {
      const response = await fetch(`${baseApiUrl}/creator/detail`, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      const data = await response.json();
      dispatch(setCreatorDetail(data.data));
      console.log(data);
    } catch (error) {
      console.log(error);
      dispatch(setCreatorDetail({}));
    }
  };

  return {
    creator,
    doSetRewardForm,
    getCreatorDetail,
  };
};

export default slices.reducer;
