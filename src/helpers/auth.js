export const sessionUser = () => { if (typeof window !== 'undefined') return JSON.parse(localStorage.getItem('token')); return null; };
export const profileImage = () => { if (typeof window !== 'undefined') return JSON.parse(localStorage.getItem('imgUrl')); return null; };
export const roles = () => { if (typeof window !== 'undefined') return JSON.parse(localStorage.getItem('roles')); return null; };
export const name = () => { if (typeof window !== 'undefined') return JSON.parse(localStorage.getItem('name')); return null; };
export const logout = () => { if (typeof window !== 'undefined') localStorage.clear(); };
