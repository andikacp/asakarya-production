import Head from 'next/head';
import CampaignContainer from '@/containers/campaign/List';
import MainLayout from '@/layouts/main';

export default function CampaignPage() {
  return (
    <>
      <Head>
        <title>Campaign</title>
      </Head>
      <MainLayout>
        <CampaignContainer />
      </MainLayout>
    </>
  );
}
