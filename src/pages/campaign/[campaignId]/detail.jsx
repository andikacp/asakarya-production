import Head from 'next/head';
import CampaignDetailLayout from '@/layouts/campaignDetail';
import CampaignInfoDetailContainer from '@/containers/campaign/InfoDetail';

export default function detailPage() {
  return (
    <>
      <Head>
        <title>Detail</title>
      </Head>
      <CampaignDetailLayout>
        <CampaignInfoDetailContainer />
      </CampaignDetailLayout>
    </>
  );
}
