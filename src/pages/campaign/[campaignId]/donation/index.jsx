import Head from 'next/head';
import DonationDetailContainer from '@/containers/donationDetail';
import MainLayout from '@/layouts/main';

export default function detailDonationPage() {
  return (
    <>
      <Head>
        <title>Detail Donasi</title>
      </Head>
      <MainLayout>
        <DonationDetailContainer />
      </MainLayout>
    </>
  );
}
