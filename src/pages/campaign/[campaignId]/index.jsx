import Head from 'next/head';
import CampaignDetailLayout from '@/layouts/campaignDetail';

export default function detailPage() {
  return (
    <>
      <Head>
        <title>Detail</title>
      </Head>
      <CampaignDetailLayout />
    </>
  );
}
