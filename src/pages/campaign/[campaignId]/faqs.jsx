import Head from 'next/head';
import CampaignDetailLayout from '@/layouts/campaignDetail';
import CampaignFAQShowContainer from '@/containers/campaign/FAQShow';

export default function detailPage() {
  return (
    <>
      <Head>
        <title>FAQs</title>
      </Head>
      <CampaignDetailLayout>
        <CampaignFAQShowContainer />
      </CampaignDetailLayout>
    </>
  );
}
