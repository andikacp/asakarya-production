import Head from 'next/head';
import GalangDanaContainer from '@/containers/galangDana';
import MainLayout from '@/layouts/main';

export default function GalangDanaPage() {
  return (
    <>
      <Head>
        <title>Galang Dana</title>
      </Head>
      <MainLayout>
        <GalangDanaContainer />
      </MainLayout>
    </>
  );
}
