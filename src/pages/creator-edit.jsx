import Head from 'next/head';
import FormGalangDanaContainer from '@/containers/formGalangDana';
import MainLayout from '@/layouts/main';
import AuthLayout from '@/layouts/auth';
import AccountLayout from '@/layouts/account';
import CreatorAuthLayout from '@/layouts/authCreator';

export default function InfoKreatorPage() {
  return (
    <>
      <CreatorAuthLayout>
        <Head>
          <title>Info Kreator</title>
        </Head>
        <MainLayout>
          <AccountLayout parent="profil">
            <AuthLayout>
              <FormGalangDanaContainer />
            </AuthLayout>
          </AccountLayout>
        </MainLayout>
      </CreatorAuthLayout>

    </>
  );
}
