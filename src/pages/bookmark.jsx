import Head from 'next/head';
import BookmarkCampaignContainer from '@/containers/bookmark';
import NavbarProfile from '@/components/NavbarProfile';
import MainLayout from '@/layouts/main';
import AuthLayout from '@/layouts/auth';

export default function BookmarkCampaignPage() {
  return (
    <>
      <Head>
        <title>Campaign Tersimpan</title>
      </Head>
      <MainLayout>
        <NavbarProfile />
        <AuthLayout>
          <BookmarkCampaignContainer />
        </AuthLayout>
      </MainLayout>
    </>
  );
}
