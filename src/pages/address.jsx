import Head from 'next/head';
import AddressListContainer from '@/containers/addressList/libraries/AddressListContainer';
import MainLayout from '@/layouts/main';
import AccountLayout from '@/layouts/account';
import AuthLayout from '@/layouts/auth';

export default function AddressListPage() {
  return (
    <>
      <Head>
        <title>Alamat</title>
      </Head>
      <MainLayout>
        <AccountLayout parent="profil">
          <AuthLayout>
            <AddressListContainer />
          </AuthLayout>
        </AccountLayout>
      </MainLayout>
    </>
  );
}
