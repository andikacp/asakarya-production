import Head from 'next/head';
import RegisterContainer from '@/containers/registration/libraries/RegisterContainer';
import Background1 from '@/components/Background/libraries/Background-1';
import MainLayout from '@/layouts/main';

export default function RegisterPage() {
  return (
    <MainLayout>
      <Head>
        <title>Daftar</title>
      </Head>
      <RegisterContainer />
      <Background1 />
    </MainLayout>
  );
}
