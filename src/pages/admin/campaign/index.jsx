import Head from 'next/head';
import AdminCampaignContainer from '@/containers/admin/campaign/list';
import AdminLayout from '@/layouts/admin';
import AdminAuthLayout from '@/layouts/authAdmin';

export default function AdminCampaignPage() {
  return (
    <>
      <AdminAuthLayout>
        <Head>
          <title>Campaign</title>
        </Head>
        <AdminLayout>
          <AdminCampaignContainer />
        </AdminLayout>
      </AdminAuthLayout>
    </>
  );
}
