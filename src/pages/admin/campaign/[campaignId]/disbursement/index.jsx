import Head from 'next/head';
import AdminCampaignLayout from '@/layouts/adminCampaign';
import AdminDisbursementListContainer from '@/containers/admin/campaign/disbursementList';
import AdminAuthLayout from '@/layouts/authAdmin';

export default function AdminDisbursementListPage() {
  return (
    <>
      <AdminAuthLayout>
        <Head>
          <title>Informasi Dana Terkirim</title>
        </Head>
        <AdminCampaignLayout>
          <AdminDisbursementListContainer />
        </AdminCampaignLayout>
      </AdminAuthLayout>
    </>

  );
}
