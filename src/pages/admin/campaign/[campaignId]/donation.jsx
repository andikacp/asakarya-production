import Head from 'next/head';
import AdminCampaignLayout from '@/layouts/adminCampaign';
import AdminCampaignDonationContainer from '@/containers/admin/campaign/donation';
import AdminAuthLayout from '@/layouts/authAdmin';

export default function AdminCampaignDonationPage() {
  return (
    <>
      <AdminAuthLayout>
        <Head>
          <title>Informasi Donasi</title>
        </Head>
        <AdminCampaignLayout>
          <AdminCampaignDonationContainer />
        </AdminCampaignLayout>
      </AdminAuthLayout>
    </>

  );
}
