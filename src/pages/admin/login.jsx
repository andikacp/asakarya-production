import Head from 'next/head';
import AdminLoginContainer from '@/containers/admin/login';
// import AuthLayout from '@/layouts/auth';

export default function AdminLoginPage() {
  return (
    <>
      <Head>
        <title>Masuk</title>
      </Head>
      <div className="h-screen relative">
        <AdminLoginContainer />
      </div>
      <div className="-z-100 bg-gray-550 inset-0 absolute">
        <img src="/img/background/leaf-vector-right.png" alt="leaf-right" className="float-right mt-40" />
        <img src="/img/background/leaf-vector-left.png" alt="leaf-left" className="top-9 mt-80" />
      </div>
    </>

  );
}
