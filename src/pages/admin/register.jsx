import Head from 'next/head';
import AdminRegisterContainer from '@/containers/admin/registration';

export default function AdminRegisterPage() {
  return (
    <>
      <Head>
        <title>Daftar</title>
      </Head>
      <div className="h-screen relative overflow-y-auto">
        <AdminRegisterContainer />
      </div>
      <div className="-z-100 bg-gray-550 inset-0 absolute">
        <img src="/img/background/leaf-vector-right.png" alt="leaf-right" className="float-right mt-40" />
        <img src="/img/background/leaf-vector-left.png" alt="leaf-left" className="top-9 mt-80" />
      </div>

    </>

  );
}
