import Head from 'next/head';
import AdminDashboardContainer from '@/containers/admin/dashboard';
import AdminLayout from '@/layouts/admin';
import AdminAuthLayout from '@/layouts/authAdmin';

export default function AdminDashboardPage() {
  return (
    <>
      <AdminAuthLayout>
        <Head>
          <title>Dashboard</title>
        </Head>
        <AdminLayout>
          <AdminDashboardContainer />
        </AdminLayout>
      </AdminAuthLayout>

    </>
  );
}
