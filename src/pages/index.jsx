import Head from 'next/head';
import LandingContainer from '@/containers/landing';
import MainLayout from '@/layouts/main';

export default function LandingPage() {
  return (
    <>
      <Head>
        <title>Landing Page</title>
      </Head>
      <MainLayout>
        <LandingContainer />
      </MainLayout>
    </>
  );
}
