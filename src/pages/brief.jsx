import Head from 'next/head';
import BriefGalangDanaContainer from '@/containers/briefGalangDana';
import MainLayout from '@/layouts/main';
import AuthLayout from '@/layouts/auth';
import NavbarProfile from '@/components/NavbarProfile';

export default function BriefGalangDanaPage() {
  return (
    <>
      <Head>
        <title>Brief Galang Dana</title>
      </Head>
      <MainLayout>
        <NavbarProfile />
        <AuthLayout>
          <BriefGalangDanaContainer />
        </AuthLayout>
      </MainLayout>
    </>
  );
}
