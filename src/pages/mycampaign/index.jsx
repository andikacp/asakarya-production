import Head from 'next/head';
import GalangDanaContainer from '@/containers/galangDana';
import MainLayout from '@/layouts/main';
import NavbarProfile from '@/components/NavbarProfile';
import CreatorAuthLayout from '@/layouts/authCreator';

export default function GalangDanaPage() {
  return (
    <>
      <CreatorAuthLayout>
        <Head>
          <title>Campaign Saya</title>
        </Head>
        <MainLayout>
          <NavbarProfile />
          <GalangDanaContainer />
        </MainLayout>
      </CreatorAuthLayout>
    </>
  );
}
