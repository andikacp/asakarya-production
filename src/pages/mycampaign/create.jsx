import Head from 'next/head';
import MainLayout from '@/layouts/main';
import AccountLayout from '@/layouts/account';
import CreateCampaignContainer from '@/containers/campaign/Create';
import CreatorAuthLayout from '@/layouts/authCreator';

export default function CreateCampaignPage() {
  return (
    <>
      <CreatorAuthLayout>
        <Head>
          <title>Buat Campaign</title>
        </Head>
        <MainLayout>
          <AccountLayout parent="createCampaign">
            <CreateCampaignContainer />
          </AccountLayout>
        </MainLayout>
      </CreatorAuthLayout>

    </>
  );
}
