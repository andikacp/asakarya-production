import Head from 'next/head';
import MainLayout from '@/layouts/main';
import AccountLayout from '@/layouts/account';
import CampaignNewsContainer from '@/containers/campaign/News';
import CreatorAuthLayout from '@/layouts/authCreator';

export default function CampaignNewsPage() {
  return (
    <>
      <CreatorAuthLayout>
        <Head>
          <title>Kabar Terbaru</title>
        </Head>
        <MainLayout>
          <AccountLayout parent="createCampaign">
            <CampaignNewsContainer />
          </AccountLayout>
        </MainLayout>
      </CreatorAuthLayout>

    </>
  );
}
