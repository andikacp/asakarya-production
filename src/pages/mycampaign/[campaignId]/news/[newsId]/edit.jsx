import Head from 'next/head';
import MainLayout from '@/layouts/main';
import AccountLayout from '@/layouts/account';
import EditCampaignNewsContainer from '@/containers/campaign/NewsEdit';
import CreatorAuthLayout from '@/layouts/authCreator';

export default function CreateCampaignNewsPage() {
  return (
    <>
      <CreatorAuthLayout>
        <Head>
          <title>Edit Kabar Terbaru</title>
        </Head>
        <MainLayout>
          <AccountLayout parent="createCampaign">
            <EditCampaignNewsContainer />
          </AccountLayout>
        </MainLayout>
      </CreatorAuthLayout>

    </>
  );
}
