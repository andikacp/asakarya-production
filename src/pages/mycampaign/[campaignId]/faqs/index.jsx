import Head from 'next/head';
import MainLayout from '@/layouts/main';
import AccountLayout from '@/layouts/account';
import CampaignFaqContainer from '@/containers/campaign/FAQ';
import CreatorAuthLayout from '@/layouts/authCreator';

export default function CampaignNewsPage() {
  return (
    <>
      <CreatorAuthLayout>
        <Head>
          <title>Faqs</title>
        </Head>
        <MainLayout>
          <AccountLayout parent="createCampaign">
            <CampaignFaqContainer />
          </AccountLayout>
        </MainLayout>
      </CreatorAuthLayout>

    </>
  );
}
