import Head from 'next/head';
import MainLayout from '@/layouts/main';
import AccountLayout from '@/layouts/account';
import EditCampaignFAQContainer from '@/containers/campaign/FAQEdit';
import CreatorAuthLayout from '@/layouts/authCreator';

export default function CreateCampaignNewsPage() {
  return (
    <>
      <CreatorAuthLayout>
        <Head>
          <title>Edit FAQ</title>
        </Head>
        <MainLayout>
          <AccountLayout parent="createCampaign">
            <EditCampaignFAQContainer />
          </AccountLayout>
        </MainLayout>
      </CreatorAuthLayout>

    </>
  );
}
