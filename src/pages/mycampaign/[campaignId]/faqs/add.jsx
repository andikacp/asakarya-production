import Head from 'next/head';
import MainLayout from '@/layouts/main';
import AccountLayout from '@/layouts/account';
import CreateCampaignFAQContainer from '@/containers/campaign/FAQCreate';
import CreatorAuthLayout from '@/layouts/authCreator';

export default function CreateCampaignNewsPage() {
  return (
    <>
      <CreatorAuthLayout>
        <Head>
          <title>Tambah FAQ</title>
        </Head>
        <MainLayout>
          <AccountLayout parent="createCampaign">
            <CreateCampaignFAQContainer />
          </AccountLayout>
        </MainLayout>
      </CreatorAuthLayout>

    </>
  );
}
