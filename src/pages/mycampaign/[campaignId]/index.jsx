import Head from 'next/head';
import CampaignMonitorContainer from '@/containers/campaign/Monitor';
import MainLayout from '@/layouts/main/';
import CreatorAuthLayout from '@/layouts/authCreator';

export default function detailPage() {
  return (
    <>
      <CreatorAuthLayout>
        <Head>
          <title>Monitoring</title>
        </Head>
        <MainLayout>
          <CampaignMonitorContainer />
        </MainLayout>
      </CreatorAuthLayout>
    </>
  );
}
