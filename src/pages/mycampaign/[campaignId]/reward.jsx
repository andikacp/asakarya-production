import Head from 'next/head';
import CampaignMonitorRewardContainer from '@/containers/campaign/MonitorReward';
import MonitorLayout from '@/layouts/monitor';
import CreatorAuthLayout from '@/layouts/authCreator';

export default function RewardMonitorPage() {
  return (
    <>
      <CreatorAuthLayout>
        <Head>
          <title>Monitor Hadiah</title>
        </Head>

        <MonitorLayout>
          <CampaignMonitorRewardContainer />
        </MonitorLayout>
      </CreatorAuthLayout>
    </>
  );
}
