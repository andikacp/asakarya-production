import Head from 'next/head';
import MainLayout from '@/layouts/main';
import AccountLayout from '@/layouts/account';
import EditCampaignContainer from '@/containers/campaign/Edit';
import CreatorAuthLayout from '@/layouts/authCreator';

export default function CreateCampaignPage() {
  return (
    <>
      <CreatorAuthLayout>
        <Head>
          <title>Edit Campaign</title>
        </Head>
        <MainLayout>
          <AccountLayout parent="createCampaign">
            <EditCampaignContainer />
          </AccountLayout>
        </MainLayout>
      </CreatorAuthLayout>
    </>
  );
}
