import Head from 'next/head';
import CampaignMonitorIncomeContainer from '@/containers/campaign/MonitorIncome';
import MonitorLayout from '@/layouts/monitor';
import CreatorAuthLayout from '@/layouts/authCreator';

export default function IncomeMonitorPage() {
  return (
    <>
      <CreatorAuthLayout>
        <Head>
          <title>Monitor Income</title>
        </Head>
        <MonitorLayout>
          <CampaignMonitorIncomeContainer />
        </MonitorLayout>
      </CreatorAuthLayout>

    </>
  );
}
