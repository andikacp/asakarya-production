import Head from 'next/head';
import AboutContainer from '@/containers/about';
import MainLayout from '@/layouts/main';

export default function AboutUsPage() {
  return (
    <>
      <Head>
        <title>About Us</title>
      </Head>
      <MainLayout>
        <AboutContainer />
      </MainLayout>
    </>
  );
}
