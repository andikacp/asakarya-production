/* eslint-disable max-len */
/* eslint-disable no-console */

import Image from 'next/image';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
// import Link from 'next/link';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { useCampaignDispatch } from '@/redux/reducers/campaign/slices';
import { useUserDispatch } from '@/redux/reducers/user/slices';
import Reward from '@/components/Reward';
import LinkButton from '@/components/LinkButton';
import { sessionUser } from '@/helpers/auth.js';
import { baseApiUrl, baseReward } from '@/helpers/baseContents';
import AddressModal from '@/components/PopupAddress';
import AddressEditModal from '@/components/PopupAddressEdit';
import Breadcrumb from '@/components/Breadcrumb';
import EmptyImage from '@/components/EmptyImage';

export default function DonationDetailContainer() {
  const url = `${baseApiUrl}/donation/add`;
  const [loading, setLoading] = useState(false);
  const token = sessionUser();
  const router = useRouter();
  const { campaignId } = router.query;
  // const { campaignId, donationId } = router.query;

  const initialValues = {
    amount: 0,
    notes: '',
    campaignId,
  };

  const validationSchema = Yup.object({
    amount: Yup.number().required('This must be filled.').moreThan(9999, 'Nominal donasi minimal Rp 10.000'),
  });

  const {
    fetchRewards, fetchCampaignDetail, fetchCampaigns, campaign,
  } = useCampaignDispatch();
  const { rewards, campaignDetail } = campaign;

  // const [tokenState, setTokenState] = useState();
  // useEffect(() => {
  //   getMainAddress();
  // }, [tokenState]);

  // useEffect(() => {
  //   setTokenState({ token });
  // }, []);

  useEffect(() => {
    fetchCampaigns({ ctg: campaignDetail.categoryId, sortBy: 'due' });
  }, [campaignDetail]);

  useEffect(() => {
    if (campaignId) {
      fetchRewards(campaignId);
      fetchCampaignDetail(campaignId);
    }
  }, [campaignId]);

  useEffect(() => {
    if (campaignId) {
      fetchRewards(campaignId);
    }
  }, [campaignId]);

  const renderRewards = () => rewards && rewards.map((item) => (
    <Reward {...item} key={`reward-${item.id}`} />
  ));

  const { getMainAddress, user } = useUserDispatch();
  const { mainAddress } = user;

  useEffect(() => {
    if (token) {
      getMainAddress();
      // console.log(mainAddress);
    }
  }, []);

  const submitDonation = async (values) => {
    const {
      amount, notes,
    } = values;

    const formDataLogin = {
      amount,
      notes,
      campaignId,
      addressId: mainAddress.id,
    };

    const formData = {
      amount,
      notes,
      campaignId,
    };
    if (token) {
      try {
        const response = await fetch(url, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify(formDataLogin),
        });
        console.log(formDataLogin);
        const data = await response.json();
        const donationId = data.id;
        // console.log(donationId);
        router.push(`/campaign/${campaignId}/donation/${donationId}/payment`);
      } catch (error) {
        console.log(error);
      }
    } else {
      try {
        const response = await fetch(url, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(formData),
        });
        const data = await response.json();
        const donationId = data.id;
        // console.log(donationId);
        console.log(formData);
        router.push(`/campaign/${campaignId}/donation/${donationId}/payment`);
      } catch (error) {
        console.log(error);
      }
    }
  };

  const handleOnSubmit = async (values) => {
    setLoading(true);
    // console.log(values);
    await submitDonation(values);
    setLoading(false);
  };

  const pages = [
    { name: 'Campaign', href: '/campaign', current: false },
    { name: campaignDetail.title, href: `/campaign/${campaignId}/detail`, current: false },
    { name: 'Detail Donasi', href: '#', current: true },
  ];

  return (

    <div className="container mx-auto flex flex-col gap-5 p-12">
      <div className="-ml-4">
        <Breadcrumb pages={pages} />
      </div>
      <h1 className="text-4xl text-green-550 font-semibold text-center">
        Detail Donasi
      </h1>

      <Formik
        onSubmit={handleOnSubmit}
        initialValues={initialValues}
        enableReinitialize
        validationSchema={validationSchema}
      >
        {({
          handleSubmit,
          handleChange,
          handleBlur,
          touched,
          errors,
        }) => (
          <Form onSubmit={handleSubmit}>

            <div className="border border-gray-150 flex justify-center h-auto w-auto">
              <div className="grid grid-cols-2 gap-12">
                <div className="col-span-1 flex justify-start items-center ">
                  {campaignDetail.imgUrl ? <Image className="object-cover" src={campaignDetail.imgUrl} width="695" height="413" /> : <EmptyImage w="695" h="413" source="default" />}
                </div>
                <div className="col-span-1">
                  <div className="text-xl font-semibold my-4">Nominal Donasi</div>
                  <div className={`px-3 focus:outline-none ${touched.amount && errors.amount ? 'border-red-700 border' : 'border-none'} flex justify-between bg-gray-50 rounded-sm px-3 w-11/12 `}>
                    <span className="font-semibold my-4 ml-6">Rp</span>
                    <input onChange={handleChange} onBlur={handleBlur} type="number" min="10000" className="bg-gray-50 w-full px-3 focus:outline-none" name="amount" id="amount" />
                  </div>
                  <div className={`box-border py-1 pb-2 text-sm ${touched.amount && errors.amount ? 'text-red-700' : 'text-gray-150'} text-sm font-normal  my-4`}>
                    *Nominal donasi minimal Rp 10.000
                  </div>
                  {/* <div className="text-sm font-normal text-gray-150 my-4">*Nominal donasi minimal Rp 10.000</div> */}
                  <div className="bg-gray-150 h-0.5 w-11/12 my-4" />
                  <div className="text-sm font-normal text-gray-450 my-4">Beri dukungan di donasi ini (opsional)</div>
                  <div>
                    <input onChange={handleChange} onBlur={handleBlur} type="text" placeholder="Tulis pesan dukungan kamu untuk karya ini" className="border border-gray-150 w-11/12 h-36 rounded-md px-3 focus:outline-none" name="notes" id="notes" />
                  </div>
                </div>
              </div>
            </div>

            <div className="text-xl text-green-550 font-semibold text-center my-5"> Penawaran Hadiah Donasi </div>

            <div className=" border-gray-150 pb-10 flex flex-col items-center gap-10">
              <div className="flex gap-10 justify-center">
                <Reward {...baseReward} />
                { renderRewards() }
              </div>
              <div className="flex flex-col gap-5 w-full justify-center">
                {token ? (
                  <>
                    {mainAddress && mainAddress.id ? (
                      <>
                        <div className="flex justify-start font-semibold text-xl">
                          <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30" fill="none">
                            <path d="M22.5 10C22.5 5.8625 19.1375 2.5 15 2.5C10.8625 2.5 7.5 5.8625 7.5 10C7.5 15.625 15 23.75 15 23.75C15 23.75 22.5 15.625 22.5 10ZM12.5 10C12.5 8.625 13.625 7.5 15 7.5C16.375 7.5 17.5 8.625 17.5 10C17.5 11.375 16.3875 12.5 15 12.5C13.625 12.5 12.5 11.375 12.5 10ZM6.25 25V27.5H23.75V25H6.25Z" fill="#75BD38" />
                          </svg>
                          <div>Alamat Pengiriman</div>
                        </div>

                        <div className="grid grid-cols-4">
                          <div className="col-span-1 font-semibold text-xl">
                            <div>{mainAddress.recipient}</div>
                            <div>{mainAddress.phone}</div>
                          </div>
                          <div className="col-span-2 text-xl">
                            <div>{mainAddress.address}</div>
                            <div>{mainAddress.postalCode}</div>
                          </div>
                          <div className="col-span-1 flex justify-end">
                            {/* <button type="button" className="font-bold text-xl text-black">UBAH</button> */}
                            <AddressEditModal modifyModal={false} addressId={mainAddress.id} {...mainAddress} />
                          </div>
                        </div>
                        <div className="flex justify-center">
                          <button type="submit" className="bg-green-250 font-semibold text-white h-12 w-1/4 rounded-lg items-center object-center transition duration-300 hover:bg-green-550 mt-8">Donasi Sekarang</button>
                        </div>
                        {/* <LinkButton disable={loading} url={`/campaign/${campaignId}/donation/payment`} text="Donasi Sekarang" /> */}
                      </>
                    ) : (
                      <>
                        <div className="flex  font-semibold text-xl">
                          <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30" fill="none">
                            <path d="M22.5 10C22.5 5.8625 19.1375 2.5 15 2.5C10.8625 2.5 7.5 5.8625 7.5 10C7.5 15.625 15 23.75 15 23.75C15 23.75 22.5 15.625 22.5 10ZM12.5 10C12.5 8.625 13.625 7.5 15 7.5C16.375 7.5 17.5 8.625 17.5 10C17.5 11.375 16.3875 12.5 15 12.5C13.625 12.5 12.5 11.375 12.5 10ZM6.25 25V27.5H23.75V25H6.25Z" fill="#75BD38" />
                          </svg>
                          <div>Alamat Pengiriman</div>
                        </div>

                        <div className="grid grid-cols-4">
                          <div className="col-span-1 font-semibold text-xl">
                            <div>Nama Anda</div>
                            <div>Nomor Telepon Anda</div>
                          </div>
                          <div className="col-span-2 text-xl">
                            Harap masukan alamat Anda terlebih dahulu.
                          </div>
                          <div className="col-span-1 flex justify-end">
                            {/* <button type="button" className="font-bold text-xl text-black">TAMBAH ALAMAT</button>
                             */}
                            <AddressModal modifyModal={false} />
                          </div>
                        </div>
                        <div className="flex justify-center">
                          <button type="submit" className="bg-green-250 font-semibold text-white h-12 w-1/4 rounded-lg items-center object-center transition duration-300 hover:bg-green-550 mt-8">Donasi Sekarang</button>
                        </div>
                        {/* <LinkButton disable={loading} url={`/campaign/${campaignId}/donation/payment`} text="Donasi Sekarang" /> */}
                      </>
                    )}
                  </>
                )
                  : (
                    <>
                      <div className="flex justify-center gap-4">
                        <button type="submit" className="bg-green-250 font-semibold text-white h-12 w-1/4 rounded-lg items-center object-center transition duration-300 hover:bg-green-550">
                          {loading ? 'loading...' : 'Donasi Tanpa Login'}
                        </button>
                        <LinkButton url={`/login?campaign=${campaignId}`} text="Login dan Dapatkan Paket" />
                      </div>
                    </>
                  )}
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
}
