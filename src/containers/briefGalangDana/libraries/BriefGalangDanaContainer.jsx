import Image from 'next/image';
import LinkButton from '@/components/LinkButton';
import { withAuth } from '@/layouts/auth/libraries/Auth';

function BriefGalangDanaContainer() {
  return (
    <>
      <div className="mx-auto container">
        <div className="grid grid-cols-4 mx-10 my-20">
          <div className="col-span-3 flex items-center justify-center">
            <div className="text-2xl font-semibold">
              Saat ini AsaKarya berfokus sebagai media penghubung
              antara pelaku industri kreatif dengan investor untuk
              {' '}
              <span className="text-green-250">#DukungKaryaAnakBangsa</span>
              <div className="mt-8">
                Lengkapi profil kreator kamu dan mulai buat
                campaign galang dana untuk dukung karyamu!
              </div>
            </div>
          </div>
          <div className="col-span-1">
            <Image src="/img/searchBar/right-search.png" width={300} height={263} />
          </div>
        </div>
        <div className="flex justify-center items-center my-16">
          <LinkButton url="/creator" text="Lengkapi Profil Kreator" />
        </div>
      </div>
    </>
  );
}

export default withAuth(BriefGalangDanaContainer);
