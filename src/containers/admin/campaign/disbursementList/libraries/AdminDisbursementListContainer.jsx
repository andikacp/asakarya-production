import Image from 'next/image';
import ReactPaginate from 'react-paginate';
import { useRouter } from 'next/router';
import { convertDateToString } from '@/helpers/dateTime';
import { sessionUser } from '@/helpers/auth';
import { useAdminDispatch } from '@/redux/reducers/admin/slices';
import LinkButton from '@/components/LinkButton/libraries/LinkButton';
import BuktiTransferModal from '@/components/PopupTransfer';
import EmptyImage from '@/components/EmptyImage';

export default function AdminDisbursementListContainer() {
  const router = useRouter();
  const token = sessionUser();
  const { campaignId } = router.query;
  const { admin, fetchDisbursements } = useAdminDispatch();
  const { disbursements, totalPages, page } = admin;

  const handlePageChange = async (selectedPage) => {
    await fetchDisbursements({ page: selectedPage.selected, campaignId, token });
  };

  const renderDisbursement = () => disbursements && disbursements.map((item) => (
    <>
      <div className="flex gap-3 p-5 rounded-lg text-xl border border-gray-150">
        <div className="p-3">
          {item.profileImage ? <Image src={item.paymentSlipUrl} width="106" height="106" /> : <EmptyImage w={106} h={106} source="profile" />}
        </div>

        <div className="flex flex-start">

          <div className="flex flex-col gap-6 font-semibold" style={{ width: '680px' }}>
            <div>
              {item.title}
            </div>
            <div>
              {item.description}
            </div>

            <div className="flex justify-between w-3/4">
              <div className="font-normal">{convertDateToString(item.updatedAt)}</div>
              <div>
                Rp.
                {' '}
                {item.amount && (item.amount).toLocaleString('id-ID')}
                ,-
                {' '}
              </div>
            </div>
          </div>
        </div>
        <div className="flex items-center">
          <BuktiTransferModal
            item={item}
          />
        </div>
      </div>

    </>

  ));

  return (
    <div className="flex flex-col gap-5">
      <div className="flex justify-between items-center text-xl">
        <div>Konfirmasi Pengiriman Dana</div>
        <LinkButton url={`/admin/campaign/${campaignId}/disbursement/add`} text="+Tambah Dana Terkirim" />
      </div>
      {disbursements && disbursements.length > 0 ? renderDisbursement(disbursements) : (
        <div className="flex flex-col justify-center items-center p-4 gap-5">
          <EmptyImage source="campaign" h={320} w={320} />
          <div>Belum ada dana yang terkirim</div>
        </div>
      )}
      <div className={`${totalPages <= 1 && 'hidden'} flex justify-center`}>
        <ReactPaginate
          containerClassName="flex gap-5 mt-5 text-green-250 font-semibold"
          activeClassName="border border-green-250 px-2 rounded-md"
          initialPage={page}
          pageCount={totalPages}
          previousLabel="<"
          nextLabel=">"
          onPageChange={(e) => handlePageChange(e)}
        />
      </div>

    </div>
  );
}
