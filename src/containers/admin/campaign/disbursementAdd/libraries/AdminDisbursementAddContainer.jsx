// import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { Formik, Form } from 'formik';
import { sessionUser } from '@/helpers/auth';
import { useCampaignDispatch } from '@/redux/reducers/campaign/slices';
import UploadPict from '@/components/UploadPict';
import { baseApiUrl } from '@/helpers/baseContents';
import { useAdminDispatch } from '@/redux/reducers/admin/slices';

export default function AdminDisbursementAddContainer() {
  const router = useRouter();
  const token = sessionUser();
  const { campaignId } = router.query;
  const { campaign } = useCampaignDispatch();
  const { admin, fetchCreatorDetail } = useAdminDispatch();
  const { campaignDetail } = campaign;
  const { totalDisbursements, creatorDetail } = admin;
  const [loading, setLoading] = useState(false);
  const [remainingFunds, setRemainingFunds] = useState(0);

  useEffect(() => {
    fetchCreatorDetail(campaignDetail.creatorId, token);
  }, [campaignDetail.creatorId]);

  useEffect(() => {
    if (campaignDetail.sumDonation) {
      const fee = Math.ceil(campaignDetail.sumDonation * (5 / 100));
      const held = campaignDetail.sumDonation - campaignDetail.sumDisbursement - fee;
      setRemainingFunds(held);
    }
  }, [campaignDetail.sumDonation]);

  const handleUploadImage = async (values) => {
    setLoading(true);
    if (!values.paymentSlipUrl) return console.log('tanpa gambar');
    const formData = new FormData();
    formData.append('file', values.paymentSlipUrl);
    console.log(formData);
    if (token) {
      try {
        const response = await fetch(`${baseApiUrl}/file/upload`, {
          method: 'POST',
          body: formData,
        });
        const data = await response.json();
        const fileUri = await data.fileDownloadUri;
        setLoading(false);
        return fileUri;
        // console.log(data);
      } catch (error) {
        console.log(error);
        setLoading(false);
      }
    }
    setLoading(false);
    return null;
  };

  const handleSubmitForm = async (values) => {
    setLoading(true);
    console.log(values);
    const {
      description, amount, paymentSlipUrl,
    } = values;

    const formData = {
      campaignId,
      title: `Pencairan dana ke ${totalDisbursements + 1}`,
      description,
      amount,
      paymentSlipUrl,
    };

    try {
      const response = await fetch(`${baseApiUrl}/admin/disbursement/add`, {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });
      const data = await response.json();
      console.log(data);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      console.log(error);
    }
    router.push(`/admin/campaign/${campaignId}/disbursement`);
  };

  const handleOnSubmit = async (values) => {
    setLoading(true);
    const paymentSlipUrl = await handleUploadImage(values);
    await handleSubmitForm({ ...values, paymentSlipUrl });
  };

  const renderForm = () => (
    <Formik
      initialValues={
        {
          campaignId,
          title: '',
          description: '',
          amount: 0,
          paymentSlipUrl: null,
        }
      }
      onSubmit={handleOnSubmit}
      enableReinitialize
    >
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        setFieldValue,
      }) => (
        <Form className="grid grid-cols-6 gap-3" onSubmit={handleSubmit}>
          <div className="col-span-2">Nama Bank</div>
          <div className="col-span-4 font-semibold">{creatorDetail.bankName}</div>
          <div className="col-span-2">Nama Pemilik Rekening</div>
          <div className="col-span-4 font-semibold">{creatorDetail.bankAccountName}</div>
          <div className="col-span-2">Nomor Rekening</div>
          <div className="col-span-4 font-semibold">{creatorDetail.bankAccount}</div>
          <div className="col-span-2">Nominal Belum Terealisasi</div>
          <div className="col-span-4 font-semibold">{remainingFunds && (`Rp. ${remainingFunds.toLocaleString('id-ID')} ,-`)}</div>
          <div className="col-span-2">Masukkan Berita</div>
          <input id="description" name="description" type="text" onChange={handleChange} onBlur={handleBlur} required placeholder="Berita" className="w-full px-4 border border-gray-750 rounded-lg focus:border-green-250 col-span-4" />
          <div className="col-span-2">Masukkan Nominal</div>
          <input id="amount" max={remainingFunds} name="amount" type="number" onChange={handleChange} onBlur={handleBlur} required placeholder="Rp. 0" className="w-full px-4 border border-gray-750 rounded-lg focus:border-green-250 col-span-4" />
          <div className="col-span-2">Upload Bukti Transfer</div>
          <div className="col-span-4">
            <UploadPict
              id="paymentSlipUrl"
              name="paymentSlipUrl"
              onChange={(event) => {
                setFieldValue('file', event.currentTarget.files[0]);
              }}
            />
          </div>
          <div className="flex justify-center gap-3 col-span-6">
            <Link href={`/admin/campaign/${campaignId}/disbursement`}>
              <button type="button" className="bg-white text-green-250 border-green-250 border-2 font-semibold h-10 w-1/6 mt-3 rounded-lg">Batal</button>
            </Link>
            <button type="submit" className={`${loading ? 'bg-gray-150' : 'bg-green-250'} text-white font-semibold h-10 w-1/6 mt-3 rounded-lg transition duration-300 hover:bg-green-550`} disabled={!!loading}>{loading ? 'Mohon Tunggu' : 'Simpan'}</button>
          </div>
        </Form>
      )}

    </Formik>
  );

  return (
    <div className="flex flex-col gap-5 text-xl">
      <div className="flex flex-col  text-xl">
        <div>Breadcrumbs</div>
        <div>Verifikasi dana terkirim ke pelaku industri kreatif</div>
      </div>
      {
        creatorDetail && creatorDetail.bankName ? (
          <div>
            {renderForm()}
          </div>
        ) : 'loading'
      }

    </div>
  );
}
