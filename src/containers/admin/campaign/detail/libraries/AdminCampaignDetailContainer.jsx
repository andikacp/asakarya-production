import { useEffect } from 'react';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { sessionUser } from '@/helpers/auth';
import { useCampaignDispatch } from '@/redux/reducers/campaign/slices';
import CampaignListView from '@/components/CampaignListView';
import { baseApiUrl } from '@/helpers/baseContents';
import NavbarAdminCampaign from '@/components/NavbarAdminCampaign/libraries/NavbarAdminCampaign';
import { useAdminDispatch } from '@/redux/reducers/admin/slices';
import Breadcrumb from '@/components/Breadcrumb';

export default function AdminCampaignDetailContainer() {
  const token = sessionUser();
  const router = useRouter();
  const { campaignId } = router.query;
  const { campaign, fetchCampaignDetail, fetchRewards } = useCampaignDispatch();
  const { fetchDonations, fetchDisbursements } = useAdminDispatch();

  const { campaignDetail } = campaign;

  useEffect(() => {
    if (campaignId) {
      fetchCampaignDetail(campaignId);
      fetchRewards(campaignId);
      fetchDonations({ campaignId, token });
      fetchDisbursements({ campaignId, token });
    }
  }, [campaignId]);

  const pages = [
    { name: 'List Campaign Masuk', href: '/admin/campaign', current: false },
    { name: 'Verifikasi Campaign', href: '#', current: true },
  ];

  const handleVerify = async (id, status) => {
    try {
      const endpoint = `${baseApiUrl}/admin/campaign/verify/${id}?isVerified=${status}`;
      const response = await fetch(endpoint, {
        method: 'PUT',
        headers: {
          authorization: `Bearer ${token}`,
        },
      });
      const data = await response.json();
      console.log(data);
      window.location.reload();
    } catch (error) {
      console.log(error);
    }
  };

  const renderDetail = (item) => (
    <div className="flex flex-col gap-3 items-center justify-center">
      <CampaignListView admin {...item} />
      <div className="flex flex-start justify-center items-center my-4">
        {item.status === 0 && (
          <div className="flex flex-start gap-5 rounded-xl text-xl">
            <button type="button" className="bg-white font-semibold text-green-250 h-12 w-72 px-5 rounded-lg items-center object-center border-2 border-green-250" onClick={() => handleVerify(item.id, false)}>Gagal Verifikasi</button>
            <button type="button" className="bg-green-250 font-semibold text-white h-12 w-72 px-5 rounded-lg items-center object-center transition duration-300 hover:bg-green-550" onClick={() => handleVerify(item.id, true)}>Verifikasi Campaign</button>
          </div>
        )}
        {
                item.status === 1 && (
                  item.daysLeft < 0
                    ? (
                      <div className="flex">
                        <Image src="/img/donation/overdue.svg" width={25} height={25} />
                        <div className="font-semibold ml-3">Lewat Batas Waktu</div>
                      </div>
                    )
                    : (
                      <div className="flex">
                        <Image src="/img/donation/berhasil.svg" width={25} height={25} />
                        <div className="font-semibold ml-3">Terverifikasi</div>
                      </div>
                    )
                )
               }
        {
          item.status === 2 && (
            <div className="flex">
              <Image src="/img/donation/gagal.svg" width={25} height={25} />
              <div className="font-semibold ml-3 text-xl">Gagal</div>
            </div>
          )
        }
      </div>
    </div>
  );

  return (
    <div>
      <div className="flex flex-col gap-1 text-xl border-b-2 pb-4">
        <div className="-ml-4">
          <Breadcrumb pages={pages} />
        </div>
        <div>Kelola campaign pelaku industri kreatif</div>
      </div>
      <div className="mt-4">
        <div className="text-center text-4xl text-green-550 font-semibold mb-4">Tinjau Detail Campaign</div>
      </div>
      {
        campaignDetail && campaignDetail.id ? (
          <div>
            {renderDetail(campaignDetail)}
          </div>
        ) : 'loading'
      }

      <NavbarAdminCampaign />
    </div>
  );
}
