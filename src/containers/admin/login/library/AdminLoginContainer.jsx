/* eslint-disable max-len */

import { Formik, Form } from 'formik';
import React, { useState } from 'react';
import { FaEye, FaEyeSlash } from 'react-icons/fa';
import * as Yup from 'yup';
import Link from 'next/link';
import Image from 'next/image';
import { useRouter } from 'next/router';
import Input from '@/components/Input';
import { useLoginDispatch } from '@/redux/reducers/login/slices';
import PopupForgot from '@/components/PopupForgot';
import { withNoAuth } from '@/layouts/auth/libraries/Auth';
import { useUserDispatch } from '@/redux/reducers/user/slices';
import { sessionUser } from '@/helpers/auth';

const initialValues = {
  username: '',
  password: '',
};

const validationSchema = Yup.object({
  username: Yup.string()
    .email('Invalid email address')
    .required('This must be filled'),
  password: Yup.string()
    // .matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/, 'Password must contain at least 8 characters and at least one number and one capital letter')
    .required('This must be filled'),
});

function AdminLoginContainer() {
  const { login: { loading, success }, doLogin } = useLoginDispatch();

  const router = useRouter();

  const { getUserDetail } = useUserDispatch();
  // const { userDetail } = user;

  async function handleOnSubmit(values) {
    // const loginResult = await doLogin(values);
    await doLogin(values);
    // console.log(sessionUser());
    const token = sessionUser();
    await getUserDetail(token);
    router.push('/admin/dashboard');
    // console.log(loginResult);
  }

  const [showPassword, setShowPassword] = useState(false);
  const togglePassword = () => {
    setShowPassword(!showPassword);
  };

  // const [showModal, setShowModal] = React.useState(false);

  return (
    <div className="xl:w-1/3 lg:w-1/4 md:w-1/2 sm:w-1/2 xs:w-1/2 mx-auto pb-20">
      <div className="box-border">
        <div className="flex justify-center pt-5 pb-2">
          <Image width="61" height="72" src="/img/logo/logo.png" alt="logo" />
        </div>
        <div className="flex justify-center pb-5">
          <Image width="250" height="46.67" src="/img/logo/asakarya-text-logo.png" alt="asakarya" />
        </div>
      </div>
      <div className="flex justify-center items-center border rounded-lg border-gray-300 bg-white filter drop-shadow-md">
        <div className="w-full h-full">
          <h1 className="text-xl font-bold mb-5 flex justify-center mx-5 mt-5">Masuk</h1>
          <div className="mx-7" id="login">
            <Formik
              initialValues={initialValues}
              onSubmit={(values, { ...rest }) => handleOnSubmit(values, { ...rest })}
              validationSchema={validationSchema}
            >
              {({
                handleSubmit,
                isValid,
                dirty,
                touched,
                errors,
              }) => (
                <Form onSubmit={handleSubmit}>
                  <div className="w-full">
                    <div className="text-md text-black font-bold py-1">Email</div>
                    <Input
                      title="Email"
                      id="username"
                      name="username"
                      placeholder="Masukan alamat email Anda"
                      type="email"
                      // onChange={(e) => setUsername(e.target.value)}
                      // value={values.username}
                    />
                    <div className={`box-border py-1 pb-2 text-xs ${touched.username && errors.username ? 'text-red-700' : ''}`}>
                      {touched.username && errors.username && errors.username}
                    </div>
                  </div>
                  <div className="w-full">
                    <div className="text-md text-black font-bold py-1">Password</div>
                    <label htmlFor="password" className="block border-gray-300 w-full">
                      <div className="flex flex-row gap-3">
                        <Input
                          className="w-full px-4 border rounded-lg py-2 focus:border-green-250"
                          title="Password"
                          id="password"
                          name="password"
                          // onChange={(e) => setPassword(e.target.value)}
                          // value={values.password}
                          type={showPassword ? 'text' : 'password'}
                          placeholder="Masukan password Anda"
                        />
                        <button type="button" className="flex items-center cursor-pointer focus:outline-none" onClick={togglePassword}>{showPassword ? <FaEyeSlash style={{ color: 'gray', fontSize: '20px' }} /> : <FaEye style={{ color: 'gray', fontSize: '20px' }} />}</button>
                      </div>
                    </label>
                    <div className={`box-border py-1 pb-2 text-xs ${touched.password && errors.password ? 'text-red-700' : ''}`}>
                      {touched.password && errors.password && errors.password}
                    </div>
                  </div>
                  <div className="flex justify-center mt-3">
                    <PopupForgot />
                  </div>
                  <div className="pt-2 pb-6">
                    <button
                      className="bg-green-250 text-sm text-white border-none rounded-lg font-bold py-3 w-full"
                      type="submit"
                      disabled={!(isValid && dirty) && loading && success}
                    >
                      {loading ? 'Mohon tunggu...' : 'Masuk'}
                    </button>
                    {success ? (
                      <div className="pt-3">
                        <div className="border-none rounded-md bg-red-500 bg-transparent opacity-70 p-2 text-center text-xs  text-white">
                          Email atau password salah. Mohon cek kembali.
                        </div>
                      </div>
                    ) : null }
                  </div>
                </Form>
              )}
            </Formik>
          </div>
          <div className="w-full flex justify-center py-3">
            <div className="text-sm">
              Belum punya akun?
              <Link href="/admin/register">
                <a className="ml-2 underline text-green-550">Daftar</a>
              </Link>
            </div>
          </div>
          <div className="w-full flex justify-center text-xs px-5 pt-5 pb-5 text-gray-500 text-center">
            {/* eslint-disable-next-line max-len */}
            <p className="mx-6">Privasi Anda akan kami lindungi dan Asakarya berkomitmen tidak akan menyebarkan data pribadi Anda.</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default withNoAuth(AdminLoginContainer);
