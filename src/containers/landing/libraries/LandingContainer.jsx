/* eslint-disable react/no-array-index-key */
import Image from 'next/image';
import Link from 'next/link';
import { Formik } from 'formik';
import { useState, useEffect } from 'react';
import CampaignGridView from '@/components/CampaignGridView';
import { useCampaignDispatch } from '@/redux/reducers/campaign/slices';
import { baseApiUrl } from '@/helpers/baseContents';

export default function LandingContainer() {
  const initialValues = {
    name: '',
    email: '', // email
    message: '',
  };

  const {
    fetchCampaigns, campaign,
  } = useCampaignDispatch();
  const {
    campaigns,
  } = campaign;

  const renderCampaign = () => campaigns && campaigns.map((item, idx) => (
    <div key={`campaign-${idx}`}>
      <CampaignGridView {...item} />
    </div>
  ));

  useEffect(async () => {
    await fetchCampaigns({ size: 3 });
  }, []);

  const [loading, setLoading] = useState(false);

  const submitForm = async (values) => {
    try {
      const response = await fetch(`${baseApiUrl}/contact/add`, {
        method: 'POST',
        body: JSON.stringify(values),
        headers: {
          'Content-Type': 'application/json',
        },
      });
      const data = await response.json();
      console.log(data);
      setLoading(true);
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };
  const handleOnSubmit = async (values, { resetForm }) => {
    setLoading(true);
    await submitForm(values);
    resetForm({ values: '' });
    setLoading(false);
  };
  return (
    <>
      <div className="container mx-auto">
        <div className="my-10 mx-6">
          <div className="grid grid-cols-2">
            <div className="col-span-1">
              <Image
                src="/img/landing/beri-harapan.png"
                alt="beri harapan"
                width={676}
                height={452}
              />
            </div>
            <div className="col-span-1 flex justify-center items-center">
              <div className="grid grid-rows-3 mx-6">
                <div className="text-4xl font-semibold">
                  Beri harapan untuk kemajuan Industri Kreatif Indonesia
                </div>
                <div className="text-2xl font-medium text-gray-350 mt-4">
                  Saatnya support karya anak bangsa
                </div>
                <div className="flex gap-4">
                  <Link href="/mycampaign">
                    <button type="button" className="transition duration-300 border border-green-250 text-green-250 w-48 h-12 text-xl rounded-xl font-normal ml-4 hover:border-green-550 hover:bg-green-550 hover:text-white">Galang Dana</button>
                  </Link>
                  <Link href="/campaign">
                    <button type="button" className="transition duration-300 bg-green-250 text-white w-48 h-12 text-xl rounded-xl font-normal hover:bg-green-550">Mulai Dukung</button>
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className="bg-gray-150 h-0.5 my-6 mx-1 mt-12" />

          {/* Section 2 */}
          <div className="flex items-center justify-center my-16">
            <h1 className="text-5xl text-black font-semibold">Tentang Kami</h1>
          </div>
          <div className="grid grid-cols-3">
            <div className="col-span-2 flex items-center justify-center">
              <p className="font-bold text-4xl leading-normal mx-32 ml-16">
                <span className="text-green-550">Asa</span>
                <span className="text-green-250">Karya</span>
                {' '}
                merupakan suatu platform socinvest crowdfunding yang berfokus pada industri kreatif.
              </p>
            </div>
            <div className="col-span-1 flex items-center justify-center">
              <Image
                src="/img/landing/biglogo.png"
                alt="logo"
                width={411}
                height={588}
              />
            </div>
          </div>
          <div className="bg-gray-150 h-0.5 my-6 mx-1 mt-12" />

          {/* Section 3 */}
          <div className="grid grid-cols-2 my-16">
            <div className="col-span-1">
              <div>
                <div className="grid-cols-2 flex">
                  <div className="col-span-1 flex justify-center mx-7">
                    <Image
                      src="/img/landing/download.png"
                      alt="download"
                      width={48}
                      height={48}
                    />
                    <div className="grid-rows-2">
                      <div className="text-3xl font-semibold">
                        10,000+
                      </div>
                      <div className="text-base font-normal">
                        Downloads per hari
                      </div>
                    </div>
                  </div>
                  <div className="col-span-1 flex mx-2">
                    <Image
                      src="/img/landing/users.png"
                      alt="download"
                      width={48}
                      height={48}
                    />
                    <div className="grid-rows-2">
                      <div className="text-3xl font-semibold">
                        2 juta
                      </div>
                      <div className="text-base font-normal">
                        Pengguna
                      </div>
                    </div>
                  </div>
                </div>
                <div className="grid-cols-2 flex mx-3 my-8 ml-7">
                  <div className="col-span-1 flex justify-center">
                    <Image
                      src="/img/landing/investor.png"
                      alt="download"
                      width={48}
                      height={48}
                    />
                    <div className="grid-rows-2">
                      <div className="text-3xl font-semibold">
                        500+
                      </div>
                      <div className="text-base font-normal">
                        Investor
                      </div>
                    </div>
                  </div>
                  <div className="col-span-1 flex mx-28">
                    <Image
                      src="/img/landing/spread.png"
                      alt="download"
                      width={48}
                      height={48}
                    />
                    <div className="grid-rows-2">
                      <div className="text-3xl font-semibold">
                        100+
                      </div>
                      <div className="text-base font-normal">
                        Kota dan Kabupaten
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-span-1">
              <div className="grid grid-rows-2">
                <div className="text-5xl font-semibold">
                  Mengapa AsaKarya
                </div>
                <div className="text-2xl font-medium mt-4 text-gray-350">
                  Bersama Asakarya memberi harapan untuk kemajuan Industri Kreatif Indonesia
                </div>
              </div>
            </div>
          </div>
          <div className="grid grid-cols-3">
            <div className="col-span-1">
              <Image
                src="/img/landing/hp.png"
                alt="handphone"
                width={988}
                height={1200}
              />
            </div>
            <div className="col-span-2 flex justify-center items-center">
              <div className="grid grid-rows-3 text-center">
                <div className="text-5xl font-semibold">
                  Download Aplikasi AsaKarya!
                </div>
                <div className="text-3xl font-normal text-gray-350">
                  Satu langkah memberi harapan kemajuan Industri Kreatif di Indonesia
                </div>
                <div className="mt-4">
                  <Link href="#">
                    <Image
                      src="/img/landing/gplay.png"
                      alt="google play"
                      width={240}
                      height={67}
                    />
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className="bg-gray-150 h-0.5 my-6 mx-1 mt-12 mb-3" />

          {/* Section 4 */}
          <div className="flex justify-between items-stretch mx-3">
            <Link href="/about">
              <a className="text-2xl font-semibold text-gray-350">Mengenal Asakarya</a>
            </Link>
            <div className="bg-gray-150 w-0.5 h-9" />
            <Link href="/campaign">
              <a className="text-2xl font-semibold text-gray-350">Lihat Campaign Aktif</a>
            </Link>
            <div className="bg-gray-150 w-0.5 h-9" />
            <Link href="/galeri">
              <a className="text-2xl font-semibold text-gray-350">Galeri Karya Pelaku Industri kreatif</a>
            </Link>
            <div className="bg-gray-150 w-0.5 h-9" />
            <Link href="/login">
              <a className="text-2xl font-semibold text-gray-350">Masuk Sekarang</a>
            </Link>
          </div>
          <div className="bg-gray-150 h-0.5 my-6 mx-1 mt-3" />

          {/* Section 5 */}
          <div className="flex justify-center items-center gap-5">
            <div className="flex-grow">
              <Image src="/img/searchBar/left-search.png" width="236" height="212" alt="Left" />
            </div>
            <div className="flex flex-col flex-grow justify-center text-center p-8">
              <h2 className="text-5xl text-green-550">Ubah Mimpi jadi Asa Pasti</h2>
              <p className="my-3 text-xl px-32">Rekan Asa, kami tahu kalian adalah orang baik</p>
            </div>
            <div className="flex-grow">
              <Image src="/img/searchBar/right-search.png" width="236" height="212" alt="Right" />
            </div>
          </div>
          <div className="flex justify-center items-center">
            <h2 className=" text-4xl my-6 font-semibold">
              Mereka
              {' '}
              <span className="text-green-250">butuh</span>
              {' '}
              dukunganmu
            </h2>
          </div>
          <div>
            {loading ? (
              <div>loading</div>
            ) : (
              <div className="grid grid-cols-3 gap-10">
                {renderCampaign()}
              </div>
            ) }
          </div>
          <div className="flex justify-center items-center">
            <Link href="/campaign">
              <button type="button" className="underline text-sm ml-3 mb-3 text-green-550 mt-4">Lihat Lebih Banyak Campaign</button>
            </Link>
          </div>
          <div className="bg-gray-150 h-0.5 my-6 mx-1 mt-12" />

          {/* Section 6 */}
          <div className="bg-picture h-80 flex justify-center items-center mt-12 px-12">
            <Link href="/galeri">
              <a className="text-white font-bold text-7xl shadow">Lihat Galeri Karya</a>
            </Link>
          </div>
          <div className="bg-gray-150 h-0.5 my-6 mx-1 mt-12" />

          {/* Section 7 */}
          <div className="flex items-center justify-center mt-8">
            <h1 className="text-5xl text-black font-semibold">Hubungi Kami</h1>
          </div>
          <div className="flex items-center justify-center mt-4">
            <h1 className="text-2xl text-gray-350 font-normal">
              Kami terbuka dengan masukan dan saran dari Rekan Asa!
            </h1>
          </div>

          <div className="grid grid-cols-2">
            <div className="col-span-1">
              <div className="border rounded-lg border-gray-300 bg-white w-3/4 h-auto mt-4">
                <h1 className="text-xl font-bold mb-5 pt-3 flex justify-center mx-5 mt-5">Hubungi Kami</h1>
                <Formik
                  initialValues={initialValues}
                  onSubmit={(values, { ...rest }) => handleOnSubmit(values, { ...rest })}
                >
                  {({
                    handleSubmit,
                    handleChange,
                    handleBlur,
                    values,
                  }) => (
                    <form onSubmit={handleSubmit} className="mx-8">
                      <div className="text-sm text-black font-bold my-2">Nama</div>
                      <input
                        className="border w-full h-9 rounded-md mb-2 text-xs px-3"
                        title="Name"
                        id="name"
                        name="name"
                        placeholder="Masukan nama Anda"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        type="text"
                        value={values.name}
                      />
                      <div className="text-sm text-black font-bold my-2">Email</div>
                      <input
                        className="border w-full h-9 rounded-md mb-2 text-xs px-3"
                        title="Email"
                        id="email"
                        name="email"
                        placeholder="Masukan email Anda"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        type="email"
                        value={values.email}
                      />
                      <div className="text-sm text-black font-bold my-2">Kritik dan Saran</div>
                      <input
                        className="border w-full h-52 rounded-md mb-2 text-xs px-3"
                        title="message"
                        id="message"
                        name="message"
                        placeholder="Masukan pesan Anda"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        type="text"
                        value={values.message}
                      />
                      <div className="pt-2 pb-6">
                        <button
                          className="bg-green-250 text-sm text-white border-none rounded-lg font-bold py-3 w-full mt-6"
                          type="submit"
                          disabled={loading}
                        >
                          {loading ? 'Mohon Tunggu...' : 'Kirim'}
                        </button>
                      </div>
                      <div className="w-full flex justify-center text-xs px-5 pt-5 pb-5 text-gray-500 text-center">
                        {/* eslint-disable-next-line max-len */}
                        <p className="">Privasi Anda akan kami lindungi dan Asakarya berkomitmen tidak akan menyebarkan data pribadi Anda.</p>
                      </div>
                    </form>
                  )}
                </Formik>
              </div>
            </div>
            <div className="col-span-1 flex justify-center items-center">
              <div className="grid grid-rows-4 justify-center items-center">
                <div className="flex justify-start items-center">
                  <div>
                    <Image
                      src="/img/landing/map.png"
                      alt="maps"
                      width={34}
                      height={42}
                    />
                  </div>
                  <div className="ml-8">
                    Kemang, Jakarta Selatan
                  </div>
                </div>
                <div className="flex justify-start items-center">
                  <div>
                    <Image
                      src="/img/landing/phone.png"
                      alt="maps"
                      width={29}
                      height={46}
                    />
                  </div>
                  <div className="ml-9">
                    021 - 14007
                  </div>
                </div>
                <div className="flex justify-start items-center">
                  <div>
                    <Image
                      src="/img/landing/messsages.png"
                      alt="maps"
                      width={36}
                      height={31}
                    />
                  </div>
                  <div className="ml-8">
                    asakarya.indonesia@gmail.com
                  </div>
                </div>
                <div className="grid-cols-4 gap-4 flex justify-center items-center mt-8">
                  <div>
                    <Image
                      src="/img/landing/ig.png"
                      alt="maps"
                      width={46.26}
                      height={46.26}
                    />
                  </div>
                  <div>
                    <Image
                      src="/img/landing/tw.png"
                      alt="maps"
                      width={48.98}
                      height={40.06}
                    />
                  </div>
                  <div>
                    <Image
                      src="/img/landing/fb.png"
                      alt="maps"
                      width={48.99}
                      height={49.31}
                    />
                  </div>
                  <div>
                    <Image
                      src="/img/landing/yt.png"
                      alt="maps"
                      width={48.98}
                      height={34.63}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
