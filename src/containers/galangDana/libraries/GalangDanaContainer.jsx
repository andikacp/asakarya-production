import Image from 'next/image';
import { useEffect } from 'react';
import ReactPaginate from 'react-paginate';
import Link from 'next/link';
import { useCampaignDispatch } from '@/redux/reducers/campaign/slices';
import { sessionUser } from '@/helpers/auth.js';
import EmptyImage from '@/components/EmptyImage';
import LinkButton from '@/components/LinkButton';

export default function GalangDanaContainer() {
  const token = sessionUser();
  const {
    fetchMonitoringCampaigns, campaign,
  } = useCampaignDispatch();
  const {
    campaigns, loading, totalPages, page,
  } = campaign;

  const renderDetail = () => campaigns && campaigns.map((item) => (
    <div key={`campaign-${item.id}`}>
      <div className="bg-gray-300 w-auto my-4 mx-8 border-t-2" />
      <div className="grid grid-cols-5 mx-7 mt-6">
        <div className="col-span-1 justify-center">
          {item.imgUrl ? <Image src={item.imgUrl} width="170" height="170" className="rounded-lg object-cover" /> : <EmptyImage w={170} h={170} source="default" />}
        </div>
        <div className="col-span-3 text-xl">
          <div className="grid-cols-2 flex justify-start items-center gap-4">
            <div className="col-span-1 my-4">
              <div className="my-2">Judul</div>
              <div className="my-2">Target</div>
              <div className="my-2">Terkumpul</div>
              <div className="my-2">Sisa Waktu</div>
            </div>
            <div className="col-span-1">
              <div className="font-semibold my-2">{item.title}</div>
              <div className="my-2">
                Rp.
                {' '}
                {(item.fundAmount).toLocaleString('id-ID')}
              </div>
              <div className="my-2">
                Rp.
                {' '}
                {(item.sumDonation).toLocaleString('id-ID')}
              </div>
              <div className="my-2">
                {item.daysLeft}
                {' '}
                hari lagi
              </div>
            </div>
          </div>
        </div>
        <div className="col-span-1 flex justify-end items-center text-right">
          <div className="text-right">
            <div className="flex text-lg gap-2 my-2 justify-end font-semibold">
              {item.status === 0 && (
              <>
                <Image src="/img/donation/diproses.svg" width={25} height={25} />
                {' '}
                <div>Diproses</div>
              </>
              )}
              {
                item.status === 1 && (
                  item.daysLeft < 0
                    ? (
                      <div className="flex">
                        <Image src="/img/donation/overdue.svg" width={25} height={25} />
                        <div className="font-semibold ml-3">Lewat Batas Waktu</div>
                      </div>
                    )
                    : (
                      <div className="flex">
                        <Image src="/img/donation/berhasil.svg" width={25} height={25} />
                        <div className="font-semibold ml-3">Terverifikasi</div>
                      </div>
                    )
                )
               }
              {item.status === 2 && (
              <>
                <Image src="/img/donation/gagal.svg" width={25} height={25} />
                {' '}
                <div>Gagal</div>
              </>
              )}
            </div>
            <a href={`/mycampaign/${item.id}/modify`}>
              <button type="button" className="text-lg underline text-black hover:text-green-250 hover:underline">Edit Campaign</button>
            </a>
            <Link href={`/mycampaign/${item.id}/donation`}>
              <button type="button" className="border border-green-250 text-green-250 text-center font-medium rounded-lg text-lg w-56 mt-2 hover:bg-green-250 hover:text-white transition duration-300">Monitor Campaign</button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  ));

  const handlePageChange = async (selectedPage) => {
    await fetchMonitoringCampaigns({ token, page: selectedPage.selected });
  };

  useEffect(async () => {
    await fetchMonitoringCampaigns({ token });
  }, []);

  return (
    <>
      <div className="mx-auto container">
        <div className="flex justify-center items-center">
          <div className="border rounded-lg border-gray-300 bg-white my-8 w-11/12 h-auto">
            <div className="grid grid-cols-2 my-6 mx-6">
              <div className="col-span-1">
                <div className="text-center text-xl font-semibold flex justify-start">Campaign Saya</div>
                <div className="text-center text-xl flex justify-start">Kelola Campaign Anda</div>
              </div>
              <div className="col-span-1 flex justify-end">
                <a href="/mycampaign/create">
                  <button type="button" className="text-white bg-green-250 text-xl rounded-xl w-64 h-10 hover:bg-green-550 transition duration-300">+ Buat Campaign Baru</button>
                </a>
              </div>
            </div>

            {loading === false && campaigns && campaigns.length === 0 ? (
              <div>
                <div className="flex justify-center items-center mt-5">
                  <Image
                    src="/img/campaign/boxes.png"
                    width="311"
                    height="319"
                    alt="empty-boxes"
                  />
                </div>
                <div className="flex justify-center items-center mt-8">
                  Belum ada campaign yang dibuat
                </div>
                <div className="flex justify-center items-center my-4">
                  <LinkButton url="/mycampaign/create" text="Buat Campaign Baru" />
                </div>
              </div>
            ) : (
              <div className="">
                {renderDetail()}
              </div>
            ) }
            {
                totalPages > 1 && (
                <div className="flex justify-center my-4">
                  <ReactPaginate
                    containerClassName="flex gap-5 mt-5 text-green-250 font-semibold"
                    activeClassName="border border-green-250 px-2 rounded-md"
                    initialPage={page}
                    pageCount={totalPages}
                    previousLabel="<"
                    nextLabel=">"
                    onPageChange={(e) => handlePageChange(e)}
                  />
                </div>
                )
            }

          </div>
        </div>
      </div>
    </>
  );
}
