/* eslint-disable no-plusplus */
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { useCampaignDispatch } from '@/redux/reducers/campaign/slices';
import { baseApiUrl } from '@/helpers/baseContents';
import { sessionUser } from '@/helpers/auth';
import CreatorCampaignForm from '@/components/CreatorCampaignForm';
import Breadcrumb from '@/components/Breadcrumb';

export default function CreateCampaignContainer() {
  const router = useRouter();
  const token = sessionUser();
  const { campaignId } = router.query;
  const {
    campaign, fetchCategories, fetchCampaignDetail, fetchRewards,
  } = useCampaignDispatch();
  const { categories, campaignDetail, rewards } = campaign;
  const [loading, setLoading] = useState();
  const [currentRewards, setCurrentRewards] = useState();

  useEffect(() => {
    fetchCategories();
  }, []);

  useEffect(async () => {
    if (campaignId) {
      await fetchCampaignDetail(campaignId);
      await fetchRewards(campaignId);
    }
  }, [campaignId]);

  const pages = [
    { name: 'Campaign Saya', href: '/mycampaign', current: false },
    { name: 'Edit Campaign', href: '#', current: true },
  ];

  const saveCurrentRewards = () => {
    const rewardIdObj = {};
    for (let i = 0; i < rewards.length; i++) {
      if (rewards[i].rewardTypeId === 1) {
        rewardIdObj.item1Id = rewards[i].id;
        rewardIdObj.img1 = rewards[i].imgUrl;
        if (rewardIdObj.img1 === null) rewardIdObj.img1 = '';
      }
      if (rewards[i].rewardTypeId === 2) {
        rewardIdObj.item2Id = rewards[i].id;
        rewardIdObj.img2 = rewards[i].imgUrl;
        if (rewardIdObj.img2 === null) rewardIdObj.img2 = '';
      }
      if (rewards[i].rewardTypeId === 3) {
        rewardIdObj.item3Id = rewards[i].id;
        rewardIdObj.img3 = rewards[i].imgUrl;
        if (rewardIdObj.img3 === null) rewardIdObj.img3 = '';
      }
    }
    setCurrentRewards(rewardIdObj);
  };

  useEffect(() => {
    saveCurrentRewards();
    // console.log('item1Id' in rewardIdObj);
    // console.log('item2Id' in Object.keys(rewardIdObj));
    // console.log('item3Id' in Object.keys(rewardIdObj));
  }, [rewards]);

  useEffect(() => {
    console.log(currentRewards);
    // console.log('item1Id' in rewardIdObj);
    // console.log('item2Id' in Object.keys(rewardIdObj));
    // console.log('item3Id' in Object.keys(rewardIdObj));
  }, [currentRewards]);

  const handleUploadImage = async (values) => {
    if (values.imgUrl && !values.imgFile) return console.log('tanpa update gambar');
    const formData = new FormData();
    formData.append('file', values.imgFile);
    console.log(formData);
    if (token) {
      try {
        const response = await fetch(`${baseApiUrl}/file/upload`, {
          method: 'POST',
          body: formData,
        });
        const data = await response.json();
        const fileUri = await data.fileDownloadUri;
        return fileUri;
        // console.log(data);
      } catch (error) {
        console.log(error);
      }
    } return null;
  };

  const submitForm = async (values) => {
    console.log(values);
    const {
      title, description, fundAmount, due, location, categoryId, imgUrl,
    } = values;

    const formData = {
      id: campaignId,
      title,
      description,
      fundAmount,
      due,
      location,
      categoryId,
      imgUrl,
    };

    try {
      const response = await fetch(`${baseApiUrl}/campaign/update`, {
        method: 'PUT',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });
      const data = await response.json();
      console.log(data);
    } catch (error) {
      console.log(error);
    }
  };

  const rewardImagesObj = (values) => {
    const imageObj = {};
    if (values.img1) imageObj.img1 = values.img1;
    if (values.img2) imageObj.img2 = values.img2;
    if (values.img3) imageObj.img3 = values.img3;
    return imageObj;
  };

  const uploadRewardImages = async (images) => {
    const len = Object.keys(images).length;

    // return jika image kosong
    if (len === 0) return console.log('tanpa update gambar');

    const imageUpload = new FormData();
    for (let i = 0; i < len; i++) {
      imageUpload.append('files', Object.values(images)[i]);
    }
    try {
      const response = await fetch(`${baseApiUrl}/file/upload-multiple`, {
        method: 'POST',
        body: imageUpload,
      });
      const data = await response.json();
      console.log('data: ', data);

      // objek untuk nyimpan hasil hit api
      const imagesWithUri = {};
      for (let i = 0; i < len; i++) {
        const key = Object.keys(images)[i];
        console.log(key);
        imagesWithUri[key] = data[i].fileDownloadUri;
      }
      console.log('imageUri: ', imagesWithUri);
      return imagesWithUri;
    } catch (error) {
      console.log(error);
      return null;
    }
  };

  const submitUpdateReward = async (values, existingRewards, images) => {
    const formData = [];
    const imgData = {};

    if (values.img1 && 'img1' in images) {
      imgData.img1 = images.img1;
    } else { imgData.img1 = existingRewards.img1; }
    if (values.img2 && 'img2' in images) {
      imgData.img2 = images.img2;
    } else { imgData.img2 = existingRewards.img2; }
    if (values.img3 && 'img3' in images) {
      imgData.img3 = images.img3;
    } else { imgData.img3 = existingRewards.img3; }

    if ((values.item1) && ('item1Id' in existingRewards)) {
      formData.push(
        {
          id: existingRewards.item1Id,
          item: values.item1,
          imgUrl: imgData.img1,
        },
      );
    }
    if ((values.item2) && ('item2Id' in existingRewards)) {
      formData.push(
        {
          id: existingRewards.item2Id,
          item: values.item2,
          imgUrl: imgData.img2,
        },
      );
    }
    if ((values.item3) && ('item3Id' in existingRewards)) {
      formData.push(
        {
          id: existingRewards.item3Id,
          item: values.item3,
          imgUrl: imgData.img3,
        },
      );
    }
    console.log('update reward : ');
    console.log(formData);
    if (formData.length === 0) return console.log('tanpa update reward');
    try {
      const response = await fetch(`${baseApiUrl}/reward/update`, {
        method: 'PUT',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });
      const data = await response.json();
      console.log(data);
      return null;
    } catch (error) {
      console.log(error);
      return null;
    }
  };

  const submitPostReward = async (values, existingRewards, images) => {
    const formData = [];
    const imgData = {};

    if (values.img1 && 'img1' in images) {
      imgData.img1 = images.img1;
    } else { imgData.img1 = existingRewards.img1; }
    if (values.img2 && 'img2' in images) {
      imgData.img2 = images.img2;
    } else { imgData.img2 = existingRewards.img2; }
    if (values.img3 && 'img3' in images) {
      imgData.img3 = images.img3;
    } else { imgData.img3 = existingRewards.img3; }

    if ((values.item1) && !('item1Id' in existingRewards)) {
      formData.push(
        {
          rewardTypeId: 1,
          campaignId,
          item: values.item1,
          imgUrl: imgData.img1,
        },
      );
    }
    if ((values.item2) && !('item2Id' in existingRewards)) {
      formData.push(
        {
          rewardTypeId: 2,
          campaignId,
          item: values.item2,
          imgUrl: imgData.img2,
        },
      );
    }
    if ((values.item3) && !('item3Id' in existingRewards)) {
      formData.push(
        {
          rewardTypeId: 3,
          campaignId,
          item: values.item3,
          imgUrl: imgData.img3,
        },
      );
    }
    console.log('post reward : ');
    // return console.log(formData);
    if (formData.length === 0) return console.log('tanpa post reward');
    try {
      const response = await fetch(`${baseApiUrl}/reward/add`, {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });
      const data = await response.json();
      console.log(data);
      return null;
    } catch (error) {
      console.log(error);
      return null;
    }
  };

  const handleOnSubmit = async (values) => {
    setLoading(true);
    const imgUrl = await handleUploadImage(values);
    if (imgUrl) {
      await submitForm({ ...values, imgUrl });
    } else { await submitForm(values); }
    if (values.item1 || values.item2 || values.item3) {
      const rewardUri = rewardImagesObj(values);
      const imagesUri = await uploadRewardImages(rewardUri);
      if (imagesUri) {
        await submitPostReward(values, currentRewards, imagesUri);
        await submitUpdateReward(values, currentRewards, imagesUri);
      } else {
        await submitPostReward(values, currentRewards);
        await submitUpdateReward(values, currentRewards);
      }

      // console.log(addRewardResponse);
      // console.log(updateRewardResponse);
    }
    setLoading(false);
    router.push(('/mycampaign'));
    // redirect ke list campaign
  };

  return (
    <div className="w-3/4 border border-gray-150 rounded-md p-5 overflow-y-auto" style={{ height: 1000 }}>
      <div className="text-xl border-b-2 border-gray-150 pb-2 flex flex-col gap-1">
        <div className="-ml-4">
          <Breadcrumb pages={pages} />
        </div>
        <div>Kelola informasi campaign anda</div>
      </div>
      <div>
        {campaignDetail && campaignDetail.id ? (
          <CreatorCampaignForm
            campaignId={campaignId}
            onSubmit={handleOnSubmit}
            loading={loading}
            categories={categories}
            {...campaignDetail}
          />
        ) : <div className="ml-7 mt-7">loading</div>}

      </div>
    </div>
  );
}
