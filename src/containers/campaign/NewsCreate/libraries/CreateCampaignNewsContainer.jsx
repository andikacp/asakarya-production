import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { baseApiUrl } from '@/helpers/baseContents';
import { sessionUser } from '@/helpers/auth';
import { useCampaignDispatch } from '@/redux/reducers/campaign/slices';
import CreatorNewsForm from '@/components/CreatorNewsForm';
import Breadcrumb from '@/components/Breadcrumb';

export default function CreateCampaignNewsContainer() {
  const router = useRouter();
  const { campaignId } = router.query;
  const [loading, setLoading] = useState();

  const {
    fetchCampaignDetail, campaign,
  } = useCampaignDispatch();
  const { campaignDetail } = campaign;

  const token = sessionUser();

  useEffect(() => {
    if (campaignId) {
      fetchCampaignDetail(campaignId);
    }
  }, [campaignId]);

  const pages = [
    { name: 'Kabar Terbaru', href: `/mycampaign/${campaignId}/news`, current: false },
    { name: 'Tambah Kabar Terbaru', href: '#', current: true },
  ];

  const handleUploadImage = async (values) => {
    const formData = new FormData();
    formData.append('file', values.imgUrl);
    if (token) {
      try {
        const response = await fetch(`${baseApiUrl}/file/upload`, {
          method: 'POST',
          body: formData,
        });
        const data = await response.json();
        const fileUri = await data.fileDownloadUri;
        return fileUri;
        // setFileData(fileUri);
        // console.log(data);
        // console.log(fileUri);
      } catch (error) {
        console.log(error);
      }
    }
    console.log('Silakan login terlebih dulu');
    return null;
  };

  const submitForm = async (values) => {
    console.log(values);
    const {
      title, activity, imgUrl,
    } = values;

    const formData = {
      title,
      activity,
      imgUrl,
      campaignId,
    };

    try {
      const response = await fetch(`${baseApiUrl}/history/add/`, {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });
      const data = await response.json();
      console.log(data);
    } catch (error) {
      console.log(error);
    }
  };

  const handleOnSubmit = async (values) => {
    setLoading(true);
    const imgUrl = await handleUploadImage(values);
    await submitForm({ ...values, imgUrl });
    console.log(values);
    setLoading(false);
    router.push(`/mycampaign/${campaignId}/news`);
    // redirect ke list news
  };

  return (
    <div className="w-3/4 border border-gray-150 rounded-md p-5 overflow-y-auto" style={{ height: 1000 }}>
      <div className="text-xl border-b-2 border-gray-150 pb-2 flex flex-col gap-1">
        <div className="-ml-4"><Breadcrumb pages={pages} /></div>
        <div>{campaignDetail && campaignDetail.title ? campaignDetail.title : 'Judul Campaign'}</div>
      </div>
      <div>
        <CreatorNewsForm campaignId={campaignId} onSubmit={handleOnSubmit} loading={loading} />
      </div>
    </div>
  );
}
