import Image from 'next/image';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import ReactPaginate from 'react-paginate';
import { useDonationDispatch } from '@/redux/reducers/donation/slices';
import EmptyImage from '@/components/EmptyImage';

export default function CampaignTestimonialShowContainer() {
  const router = useRouter();
  const { campaignId } = router.query;
  const { donation, fetchDonation } = useDonationDispatch();
  const { donations, page, totalPages } = donation;

  useEffect(() => {
    if (campaignId) {
      fetchDonation({ campaignId });
    }
  }, [campaignId]);

  const handlePageChange = async (selectedPage) => {
    await fetchDonation({ campaignId, page: selectedPage.selected });
  };

  const renderDonation = () => donations && donations.map((item) => (
    item.notes && (
    <>
      <div className="flex gap-3 p-5 rounded-lg text-xl border border-gray-150">
        <div className="p-3">
          {item.profileImage && item.profileFullName ? <Image src={item.profileImage} width="106" height="106" className="rounded-full" /> : <EmptyImage h={106} w={106} source="profile" />}
        </div>

        <div className="flex flex-col gap-6 font-semibold w-full justify-center">
          <div className="h-20">
            <div>
              {item.profileFullName ? item.profileFullName : 'Anonim'}
            </div>
            <div className="font-normal">
              {item.notes}
            </div>
          </div>
        </div>
      </div>

    </>
    )
  ));

  return (
    <>
      {donations && donations.length > 0 ? renderDonation() : (
        <div className="flex flex-col justify-center items-center p-4 gap-5">
          <EmptyImage source="campaign" h={320} w={320} />
          <div>Belum ada pesan yang disampaikan</div>
        </div>
      )}

      {totalPages > 1 && (
        <div className="flex justify-center">
          <ReactPaginate
            containerClassName="flex gap-5 mt-5 text-green-250 font-semibold"
            activeClassName="border border-green-250 px-2 rounded-md"
            initialPage={page}
            pageCount={totalPages}
            previousLabel="<"
            nextLabel=">"
            onPageChange={(e) => handlePageChange(e)}
          />
        </div>

      )}
    </>

  );
}
