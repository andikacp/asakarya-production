/* eslint-disable no-nested-ternary */
import { useRouter } from 'next/router';
import ReactPaginate from 'react-paginate';
import Image from 'next/image';
import { useDisbursementDispatch } from '@/redux/reducers/disbursement/slices';
import { useCampaignDispatch } from '@/redux/reducers/campaign/slices';
import { convertDateToString } from '@/helpers/dateTime';
import { sessionUser } from '@/helpers/auth';
import EmptyImage from '@/components/EmptyImage';
import BuktiTransferModal from '@/components/PopupTransfer';

export default function CampaignMonitorDonationContainer() {
  const router = useRouter();
  const token = sessionUser();
  const { campaignId } = router.query;
  const { campaign } = useCampaignDispatch();
  const { campaignDetail } = campaign;
  const { disbursement, fetchDisbursements } = useDisbursementDispatch();
  const {
    disbursements, totalPages, page, loading,
  } = disbursement;

  const handlePageChange = async (selectedPage) => {
    await fetchDisbursements({ campaignId, page: selectedPage.selected, token });
  };

  const renderDisbursement = () => disbursements && disbursements.map((item) => (
    <>
      <div className="flex gap-3 p-3 rounded-lg text-xl border border-gray-150">
        <div className="p-3">
          {item.profileImage ? <Image src={item.profileImage} width="106" height="106" /> : <EmptyImage h={106} w={106} source="profile" />}
        </div>

        <div className="flex flex-col gap-2 font-semibold w-full">
          <div className="flex justify-between w-1/2">
            <div className="flex flex-col gap-2">
              <div className="h-20">
                <div>
                  {item.title}
                </div>
                <div>
                  {item.description}
                </div>
              </div>
            </div>
            <div>
              <div className="flex items-center">
                <BuktiTransferModal
                  item={item}
                />
              </div>
            </div>
          </div>

          <div className="flex flex-grow justify-between w-1/2">
            <div className="font-normal">{convertDateToString(item.updatedAt)}</div>
            <div>
              Rp.
              {' '}
              {(item.amount).toLocaleString('id-ID')}
              ,-
              {' '}
            </div>
          </div>
        </div>
      </div>

    </>

  ));

  const renderCampaignIncome = (item) => {
    const fee = Math.ceil(item.sumDonation * (5 / 100));
    const held = item.sumDonation - item.sumDisbursement - fee;

    return (
      <div className="text-xl flex flex-col gap-3">

        <div className="grid grid-cols-6">
          <div className="font-semibold">Dana Masuk</div>
          <div />

          <div className="col-span-5">Total Donasi</div>
          <div className="flex justify-between">
            <div>
              Rp.
            </div>
            <div>
              {item.sumDonation && item.sumDonation.toLocaleString('id-ID')}
              {' '}
              ,-
            </div>
          </div>

          <div className="col-span-5">Fee 5%</div>
          <div className="flex justify-between">
            <div>
              Rp.
            </div>
            <div>
              {item.sumDonation && fee.toLocaleString('id-ID')}
              {' '}
              ,-
            </div>
          </div>

          <div className="col-span-5">Total Dana Masuk</div>
          <div className="flex justify-between">
            <div>
              Rp.
            </div>
            <div>
              {item.sumDisbursement && item.sumDisbursement.toLocaleString('id-ID')}
              {' '}
              ,-
            </div>
          </div>
        </div>
        <div className="w-full border-black border-b border-dashed" />
        <div className="grid grid-cols-6">
          <div className="col-span-5">Belum Terealisasi</div>
          <div className="flex justify-between">
            <div>
              Rp.
            </div>
            <div>
              {held.toLocaleString('id-ID')}
              {' '}
              ,-
            </div>
          </div>
        </div>
      </div>
    );
  };

  return (
    <div className="container mx-auto p-8 -mt-16 flex flex-col gap-3">
      {renderCampaignIncome(campaignDetail)}
      <div className="font-semibold text-xl mt-5">Riwayat Dana Masuk</div>
      <div>
        {loading ? 'loading' : (
          disbursements && disbursements.length === 0 ? (
            'tidak ada data'
          ) : (
            <div className="flex flex-col gap-8">
              {renderDisbursement()}
            </div>
          )
        )}
        {' '}
        {campaignId && (
          <div className={`${totalPages <= 1 && 'hidden'} flex justify-center`}>
            <ReactPaginate
              containerClassName="flex gap-5 mt-5 text-green-250 font-semibold"
              activeClassName="border border-green-250 px-2 rounded-md"
              initialPage={page}
              pageCount={totalPages}
              previousLabel="<"
              nextLabel=">"
              onPageChange={(e) => handlePageChange(e)}
            />
          </div>
        ) }

      </div>

    </div>
  );
}
