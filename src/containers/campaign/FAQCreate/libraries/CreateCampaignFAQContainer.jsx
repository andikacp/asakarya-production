import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { baseApiUrl } from '@/helpers/baseContents';
import { sessionUser } from '@/helpers/auth';
import { useCampaignDispatch } from '@/redux/reducers/campaign/slices';
import CreatorFAQForm from '@/components/CreatorFAQForm';
import Breadcrumb from '@/components/Breadcrumb';

export default function CreateCampaignFAQContainer() {
  const router = useRouter();
  const { campaignId } = router.query;
  const [loading, setLoading] = useState();

  const {
    fetchCampaignDetail, campaign,
  } = useCampaignDispatch();
  const { campaignDetail } = campaign;

  const token = sessionUser();

  useEffect(() => {
    if (campaignId) {
      fetchCampaignDetail(campaignId);
    }
  }, [campaignId]);

  const pages = [
    { name: 'FAQ', href: `/mycampaign/${campaignId}/faqs`, current: false },
    { name: 'Tambah FAQ', href: '#', current: true },
  ];

  const submitForm = async (values) => {
    setLoading(true);
    try {
      const response = await fetch(`${baseApiUrl}/faq/add/`, {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(values),
      });
      const data = await response.json();
      console.log(data);
    } catch (error) {
      console.log(error);
    }
    setLoading(false);
    router.push(`/mycampaign/${campaignId}/faqs`);
  };

  return (
    <div className="w-3/4 border border-gray-150 rounded-md p-5 overflow-y-auto" style={{ height: 1000 }}>
      <div className="text-xl border-b-2 border-gray-150 pb-2 flex flex-col gap-1">
        <div className="-ml-4">
          <Breadcrumb pages={pages} />
        </div>
        <div>{campaignDetail && campaignDetail.title ? campaignDetail.title : 'Judul Campaign'}</div>
      </div>
      <div>
        <CreatorFAQForm campaignId={campaignDetail.id} loading={loading} onSubmit={submitForm} />
      </div>
    </div>
  );
}
