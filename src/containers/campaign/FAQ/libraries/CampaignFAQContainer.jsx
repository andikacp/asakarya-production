import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { sessionUser } from '@/helpers/auth';
import { baseApiUrl } from '@/helpers/baseContents';
import { useCampaignDispatch } from '@/redux/reducers/campaign/slices';
import LinkButton from '@/components/LinkButton';
import FaqNews from '@/components/PopupFAQ';
import Breadcrumb from '@/components/Breadcrumb';

export default function CampaignFaqContainer() {
  const router = useRouter();
  const { campaignId } = router.query;
  const token = sessionUser();
  const {
    fetchCampaignDetail, fetchFaq, campaign,
  } = useCampaignDispatch();
  const {
    campaignDetail, loading, faq,
  } = campaign;

  useEffect(() => {
    if (campaignId) {
      fetchCampaignDetail(campaignId);
      fetchFaq(campaignId);
    }
  }, [campaignId]);

  const pages = [
    { name: 'FAQ', href: '#', current: true },
  ];

  const handleRemove = async (id) => {
    const response = await fetch(`${baseApiUrl}/faq/delete/${id}`, {
      method: 'Delete',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const data = await response.json();
    console.log(data);
    window.location.reload();
  };

  const renderFaq = () => faq && faq.map((item) => (
    <div key={item.id} className="flex flex-col gap-3 border-b-2 pb-10 my-2 break-words">
      <div className="grid grid-cols-6">
        <div className="col-span-1">
          Pertanyaan
        </div>
        <div className="col-span-4 font-semibold">
          {item.question}
        </div>
        <div className="col-span-1" />
      </div>

      <div className="grid grid-cols-6">
        <div className="col-span-1">
          Jawaban
        </div>
        <div className="col-span-4">
          {item.answer}
        </div>
        <div className="col-span-1 flex flex-col text-right underline">
          <a href={`/mycampaign/${campaignId}/faqs/${item.id}/edit`}>
            <span className="hover:text-green-250 hover:underline cursor-pointer">
              Edit FAQ
            </span>
          </a>
          <FaqNews
            source="FAQ"
            onConfirm={() => handleRemove(item.id)}
          />
        </div>
      </div>

    </div>
  ));

  return (
    <div className="w-3/4 border border-gray-150 rounded-md px-5 overflow-y-auto" style={{ height: 1000 }}>
      <div className="flex border-b-2 border-gray-150 pb-2 sticky top-0 bg-white p-5">
        <div className="text-xl flex flex-grow flex-col gap-1">
          <div className="-ml-4">
            <Breadcrumb pages={pages} />
          </div>
          <div>{campaignDetail && campaignDetail.title ? campaignDetail.title : 'Judul Campaign'}</div>
        </div>
        <LinkButton text="+ Tambah FAQ" url={`/mycampaign/${campaignId}/faqs/add`} />
      </div>
      <div className="flex flex-col p-5 gap-3">

        {loading ? 'loading' : renderFaq()}
      </div>
    </div>
  );
}
