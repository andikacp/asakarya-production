import { useEffect, useState } from 'react';
import Image from 'next/image';
import { useRouter } from 'next/router';
import ReactPaginate from 'react-paginate';
import { sessionUser } from '@/helpers/auth';
import { baseApiUrl } from '@/helpers/baseContents';
import { useCampaignDispatch } from '@/redux/reducers/campaign/slices';
import LinkButton from '@/components/LinkButton';
import FaqNews from '@/components/PopupFAQ';
import Breadcrumb from '@/components/Breadcrumb';

export default function CampaignNewsContainer() {
  const router = useRouter();
  const { campaignId } = router.query;
  const token = sessionUser();
  const {
    fetchCampaignDetail, fetchNews, campaign,
  } = useCampaignDispatch();
  const {
    campaignDetail, loading, news, totalPages, page,
  } = campaign;

  // const token = sessionUser();

  useEffect(() => {
    if (campaignId) {
      fetchCampaignDetail(campaignId);
      fetchNews({ campaignId });
    }
  }, [campaignId]);

  const pages = [
    { name: 'Kabar Terbaru', href: '#', current: true },
  ];

  // useEffect(() => {
  //   if (newsId) {
  //     fetchNews(newsId);
  //   }
  // }, [newsId]);
  const handlePageChange = async (selectedPage) => {
    await fetchNews({ campaignId, page: selectedPage.selected });
  };

  const handleRemove = async (id) => {
    const response = await fetch(`${baseApiUrl}/history/delete/${id}`, {
      method: 'Delete',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const data = await response.json();
    console.log(data);
    window.location.reload();
  };

  const renderNews = () => news && news.map((item) => (
    <div key={item.id} className="grid grid-cols-6 border-b-2 pb-2 my-2 break-words">
      <div className="col-span-1">
        {item.imgUrl ? (<Image src={item.imgUrl} width="170" height="170" className="object-cover bg-gray-50 rounded-lg" />)
          : <div className="flex items-center justify-center border rounded-lg" style={{ width: 170, height: 170 }}>no image found</div>}
      </div>
      <div className="col-span-5 px-3">
        <div className="flex flex-col px-3 gap-2">

          <div className="grid grid-cols-5">
            <div className="col-span-1">
              Judul
            </div>
            <div className="col-span-4 font-semibold">
              {item.title}
            </div>
          </div>

          <div className="grid grid-cols-5">
            <div className=" col-span-1 ">
              Deskripsi
            </div>
            <div className="col-span-3">
              {item.activity}
            </div>
            <div className="col-span-1 flex flex-col text-right underline">
              <a href={`/mycampaign/${campaignId}/news/${item.id}/edit`}>
                <span className="hover:text-green-250 hover:underline cursor-pointer">
                  Edit Kabar
                </span>
              </a>
              <FaqNews
                source="Kabar"
                onConfirm={() => handleRemove(item.id)}
              />
            </div>
          </div>

        </div>

      </div>

    </div>
  ));

  return (
    <div className="w-3/4 border border-gray-150 rounded-md overflow-y-auto px-5" style={{ height: 1000 }}>
      <div className="flex border-b-2 border-gray-150 pb-2 sticky top-0 bg-white p-5">
        <div className="text-xl flex flex-grow flex-col gap-1">
          <div className="-ml-4"><Breadcrumb pages={pages} /></div>
          <div>{campaignDetail && campaignDetail.title ? campaignDetail.title : 'Judul Campaign'}</div>
        </div>
        <LinkButton text="+ Tambah Kabar Terbaru" url={`/mycampaign/${campaignId}/news/add`} />
      </div>
      <div className="flex flex-col p-5 gap-3">

        {loading ? 'loading' : renderNews()}
        {totalPages <= 1 ? null : (
          <ReactPaginate
            containerClassName="flex gap-5 mt-5 text-green-250 font-semibold"
            activeClassName="border border-green-250 px-2 rounded-md"
            initialPage={page}
            pageCount={totalPages}
            previousLabel="<"
            nextLabel=">"
            onPageChange={(e) => handlePageChange(e)}
          />
        )}
      </div>
    </div>
  );
}
