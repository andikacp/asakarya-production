/* eslint-disable no-plusplus */
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { useCampaignDispatch } from '@/redux/reducers/campaign/slices';
import { baseApiUrl } from '@/helpers/baseContents';
import { sessionUser } from '@/helpers/auth';
import CreatorCampaignForm from '@/components/CreatorCampaignForm';
import Breadcrumb from '@/components/Breadcrumb';

export default function CreateCampaignContainer() {
  const router = useRouter();
  const { campaign, fetchCategories } = useCampaignDispatch();
  const { categories } = campaign;
  const [loading, setLoading] = useState();

  const token = sessionUser();

  useEffect(() => {
    fetchCategories();
  }, []);

  const pages = [
    { name: 'Campaign Saya', href: '/mycampaign', current: false },
    { name: 'Campaign Baru', href: '#', current: true },
  ];

  const handleUploadImage = async (values) => {
    if (!values.imgFile) return console.log('tanpa gambar');
    const formData = new FormData();
    formData.append('file', values.imgFile);
    console.log(formData);
    if (token) {
      try {
        const response = await fetch(`${baseApiUrl}/file/upload`, {
          method: 'POST',
          body: formData,
        });
        const data = await response.json();
        const fileUri = await data.fileDownloadUri;
        return fileUri;
        // console.log(data);
      } catch (error) {
        console.log(error);
      }
    } return null;
  };

  const submitForm = async (values) => {
    console.log(values);
    const {
      title, description, fundAmount, due, location, categoryId, imgUrl,
    } = values;

    const formData = {
      title,
      description,
      fundAmount,
      due,
      location,
      categoryId,
      imgUrl,
    };

    try {
      const response = await fetch(`${baseApiUrl}/campaign/add`, {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });
      const data = await response.json();
      console.log(data);
      return data.id;
    } catch (error) {
      console.log(error);
      return null;
    }
  };

  const rewardImagesObj = (values) => {
    const imageObj = {};
    if (values.img1) imageObj.img1 = values.img1;
    if (values.img2) imageObj.img2 = values.img2;
    if (values.img3) imageObj.img3 = values.img3;
    return imageObj;
  };

  const uploadRewardImages = async (images) => {
    const len = Object.keys(images).length;

    // return jika image kosong
    if (len === 0) return console.log('tidak ada gambar');

    const imageUpload = new FormData();
    for (let i = 0; i < len; i++) {
      imageUpload.append('files', Object.values(images)[i]);
    }
    try {
      const response = await fetch(`${baseApiUrl}/file/upload-multiple`, {
        method: 'POST',
        body: imageUpload,
      });
      const data = await response.json();
      console.log('data: ', data);

      // objek untuk nyimpan hasil hit api
      const imagesWithUri = {};
      for (let i = 0; i < len; i++) {
        const key = Object.keys(images)[i];
        console.log(key);
        imagesWithUri[key] = data[i].fileDownloadUri;
      }
      console.log('imageUri: ', imagesWithUri);
      return imagesWithUri;
    } catch (error) {
      console.log(error);
      return null;
    }
  };

  const submitRewardForm = async (values, id, images) => {
    const formData = [];
    const imgData = {};

    if ('img1' in images) {
      imgData.img1 = images.img1;
    }
    if ('img2' in images) {
      imgData.img2 = images.img2;
    }
    if ('img3' in images) {
      imgData.img3 = images.img3;
    }

    if (values.item1 || imgData.img1) {
      formData.push(
        {
          rewardTypeId: 1,
          campaignId: id,
          item: values.item1,
          imgUrl: imgData.img1,
        },
      );
    }
    if (values.item2 || imgData.img2) {
      formData.push(
        {
          rewardTypeId: 2,
          campaignId: id,
          item: values.item2,
          imgUrl: imgData.img2,
        },
      );
    }
    if (values.item3 || imgData.img3) {
      formData.push(
        {
          rewardTypeId: 3,
          campaignId: id,
          item: values.item3,
          imgUrl: imgData.img3,
        },
      );
    }
    if (formData.length === 0) return console.log('tidak ada reward');
    // console.log('img1 data: ', imgData.img1);
    // console.log('raw : ', formData);
    // console.log('stringified : ', JSON.stringify(formData));
    try {
      const response = await fetch(`${baseApiUrl}/reward/add`, {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });
      const data = await response.json();
      console.log(data);
      return null;
    } catch (error) {
      console.log(error);
      return null;
    }
  };

  const handleOnSubmit = async (values) => {
    setLoading(true);
    const imgUrl = await handleUploadImage(values);
    const campaignId = await submitForm({ ...values, imgUrl });
    if (values.item1 || values.item2 || values.item3) {
      const rewardUri = rewardImagesObj(values);
      const imagesUri = await uploadRewardImages(rewardUri);
      console.log('hit api upload reward image: ', imagesUri);
      if (imagesUri) {
        await submitRewardForm(values, campaignId, imagesUri);
      } else {
        await submitRewardForm(values, campaignId);
      }
    }
    setLoading(false);
    router.push(('/mycampaign'));
    // redirect ke list campaign
  };

  return (
    <div className="w-3/4 border border-gray-150 rounded-md p-5 overflow-y-auto" style={{ height: 1000 }}>
      <div className="text-xl border-b-2 border-gray-150 pb-2 flex flex-col gap-1">
        <div className="-ml-4">
          <Breadcrumb pages={pages} />
        </div>

        <div>Kelola informasi campaign anda</div>
      </div>
      <div>
        <CreatorCampaignForm
          onSubmit={handleOnSubmit}
          loading={loading}
          categories={categories}
        />
      </div>
    </div>
  );
}
