/* eslint-disable max-len */
/* eslint-disable react/no-array-index-key */
import Image from 'next/image';
import { useState, useEffect } from 'react';
import ReactPaginate from 'react-paginate';
import SearchBar from '@/components/SearchBar';
import CampaignListView from '@/components/CampaignListView';
import CampaignGridView from '@/components/CampaignGridView';
import { useCampaignDispatch } from '@/redux/reducers/campaign/slices';

export default function CampaignContainer() {
  const [gridView, setGridView] = useState(true);
  const [category, setCategory] = useState();
  const [query, setQuery] = useState();
  const {
    fetchCampaigns, fetchCategories, campaign,
  } = useCampaignDispatch();
  const {
    campaigns, loading, totalPages, page, categories,
  } = campaign;

  useEffect(() => {
    fetchCampaigns({ ctg: category, query });
  }, [category]);

  useEffect(() => {
    fetchCampaigns({ ctg: category, query });
  }, [query]);

  const handleCategoryClick = async (e) => {
    if (String(e.target.id) === String(0)) {
      setCategory('');
    } else {
      setCategory(e.target.id);
    }
  };

  const handleSwitchView = () => {
    if (gridView === true) {
      setGridView(false);
    } else {
      setGridView(true);
    }
  };

  const handlePageChange = async (selectedPage) => {
    await fetchCampaigns({ ctg: category, page: selectedPage.selected, query });
  };

  const handleSearchButton = (value) => {
    setQuery(value.query);
  };

  const renderCategories = () => categories && categories.map((item) => (
    <button
      key={item.id}
      id={item.id}
      type="button"
      className={`${String(category) === String(item.id) ? ' bg-yellow-550 mx-2 px-5 rounded-lg text-white font-semibold h-14 md:text-md lg:text-xl' : 'bg-gray-200 hover:bg-gray-400 mx-2 px-5 rounded-lg h-14 md:text-md lg:text-xl'}`}
      onClick={handleCategoryClick}
    >
      {item.categoryName}
    </button>
  ));

  const renderCampaign = () => campaigns && campaigns.map((item) => (
    <div key={`campaign-${item.id}`}>
      {gridView ? <CampaignGridView {...item} /> : <CampaignListView detailed={false} {...item} />}
    </div>
  ));

  useEffect(async () => {
    await fetchCategories();
    await fetchCampaigns({});
  }, []);

  return (

    <div className="container mx-auto p-12 flex flex-col justify-center items-center">
      <SearchBar handleOnSubmit={(e) => handleSearchButton(e)} />
      <div className="flex justify-center text-sm my-3 gap-3 overflow-x-auto">
        <button type="button" onClick={handleSwitchView}>
          {gridView ? <Image src="/img/gallery/choose-grid.png" width="72" height="50" alt="Preview" /> : <Image src="/img/gallery/choose-tile.png" width="72" height="50" alt="Preview" /> }
        </button>
        <img src="img/gallery/line.png" alt="" className="ml-1" />
        <div className="w-full flex flex-start ">
          {renderCategories()}
        </div>

      </div>
      <h2 className="text-4xl my-10 font-semibold sm:hidden md:inline">
        Mereka
        {' '}
        <span className="text-green-250">butuh</span>
        {' '}
        dukunganmu
      </h2>

      {loading ? (
        <div>loading</div>
      ) : (
        <div className={gridView ? 'md:flex md:flex-col lg:grid lg:grid-cols-3 gap-16 mt-5' : 'flex flex-col gap-16 mt-5 md:hidden lg:inline'}>
          {renderCampaign()}
        </div>
      ) }

      {loading === false && campaigns && campaigns.length === 0 ? (
        <div>
          Data tidak ditemukan
        </div>
      ) : (
        <div>
          <ReactPaginate
            containerClassName="flex gap-5 mt-5 text-green-250 font-semibold"
            activeClassName="border border-green-250 px-2 rounded-md"
            initialPage={page}
            pageCount={totalPages}
            previousLabel="<"
            nextLabel=">"
            onPageChange={(e) => handlePageChange(e)}
          />
        </div>

      )}

    </div>

  );
}
