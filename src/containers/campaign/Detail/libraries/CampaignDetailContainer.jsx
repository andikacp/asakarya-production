/* eslint-disable max-len */
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useCampaignDispatch } from '@/redux/reducers/campaign/slices';
import CampaignListView from '@/components/CampaignListView';
import NavbarDetail from '@/components/NavbarDetail';
import Breadcrumb from '@/components/Breadcrumb/libraries/Breadcrumb';

export default function CampaignDetailContainer() {
  const router = useRouter();
  const { campaignId } = router.query;
  const {
    fetchCampaigns, fetchCampaignDetail, fetchRewards, campaign,
  } = useCampaignDispatch();
  const { campaignDetail } = campaign;

  useEffect(() => {
    fetchCampaigns({ ctg: campaignDetail.categoryId, sortBy: 'due', sortType: 'asc' });
  }, [campaignDetail]);

  useEffect(() => {
    if (campaignId) {
      fetchCampaignDetail(campaignId);
      fetchRewards(campaignId);
    }
  }, [campaignId]);

  const pages = [
    { name: 'Campaign', href: '/campaign', current: false },
    { name: campaignDetail.title, href: '#', current: true },
  ];

  return (
    <>
      <div className="-ml-4 -mb-4">
        <Breadcrumb pages={pages} />
      </div>
      <h1 className="text-4xl text-green-550 font-semibold text-center">
        Detail Campaign
      </h1>

      {campaignDetail && campaignDetail.id ? <CampaignListView description="" buttonText="Donasi Sekarang" {...campaignDetail} /> : <div className="text-center">loading</div>}

      <NavbarDetail />
    </>

  );
}
