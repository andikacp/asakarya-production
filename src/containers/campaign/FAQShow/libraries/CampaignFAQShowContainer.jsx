import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useCampaignDispatch } from '@/redux/reducers/campaign/slices';
import EmptyImage from '@/components/EmptyImage';

export default function CampaignFAQShowContainer() {
  const router = useRouter();
  const { campaignId } = router.query;
  const { campaign, fetchFaq } = useCampaignDispatch();
  const { faq } = campaign;

  useEffect(() => {
    if (campaignId) {
      fetchFaq(campaignId);
    }
  }, [campaignId]);

  const renderFaq = () => faq && faq.map((item) => (
    <div key={item.id} className="flex flex-col gap-3 border-b-2 py-5 my-2 break-words">
      <div className="grid grid-cols-6">
        <div className="col-span-1">
          Pertanyaan
        </div>
        <div className="col-span-4 font-semibold">
          {item.question}
        </div>
        <div className="col-span-1" />
      </div>

      <div className="grid grid-cols-6">
        <div className="col-span-1">
          Jawaban
        </div>
        <div className="col-span-4">
          {item.answer}
        </div>
      </div>

    </div>
  ));

  return (
    <>
      {faq && faq.length > 0 ? (
        <div className="px-8 -mt-4">

          { renderFaq()}

        </div>
      ) : (
        <div className="flex flex-col justify-center items-center p-4 gap-5">
          <EmptyImage source="campaign" h={320} w={320} />
          <div>Creator belum menambahkan FAQ pada campaign ini</div>
        </div>
      )}

    </>

  );
}
