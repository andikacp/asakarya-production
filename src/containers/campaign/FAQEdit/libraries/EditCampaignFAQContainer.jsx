import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { baseApiUrl } from '@/helpers/baseContents';
import { sessionUser } from '@/helpers/auth';
import { useCampaignDispatch } from '@/redux/reducers/campaign/slices';
import CreatorFAQForm from '@/components/CreatorFAQForm';
import Breadcrumb from '@/components/Breadcrumb';

export default function EditCampaignFAQContainer() {
  const router = useRouter();
  const { campaignId, faqId } = router.query;
  const [loading, setLoading] = useState();

  const {
    fetchCampaignDetail, fetchFaqDetail, campaign,
  } = useCampaignDispatch();
  const { campaignDetail, faqDetail } = campaign;

  const token = sessionUser();

  useEffect(() => {
    if (campaignId) {
      fetchCampaignDetail(campaignId);
    }
  }, [campaignId]);

  useEffect(() => {
    if (faqId) {
      fetchFaqDetail(faqId);
    }
  }, [faqId]);

  const pages = [
    { name: 'FAQ', href: `/mycampaign/${campaignId}/faqs`, current: false },
    { name: 'Edit FAQ', href: '#', current: true },
  ];

  const submitForm = async (values) => {
    setLoading(true);
    try {
      const response = await fetch(`${baseApiUrl}/faq/update`, {
        method: 'PUT',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(values),
      });
      const data = await response.json();
      console.log(data);
    } catch (error) {
      console.log(error);
    }
    setLoading(false);
    router.push(`/mycampaign/${campaignId}/faqs`);
  };

  return (
    <div className="w-3/4 border border-gray-150 rounded-md p-5 overflow-y-auto" style={{ height: 1000 }}>
      <div className="text-xl border-b-2 border-gray-150 pb-2 flex flex-col gap-1">
        <div className="-ml-4">
          <Breadcrumb pages={pages} />
        </div>
        <div>{campaignDetail && campaignDetail.title ? campaignDetail.title : 'Judul Campaign'}</div>
      </div>
      <div>
        {faqDetail && faqDetail.id ? <CreatorFAQForm campaignId={campaignDetail.id} loading={loading} onSubmit={submitForm} {...faqDetail} /> : (<div className="ml-7 mt-7">loading</div>)}

      </div>
    </div>
  );
}
