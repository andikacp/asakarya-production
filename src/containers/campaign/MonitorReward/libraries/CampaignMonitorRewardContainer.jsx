/* eslint-disable no-nested-ternary */
import { useRouter } from 'next/router';
import Image from 'next/image';
import { useEffect } from 'react';
import { useDonationDispatch } from '@/redux/reducers/donation/slices';
import { useCampaignDispatch } from '@/redux/reducers/campaign/slices';
// import { convertDateToString } from '@/helpers/dateTime';
import { sessionUser } from '@/helpers/auth';
import InformationAddressModal from '@/components/PopupInformation';
import { baseApiUrl } from '@/helpers/baseContents';
import Reward from '@/components/Reward';

export default function CampaignMonitorDonationContainer() {
  const router = useRouter();
  const token = sessionUser();
  const { campaignId } = router.query;
  const { campaign } = useCampaignDispatch();
  const { rewards } = campaign;
  const { donation, fetchDonationReward } = useDonationDispatch();
  const {
    donationRewards, loading,
  } = donation;

  useEffect(() => {
    if (campaignId) {
      fetchDonationReward({ campaignId, token });
    }
  }, [campaignId]);

  const handleConfirm = async (id) => {
    try {
      const response = await fetch(`${baseApiUrl}/donation-reward/deliver/${id}`, {
        method: 'PUT',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      const data = await response.message;
      console.log(data);
    } catch (error) {
      console.log(error);
    }
    window.location.reload();
  };

  const renderRewards = () => rewards && rewards.map((item) => (
    <Reward hideBase {...item} key={`reward-${item.id}`} />
  ));

  const renderDonation = () => donationRewards && donationRewards.map((item) => (
    <>
      <div className="flex gap-3 p-5 rounded-lg text-xl border border-gray-150" key={item.id}>
        <div className="p-3">
          {item.profileImage ? <Image src={item.profileImage} width="106" height="106" /> : <div className="flex justify-center items-center" style={{ width: 106, height: 106 }}> no image found</div>}
        </div>

        <div className="flex flex-col gap-2 font-semibold w-2/3">
          <div className="h-20">
            <div>
              {item.recipient}
            </div>
            <div>
              Paket
              {' '}
              {item.rewardType}
              -
              {item.rewardItem}
            </div>
          </div>
          <InformationAddressModal
            detail={item}
            onConfirm={() => handleConfirm(item.id)}
            delivered={item.delivered}
          />

        </div>
        <div className="flex items-center text-semibold">
          {item.delivered
            ? (
              <>
                <Image src="/img/donation/berhasil.svg" width={25} height={25} />
                <div className="font-semibold ml-3">Sudah Dikirim</div>
              </>

            )
            : (
              <>
                <Image src="/img/donation/diproses.svg" width={25} height={25} />
                <div className="font-semibold ml-3">Perlu Dikirim</div>
              </>
            )}
        </div>
      </div>

    </>

  ));

  return (
    <div className="container mx-auto p-8 -mt-16 flex flex-col gap-3">
      <div className="flex flex-col gap-5 justify-center items-center border-b-2 pb-14">
        <div className="text-xl font-semibold text-green-550">Penawaran Hadiah Donasi</div>
        <div className="flex gap-8">
          {renderRewards()}
        </div>
      </div>
      <div className="font-semibold text-xl mt-5">Riwayat Donasi</div>
      <div>
        {loading ? 'loading' : (
          donationRewards && donationRewards.length === 0 ? (
            'tidak ada data'
          ) : (
            <div className="flex flex-col gap-8">
              {renderDonation(donation)}
            </div>
          )
        )}
        {' '}

      </div>

    </div>
  );
}
