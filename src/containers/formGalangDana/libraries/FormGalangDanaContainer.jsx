import { useState, useEffect } from 'react';
// import { useRouter } from 'next/router';
import { sessionUser } from '@/helpers/auth.js';
import { baseApiUrl } from '@/helpers/baseContents';
import CreatorProfileForm from '@/components/CreatorProfileForm';
import { withAuth } from '@/layouts/auth/libraries/Auth';
import { useCreatorDispatch } from '@/redux/reducers/creator/slices';
import { useUserDispatch } from '@/redux/reducers/user/slices';

function FormGalangDanaContainer() {
  const token = sessionUser();
  const [loading, setLoading] = useState();
  // const router = useRouter();
  const { creator, getCreatorDetail } = useCreatorDispatch();
  const { creatorDetail } = creator;

  const { getUserDetail } = useUserDispatch();

  useEffect(async () => {
    if (creatorDetail) {
      await getCreatorDetail();
    }
  }, []);

  const uploadKtp = async (values) => {
    const formData = new FormData();
    formData.append('file', values.ktpUrl);
    // console.log('file', values.ktpUrl);
    if (token) {
      try {
        const response = await fetch(`${baseApiUrl}/file/upload`, {
          method: 'POST',
          body: formData,
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        const data = await response.json();
        const fileUri = await data.fileDownloadUri;
        return fileUri;
      // console.log(data);
      // const file = data.fileDownloadUri;
      // setFileData(file);
      // console.log(file);
      } catch (error) {
        console.log(error);
        return null;
      }
    }
    return null;
  };

  const submitForm = async (values) => {
    console.log(values);
    const {
      ktpUrl,
      organization,
      occupation,
      socialMedia, socialMediaAccount, bio, nik, bankName, bankAccount, bankAccountName,
    } = values;

    const formData = {
      ktpUrl,
      organization,
      occupation,
      socialMedia,
      socialMediaAccount,
      bio,
      nik,
      bankName,
      bankAccount,
      bankAccountName,
    };
    console.log(formData);
    try {
      const response = await fetch(`${baseApiUrl}/creator/register`, {
        method: 'POST',
        body: JSON.stringify(formData),
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      });
      const data = await response.json();
      console.log(data);
    } catch (error) {
      console.log(error);
    }
  };

  const updateForm = async (values) => {
    const {
      ktpUrl,
      organization,
      occupation,
      socialMedia, socialMediaAccount, bio, nik, bankName, bankAccount, bankAccountName,
    } = values;
    const formData = {
      ktpUrl,
      organization,
      occupation,
      socialMedia,
      socialMediaAccount,
      bio,
      nik,
      bankName,
      bankAccount,
      bankAccountName,
    };
    try {
      const response = await fetch(`${baseApiUrl}/creator/update`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(formData),
      });
      const data = await response.json();
      console.log(data);
    } catch (error) {
      console.log(error);
    }
  };

  const handleOnSubmit = async (values) => {
    setLoading(true);
    const ktpUrl = await uploadKtp(values);

    if (!creatorDetail) {
      await submitForm({ ...values, ktpUrl });
      await getUserDetail();
      window.location.href = '/profile';
    } else if (ktpUrl) {
      await updateForm({ ...values, ktpUrl });
    } else {
      await updateForm(values);
    }
    // await updateForm({ ...values, ktpUrl });

    console.log(values);
    setLoading(false);
  };

  return (
    <>
      <div className="mx-auto container">
        <div className="flex justify-center items-center">
          <div className="border rounded-lg border-gray-300 bg-white my-8 w-10/12 h-auto">
            <div className="w-full h-full my-6">
              <div className="text-center text-xl font-semibold">Profil Kreator</div>
              <div className="text-center text-xl">Kelola informasi profil Kreator Anda</div>
              <div className="w-auto my-4 mx-8 border-t-2">
                <CreatorProfileForm
                  onSubmit={(values) => handleOnSubmit(values)}
                  loading={loading}
                  {...creatorDetail}
                />
              </div>

            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default withAuth(FormGalangDanaContainer);
