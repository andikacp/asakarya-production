/* eslint-disable max-len */
/* eslint-disable no-console */

import Image from 'next/image';
import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { Formik } from 'formik';
import Upload from '@/components/Upload';
import { sessionUser } from '@/helpers/auth';
import { usePaymentDispatch } from '@/redux/reducers/payment/slices';
import { baseApiUrl } from '@/helpers/baseContents';
import Thankyou from '@/components/PopupThankyou';
import Breadcrumb from '@/components/Breadcrumb';
import { useCampaignDispatch } from '@/redux/reducers/campaign/slices';

export default function PaymentContainer() {
  const [isCopied, setIsCopied] = useState(false);
  const [loading, setLoading] = useState(false);

  const copyText = '128029520392340';
  useEffect(() => {
    window.onbeforeunload = function () {
      return 'Halo';
    };
  }, []);

  async function copyTextToClipboard(text) {
    if ('clipboard' in navigator) {
      const x = await navigator.clipboard.writeText(text);
      return x;
    }
    return document.execCommand('copy', true, text);
  }

  async function handleCopyClick() {
    try {
      await copyTextToClipboard(copyText);
      setIsCopied(true);
      setTimeout(() => {
        setIsCopied(false);
      }, 1500);
      console.log(copyText);
    } catch (error) {
      console.log(error);
      setIsCopied(false);
    }
  }

  const router = useRouter();
  const { donationId, campaignId } = router.query;
  const { payment, getDonations } = usePaymentDispatch();
  const { donations } = payment;
  const { fetchCampaignDetail, campaign } = useCampaignDispatch();
  const { campaignDetail } = campaign;

  useEffect(() => {
    if (campaignId) {
      fetchCampaignDetail(campaignId);
    }
  }, [campaignId]);

  const [showThankyou, setShowThankyou] = useState(false);
  // const [errorFormat, setErrorFormat] = useState(false);

  const token = sessionUser();

  useEffect(() => {
    if (donationId) {
      getDonations(donationId);
    }
  }, [donationId]);

  // eslint-disable-next-line consistent-return
  async function uploadPayment(values) {
    const formData = new FormData();
    formData.append('file', values.paymentSlipUrl);

    if (token) {
      try {
        const response = await fetch(`${baseApiUrl}/file/upload`, {
          method: 'POST',
          body: formData,
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        const data = await response.json();
        // console.log(data);
        const fileUri = await data.fileDownloadUri;
        return fileUri;
      } catch (error) {
        console.log(error);
      }
    } else {
      try {
        const response = await fetch(`${baseApiUrl}/file/upload`, {
          method: 'POST',
          body: formData,
        });
        const data = await response.json();
        console.log(data);
        const fileUri = await data.fileDownloadUri;
        return fileUri;
      } catch (error) {
        console.log(error);
      }
    }
  }

  const submitPayment = async (Uri) => {
    const paymentSlipUrl = Uri;
    const formData = { id: donationId, paymentSlipUrl };
    // console.log(paymentSlipUrl);

    setLoading(true);
    if (token) {
      try {
        const response = await fetch(`${baseApiUrl}/donation/update`, {
          method: 'PUT',
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        });
        const data = await response.json();
        console.log(data);
        setLoading(false);
      } catch (error) {
        console.log(error);
        setLoading(false);
      }
    } else {
      try {
        const response = await fetch(`${baseApiUrl}/donation/update`, {
          method: 'PUT',
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        });
        const data = await response.json();
        console.log('submit', data);
        setLoading(false);
        // if (data.code === 200) {
        //   setShowThankyou(true);
        // }
      } catch (error) {
        console.log(error);
        setLoading(false);
      }
    }
  };

  const handleOnSubmit = async (values) => {
    const imgUrl = await uploadPayment(values);
    console.log(imgUrl);
    if (imgUrl) {
      await submitPayment(imgUrl);
    }
    setShowThankyou(true);
    console.log(values, imgUrl);
  };

  useEffect(() => {
    console.log(showThankyou);
  }, [showThankyou]);

  const handleCloseModal = () => {
    setShowThankyou(false);
    router.push('/campaign');
  };

  const pages = [
    { name: 'Campaign', href: '/campaign', current: false },
    { name: campaignDetail.title, href: `/campaign/${campaignId}/detail`, current: false },
    { name: 'Detail Donasi', href: `/campaign/${campaignId}/donation`, current: false },
    { name: 'Pembayaran', href: '#', current: true },
  ];

  return (
    <>
      <div className="container mx-auto">
        <Breadcrumb pages={pages} />

        <div className="pt-6">
          <Formik
            initialValues={{
              id: donationId,
              paymentSlipUrl: {},
            }}
            onSubmit={(values) => handleOnSubmit(values)}
          >
            {({
              setFieldValue,
              handleSubmit,
            }) => (
              <form onSubmit={handleSubmit}>
                <div className="grid grid-cols-3 px-20 xl:gap-10 gap-5 justify-center">
                  <div className="col-span-1 flex items-center justify-center flex-col">
                    <div>
                      <Image
                        src="/img/payment/qr.png"
                        alt="QR code"
                        width={316}
                        height={316}
                      />
                    </div>
                    <div className="flex justify-center">
                      <Image
                        src="/img/payment/qris.png"
                        alt="QRIS"
                        width={102}
                        height={32.5}
                      />
                    </div>
                  </div>
                  <div className="col-span-2 flex flex-col items-left px-20">
                    <div className="py-5 flex-row">
                      <div className="float-left">
                        <Image
                          src="/img/payment/bca.png"
                          alt="bank bca"
                          width={140}
                          height={42}
                        />
                      </div>
                      <div className="text-lg text-black float-right">PT. Asakarya Membangun Negeri</div>
                    </div>
                    <div className="py-5 flex-row">
                      <div className="float-left">
                        <div className="text-md text-black">No. Rekening</div>
                        <div className="text-md font-semibold text-green-350">128 0295 2039 2340</div>
                      </div>
                      <button type="button" onClick={handleCopyClick} className="cursor-pointer text-lg text-green-250 float-right font-semibold hover:text-green-550">{isCopied ? 'Tersalin!' : 'Salin'}</button>
                    </div>
                    <div className="py-5">
                      <div className="float-left">
                        <div className="text-md text-black">Nominal Pembayaran</div>
                        {/* ambil dari API */}
                        <div className="text-md font-semibold text-green-350">
                          <span>Rp  </span>
                          <span>{donations.amount}</span>
                        </div>
                      </div>
                      <div className="text-lg text-green-250 float-right font-semibold hover:text-green-550 cursor-pointer">Lihat Detail</div>
                    </div>
                    {/* upload bukti */}
                    <Upload
                      title="Upload Bukti Pembayaran"
                      name="paymentSlipUrl"
                      accept=".jpg, .png, .jpeg"
                      onChange={(event) => {
                        setFieldValue('file', event.currentTarget.files[0]);
                      }}
                      // required
                    />
                    <div className="text-sm text-gray-225 mx-auto pt-5 text-center">
                      <div>Ukuran gambar maks 2 MB</div>
                      <div>Format gambar: .JPG, .PNG., .PDF</div>
                    </div>
                  </div>
                </div>
                <div className="py-10 px-10">
                  <div className="text-lg font-bold">Cara Pembayaran</div>
                  <div className="text-lg">
                    1. Masuk ke akun rekening anda melalui ATM/M-Banking lalu pilih menu
                    {' '}
                    <span className="font-bold">transfer.</span>
                  </div>
                  <div className="text-lg">
                    2. Masukan
                    {' '}
                    <span className="font-bold">nomor rekening</span>
                    {' '}
                    dan
                    {' '}
                    <span className="font-bold">nominal </span>
                    {' '}
                    pembayaran yang sudah yang sudah diinput sebelumnya.
                  </div>
                  <div className="text-lg">
                    3.
                    {' '}
                    <span className="font-bold">Foto/screenshot</span>
                    {' '}
                    bukti pembayaran yang sudah dilakukan
                  </div>
                  <div className="text-lg">
                    4.
                    {' '}
                    <span className="font-bold">Upload bukti pembayaran</span>
                    , status pembayaran akan berubah
                    {' '}
                    <span className="font-bold">max. 2x24 jam.</span>
                  </div>
                </div>
                <div className="flex justify-center items-center">
                  <button
                    type="submit"
                    className="bg-green-250 border-none rounded-lg text-white py-4 px-5  mb-10 transition duration-500 ease-in-out hover:bg-green-550 transform hover:-translate-y-1 hover:scale-110"
                    disabled={loading && showThankyou}
                  >
                    {loading ? 'Mohon Tunggu...' : 'Konfirmasi Pembayaran'}
                  </button>
                  <Thankyou modalIsOpen={showThankyou} handleOnClose={handleCloseModal} />
                </div>
              </form>
            )}
          </Formik>
        </div>
      </div>
    </>
  );
}
