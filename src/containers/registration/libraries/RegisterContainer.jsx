/* eslint-disable max-len */
/* eslint-disable no-console */
import { Formik } from 'formik';
import { useState } from 'react';
import { useRouter } from 'next/router';
import { FaEye, FaEyeSlash } from 'react-icons/fa';
import * as Yup from 'yup';
import Link from 'next/link';
import Image from 'next/image';
import Input from '@/components/Input';
import { baseApiUrl } from '@/helpers/baseContents';

const initialValues = {
  fullName: '',
  username: '', // email
  password: '',
  passwordConfirmation: '',
  phone: '',
};

const validationSchema = Yup.object({
  username: Yup.string()
    .email('Invalid email address')
    .required('This must be filled'),
  password: Yup.string()
    .matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/, 'Password must contain at least 8 characters and at least one number and one capital letter')
    .required('This must be filled'),
  passwordConfirmation: Yup.string()
    .oneOf([Yup.ref('password'), null], 'Password must match'),
  fullName: Yup.string()
    .min(3, 'Must be 3 characters or more')
    .required('This must be filled'),
  phone: Yup.string()
    .matches(/^(^\+62|62|^08)(\d{3,4}-?){2}\d{3,4}$/, 'Phone number is not valid')
    .min(8)
    .required('This must be filled'),
});

export default function RegisterContainer() {
  const [showError, setShowError] = useState(false);
  const [loading, setLoading] = useState(false);

  const router = useRouter();

  const handleOnSubmit = async (values) => {
    setLoading(true);
    try {
      const response = await fetch(`${baseApiUrl}/user/register`, {
        method: 'POST',
        body: JSON.stringify(values),
        headers: {
          'Content-Type': 'application/json',
        },
      });
      const data = await response.json();
      console.log(data);
      setLoading(false);
      if (data.code === '201') {
        router.push('/login');
        setShowError(false);
        setLoading(false);
      }
    } catch (error) {
      setShowError(true);
      console.log(error);
      setLoading(false);
    }
  };
  const [showPassword, setShowPassword] = useState(false);
  const togglePassword = () => {
    setShowPassword(!showPassword);
  };

  const [showPasswordConf, setShowPasswordConf] = useState(false);
  const togglePasswordConf = () => {
    setShowPasswordConf(!showPassword);
  };

  return (
    <div className="2xl:w-1/3 xl:w-1/3 sm:w-2/3 mx-auto pb-20">
      <div className="box-border">
        <div className="flex justify-center pt-5 pb-2">
          <Image src="/img/logo/logo.png" alt="logo" width="61" height="72" />
        </div>
        <div className="flex justify-center pb-5">
          <Image src="/img/logo/asakarya-text-logo.png" alt="asakarya" width="250" height="46.67" />
        </div>
      </div>
      <div className="flex justify-center items-center border rounded-lg border-gray-300 bg-white filter drop-shadow-md">
        <div className="w-full">
          <div className="w-full flex justify-center border-b border-gray-300 py-3">
            <div className="text-sm">
              Sudah Punya Akun?
              <Link href="/login">
                <a className="ml-2 underline text-green-350">Masuk</a>
              </Link>
            </div>
          </div>
          <h1 className="text-xl font-bold flex justify-center pt-3 mt-2">Daftar</h1>
          <div className="mx-6 mt-3 sm:text-xs md:text-lg">
            <Formik
              initialValues={initialValues}
              onSubmit={handleOnSubmit}
              validationSchema={validationSchema}
              validateOnMount
            >
              {({
                handleSubmit,
                handleChange,
                handleBlur,
                isValid,
                dirty,
                touched,
                errors,
                values,
              }) => (
                <form onSubmit={handleSubmit}>
                  <div className="w-full">
                    <div className="text-md text-black font-bold py-1">Nama</div>
                    <Input
                      title="Name"
                      id="fullName"
                      name="fullName"
                      placeholder="Masukan Nama Anda"
                      required
                    />
                    <div className={`box-border py-1 pb-2 text-xs ${touched.fullName && errors.fullName ? 'text-red-700' : ''}`}>
                      {touched.fullName && errors.fullName && errors.fullName}
                    </div>
                  </div>
                  <div className="w-full">
                    <div className="text-md text-black font-bold py-1">Email</div>
                    <Input
                      title="Email"
                      id="username" // email
                      name="username" // email
                      placeholder="Masukan alamat email Anda"
                      type="email"
                      required
                    />
                    <div className={`box-border py-1 pb-2 text-xs ${touched.username && errors.username ? 'text-red-700' : ''}`}>
                      {touched.username && errors.username && errors.username}
                    </div>
                  </div>
                  <div className="w-full">
                    <div className="text-md text-black font-bold py-1">Nomor Telepon</div>
                    <Input
                      title="phone"
                      id="phone"
                      name="phone"
                      placeholder="Masukan nomor telepon Anda"
                      required
                    />
                    <div className={`box-border py-1 pb-2 text-xs ${touched.phone && errors.phone ? 'text-red-700' : ''}`}>
                      {touched.phone && errors.phone && errors.phone}
                    </div>
                  </div>
                  <div className="w-full">
                    <div className="text-md text-black font-bold py-1">Password</div>
                    <label htmlFor="password" className="border-gray-300 w-full">
                      <div className="flex flex-row gap-3">
                        <input
                          className="w-full px-4 border rounded-lg py-2 focus:outline-none focus:border-green-250"
                          title="password"
                          id="password"
                          name="password"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          type={showPassword ? 'text' : 'password'}
                          placeholder="Masukan password Anda"
                          value={values.password}
                          required
                        />
                        <button type="button" className="flex items-center cursor-pointer focus:outline-none" onClick={togglePassword}>{showPassword ? <FaEyeSlash style={{ color: 'gray', fontSize: '20px' }} /> : <FaEye style={{ color: 'gray', fontSize: '20px' }} />}</button>
                      </div>
                    </label>
                    <div className={`text-xs box-border py-1 pb-2 ${touched.password && errors.password ? 'text-red-500' : ''}`}>
                      {touched.password && errors.password && errors.password}
                    </div>
                  </div>
                  <div className="w-full py-2">
                    <div className="text-md text-black font-bold py-1">Ulangi Password</div>
                    <label htmlFor="password" className="border-gray-300 w-full">
                      <div className="flex flex-row gap-3">
                        <input
                          className="w-full px-4 border rounded-lg py-2 focus:outline-none focus:border-green-250"
                          title="passwordConfirmation"
                          id="passwordConfirmation"
                          name="passwordConfirmation"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          type={showPasswordConf ? 'text' : 'password'}
                          placeholder="Masukan kembali password Anda"
                          value={values.passwordConfirmation}
                          required
                        />
                        <button type="button" className="flex items-center cursor-pointer focus:outline-none" onClick={togglePasswordConf}>{showPasswordConf ? <FaEyeSlash style={{ color: 'gray', fontSize: '20px' }} /> : <FaEye style={{ color: 'gray', fontSize: '20px' }} />}</button>
                      </div>
                    </label>
                    <div className={`text-xs box-border py-1 pb-2 ${touched.passwordConfirmation && errors.passwordConfirmation ? 'text-red-500' : ''}`}>
                      {touched.passwordConfirmation && errors.password && errors.password}
                    </div>
                  </div>
                  <div className="pt-2 pb-6">
                    <button
                      onClick={() => setShowError(false)}
                      className="bg-green-250 text-sm text-white border-none rounded-lg font-bold py-3 w-full"
                      type="submit"
                      disabled={!(isValid && dirty) && loading}
                    >
                      {loading ? 'Mohon Tunggu...' : 'Buat Akun'}
                    </button>
                    {showError ? (
                      <div className="pt-3">
                        <div className="border-none rounded-md bg-red-600 bg-transparent opacity-70 p-2 text-center text-xs  text-white">
                          Email sudah digunakan, mohon mengubah alamat email anda.
                        </div>
                      </div>
                    ) : null}
                  </div>
                </form>
              )}
            </Formik>
          </div>
          <div className="border-t border-gray-300">
            <div className="w-full flex justify-center text-sm text-center py-2 text-gray-400">
              {/* eslint-disable-next-line max-len */}
              <p className="md:mx-7 sm:mx-4 sm:text-xs">
                Dengan mendaftar, Anda menyetujui
                {' '}
                <Link href="/">
                  <a className="text-green-550">Kebijakan dan Syarat Ketentuan</a>
                </Link>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
