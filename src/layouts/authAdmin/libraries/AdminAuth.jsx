import { useRouter } from 'next/router';
import { roles } from '@/helpers/auth';

export default function AdminAuthLayout({ children }) {
  const router = useRouter();
  const role = roles();

  if (((role !== 'ROLE_SUPERUSER') || !role) && typeof window !== 'undefined') {
    router.push('/');
    return (
      <div className="container mx-auto flex justify-center items-center font-semibold h-screen">
        Mohon Tunggu...
      </div>
    );
  }
  return (
    children
  );
}
