import Sidebar from '@/components/Sidebar';
import NavbarProfile from '@/components/NavbarProfile/libraries/NavbarProfile';

export default function AccountLayout({ children, parent }) {
  return (
    <div>
      <NavbarProfile />
      <div className="container mx-auto flex p-8">
        <Sidebar parent={parent} />
        {children}
      </div>
    </div>
  );
}
