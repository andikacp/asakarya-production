import { useRouter } from 'next/router';
import { roles } from '@/helpers/auth';

export default function CreatorAuthLayout({ children }) {
  const router = useRouter();
  const role = roles();

  if (!role && typeof window !== 'undefined') {
    router.push('/login');
  }

  if (role === 'ROLE_INVESTOR') {
    router.push('/brief');
    return (
      <div className="container mx-auto flex justify-center items-center font-semibold h-screen">
        Mohon Tunggu...
      </div>
    );
  }
  return (
    children
  );
}
