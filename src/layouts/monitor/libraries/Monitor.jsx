import Main from '@/layouts/main';
import CampaignMonitorContainer from '@/containers/campaign/Monitor';

// eslint-disable-next-line react/prop-types
export default function MonitorLayout({ children }) {
  return (
    <>
      <Main>
        <CampaignMonitorContainer />
        {children}
      </Main>
    </>
  );
}
