import Copyright from '@/components/Copyright';
import NavbarAdmin from '@/components/NavbarAdmin';
import SidebarAdmin from '@/components/SidebarAdmin';

// eslint-disable-next-line react/prop-types
export default function AdminLayout({ children }) {
  return (
    <>

      <div className="bg-cream-100 container mx-auto w-full">
        <SidebarAdmin />
        <div className="flex flex-col" style={{ paddingLeft: '280px' }}>
          <NavbarAdmin />
          <div className="p-10">
            {children}
          </div>
        </div>
      </div>
    </>

  );
}
