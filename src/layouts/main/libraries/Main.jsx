// import { useState, useEffect } from 'react';
import Footer from '@/components/Footer';
import Header from '@/components/Header';
import Navbar from '@/components/Navbar';
// import NavbarToggle from '@/components/NavbarToggle';

export const screen = () => { if (typeof window !== 'undefined') return window.innerWidth; return null; };

// eslint-disable-next-line react/prop-types
export default function MainLayout({ children }) {
  return (
    <>
      <Header />
      {/* {screen > 960 ? (
        <Navbar />
      ) : (
        <NavbarToggle />
      )} */}
      {/* <NavbarToggle /> */}
      <Navbar />
      {children}
      <Footer />
    </>
  );
}
