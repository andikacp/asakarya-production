import Link from 'next/link';
import { useRouter } from 'next/router';
import Image from 'next/image';
import { logout } from 'src/helpers/auth';

export default function Sidebar() {
  const router = useRouter();
  const handleLogout = () => {
    logout();
    router.push('/');
  };

  return (
    <aside className="h-screen bg-gray-850 fixed top-0 left-0 z-50" style={{ width: '280px' }}>
      <div className="flex justify-center">
        <Image src="/img/logo/logo-with-text.png" width={157} height={214} />
      </div>
      <div className="mt-8">
        <Link href="/admin/dashboard">
          <a className={`${router.pathname.includes('/dashboard') && 'bg-gray-750 bg-opacity-10 underline text-white'} px-16 py-2 my-4 text-xl flex items-center text-gray-75 hover:bg-gray-750 hover:bg-opacity-10 transition duration-300 `}>
            <svg xmlns="http://www.w3.org/2000/svg" width="44" height="44" viewBox="0 0 44 44" fill="none">
              <path d="M7.33333 23.8333H18.3333C19.3417 23.8333 20.1667 23.0083 20.1667 22V7.33333C20.1667 6.325 19.3417 5.5 18.3333 5.5H7.33333C6.325 5.5 5.5 6.325 5.5 7.33333V22C5.5 23.0083 6.325 23.8333 7.33333 23.8333ZM7.33333 38.5H18.3333C19.3417 38.5 20.1667 37.675 20.1667 36.6667V29.3333C20.1667 28.325 19.3417 27.5 18.3333 27.5H7.33333C6.325 27.5 5.5 28.325 5.5 29.3333V36.6667C5.5 37.675 6.325 38.5 7.33333 38.5ZM25.6667 38.5H36.6667C37.675 38.5 38.5 37.675 38.5 36.6667V22C38.5 20.9917 37.675 20.1667 36.6667 20.1667H25.6667C24.6583 20.1667 23.8333 20.9917 23.8333 22V36.6667C23.8333 37.675 24.6583 38.5 25.6667 38.5ZM23.8333 7.33333V14.6667C23.8333 15.675 24.6583 16.5 25.6667 16.5H36.6667C37.675 16.5 38.5 15.675 38.5 14.6667V7.33333C38.5 6.325 37.675 5.5 36.6667 5.5H25.6667C24.6583 5.5 23.8333 6.325 23.8333 7.33333Z" fill="#E1E1E1" />
            </svg>
            <span className={`mx-1 ${router.pathname.includes('/dashboard') && 'text-white'}`}>Dashboard</span>
          </a>
        </Link>
        <Link href="/admin/campaign">
          <a className={`${router.pathname.includes('campaign') && 'bg-gray-750 bg-opacity-10 underline text-white'} px-16 py-3 my-4 text-xl flex items-center text-gray-75 hover:bg-gray-750 hover:bg-opacity-10 transition duration-300`}>
            <svg xmlns="http://www.w3.org/2000/svg" width="38" height="34" viewBox="0 0 38 34" fill="none">
              <path d="M9.83333 9.6665C8.825 9.6665 8 10.4915 8 11.4998V22.4998C8 24.5165 9.65 26.1665 11.6667 26.1665H26.3333C27.3417 26.1665 28.1667 25.3415 28.1667 24.3332C28.1667 23.3248 27.3417 22.4998 26.3333 22.4998H11.6667V11.4998C11.6667 10.4915 10.8417 9.6665 9.83333 9.6665Z" fill="#E1E1E1" />
              <path d="M33.6673 0.5H19.0007C16.984 0.5 15.334 2.15 15.334 4.16667V15.1667C15.334 17.1833 16.984 18.8333 19.0007 18.8333H33.6673C35.684 18.8333 37.334 17.1833 37.334 15.1667V4.16667C37.334 2.15 35.684 0.5 33.6673 0.5ZM32.7507 15.1667H19.9173C19.404 15.1667 19.0007 14.7633 19.0007 14.25V7.83333H33.6673V14.25C33.6673 14.7633 33.264 15.1667 32.7507 15.1667Z" fill="#E1E1E1" />
              <path d="M2.50033 17C1.49199 17 0.666992 17.825 0.666992 18.8333V29.8333C0.666992 31.85 2.31699 33.5 4.33366 33.5H19.0003C20.0087 33.5 20.8337 32.675 20.8337 31.6667C20.8337 30.6583 20.0087 29.8333 19.0003 29.8333H4.33366V18.8333C4.33366 17.825 3.50866 17 2.50033 17Z" fill="#E1E1E1" />
            </svg>
            <span className={`mx-2 ${router.pathname.includes('/campaign') && 'text-white'}`}>Campaign</span>
          </a>
        </Link>
        <Link href="#">
          <a className={`${router.pathname.includes('#') && 'bg-gray-750 bg-opacity-10 underline text-white'} hidden px-16 py-2 my-4 text-xl flex items-center text-gray-75 hover:bg-gray-750 hover:bg-opacity-10 transition duration-300`}>
            <svg xmlns="http://www.w3.org/2000/svg" width="38" height="38" viewBox="0 0 38 38" fill="none">
              <path d="M30.6667 7.33333H34.1511C35.1644 7.33333 36 8.16889 36 9.18222V32.1511C36 33.1644 35.1644 34 34.1511 34H11.1822C10.1689 34 9.33333 33.1644 9.33333 32.1511V28.6667H5.84889C4.83556 28.6667 4 27.8311 4 26.8178V3.84889C4 2.83556 4.83556 2 5.84889 2H28.8178C29.8311 2 30.6667 2.83556 30.6667 3.84889V7.33333ZM7.55556 25.1111H27.1111V5.55556H7.55556V25.1111ZM16.4444 10C16.4444 8.52444 15.2533 7.33333 13.7778 7.33333C12.3022 7.33333 11.1111 8.52444 11.1111 10C11.1111 11.4756 12.3022 12.6667 13.7778 12.6667C15.2533 12.6667 16.4444 11.4756 16.4444 10ZM20 18C20 18 21.7778 9.11111 25.3333 9.11111V23.3333H9.33333V12.6667C12.8889 12.6667 12.8889 18 12.8889 18C12.8889 18 13.4756 14.4444 16.4444 14.4444C19.4133 14.4444 20 18 20 18ZM32.4444 30.4444V10.8889H30.6667V26.8178C30.6667 27.8311 29.8311 28.6667 28.8178 28.6667H12.8889V30.4444H32.4444Z" fill="#E1E1E1" />
            </svg>
            <span className={`mx-2 ${router.pathname.includes('/dashboard') && 'text-white'}`}>Gallery</span>
          </a>
        </Link>
        <Link href="#">
          <a className={`${router.pathname.includes('#') && 'bg-gray-750 bg-opacity-10 underline text-white'} hidden px-16 py-2 my-4 text-xl flex items-center text-gray-75 hover:bg-gray-750 hover:bg-opacity-10 transition duration-300`}>
            <svg xmlns="http://www.w3.org/2000/svg" width="42" height="42" viewBox="0 0 42 42" fill="none">
              <path d="M15.75 20.5625C14.5425 20.5625 13.5625 21.5425 13.5625 22.75C13.5625 23.9575 14.5425 24.9375 15.75 24.9375C16.9575 24.9375 17.9375 23.9575 17.9375 22.75C17.9375 21.5425 16.9575 20.5625 15.75 20.5625ZM26.25 20.5625C25.0425 20.5625 24.0625 21.5425 24.0625 22.75C24.0625 23.9575 25.0425 24.9375 26.25 24.9375C27.4575 24.9375 28.4375 23.9575 28.4375 22.75C28.4375 21.5425 27.4575 20.5625 26.25 20.5625ZM21 3.5C11.34 3.5 3.5 11.34 3.5 21C3.5 30.66 11.34 38.5 21 38.5C30.66 38.5 38.5 30.66 38.5 21C38.5 11.34 30.66 3.5 21 3.5ZM21 35C13.2825 35 7 28.7175 7 21C7 20.4925 7.035 19.985 7.0875 19.495C11.2175 17.6575 14.49 14.28 16.205 10.0975C19.3725 14.5775 24.5875 17.5 30.485 17.5C31.85 17.5 33.1625 17.3425 34.4225 17.045C34.79 18.2875 35 19.6175 35 21C35 28.7175 28.7175 35 21 35Z" fill="#E1E1E1" />
            </svg>
            <span className={`mx-1 ${router.pathname.includes('/dashboard') && 'text-white'}`}>User Profile</span>
          </a>
        </Link>
      </div>
      <div className="flex justify-center mt-60">
        <button type="button" onClick={handleLogout} className="flex items-center gap-2">
          <a className="text-gray-75 text-xl hover:text-gray-750 hover:underline">Log Out</a>
          <svg xmlns="http://www.w3.org/2000/svg" width="36" height="44" viewBox="0 0 36 44" fill="none">
            <path d="M31.0003 0.333496H5.00033C2.61699 0.333496 0.666992 2.2835 0.666992 4.66683V39.3335C0.666992 41.7168 2.61699 43.6668 5.00033 43.6668H31.0003C33.3837 43.6668 35.3337 41.7168 35.3337 39.3335V4.66683C35.3337 2.2835 33.3837 0.333496 31.0003 0.333496ZM25.5837 25.2502C23.7853 25.2502 22.3337 23.7985 22.3337 22.0002C22.3337 20.2018 23.7853 18.7502 25.5837 18.7502C27.382 18.7502 28.8337 20.2018 28.8337 22.0002C28.8337 23.7985 27.382 25.2502 25.5837 25.2502Z" fill="#E1E1E1" />
          </svg>
        </button>
      </div>
    </aside>
  );
}
