import { ChevronRightIcon } from '@heroicons/react/solid';

export default function Breadcrumb({ pages }) {
  return (
    <nav className="flex" aria-label="Breadcrumb">
      <ol className="flex items-center space-x-4">
        <li>
          <div>
            <a href="#" className="text-lg font-semibold text-black hover:text-green-550 hover:underline">
              <span className="sr-only">Campaign</span>
            </a>
          </div>
        </li>
        {pages.map((page) => (
          <li key={page.name}>
            <div className="flex items-center">
              <a
                href={page.href}
                className="text-lg font-semibold text-black hover:text-green-550 hover:underline"
                aria-current={page.current ? 'page' : undefined}
              >
                {page.name}
              </a>
              <ChevronRightIcon className={`ml-4 flex-shrink-0 h-5 w-5 text-green-250 ${page.current && 'hidden'}`} aria-hidden={page.current} />
            </div>
          </li>
        ))}
      </ol>
    </nav>
  );
}
