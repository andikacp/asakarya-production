/* eslint-disable no-console */

import React, { useState } from 'react';
import Image from 'next/image';
import Modal from 'react-modal';
import { Formik } from 'formik';
import { sessionUser } from '@/helpers/auth';
import { baseApiUrl } from '@/helpers/baseContents';
// import { useUserDispatch } from '@/redux/reducers/user/slices';

const customStyles = {
  content: {
    width: '630px',
    height: '630px',
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};

export default function EditAddress({ addressId, modifyModal = true, ...addressDetail }) {
  // const { getAdressDetail, user } = useUserDispatch();
  // const { addressDetail } = user;

  // useEffect(() => {
  //   if (addressDetail) {
  //     getAdressDetail();
  //   }
  // }, []);

  const [modalIsOpen, setModalIsOpen] = useState(false);

  const initialValues = {
    recipient: addressDetail.recipient,
    phone: addressDetail.phone,
    address: addressDetail.address,
    postalCode: addressDetail.postalCode,
    id: addressId,
  };

  const token = sessionUser();

  const editAddress = async (values) => {
    console.log(values);
    try {
      const response = await fetch(`${baseApiUrl}/address/update`, {
        method: 'PUT',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(values),
      });
      const data = await response.json();
      console.log(data);
    } catch (error) {
      console.log(error);
    }
  };

  const handleOnSubmit = async (values) => {
    await editAddress(values);
    // setModalIsOpen(false);
    window.location.reload();
  };

  return (
    <>
      <div className="rounded-t">
        {modifyModal ? (<button type="button" className="underline text-md ml-3 mb-3 focus:text-green-250 focus:underline" onClick={() => setModalIsOpen(true)}>Ubah</button>) : (<button type="button" className="font-bold text-xl text-black hovertext-green-250" onClick={() => setModalIsOpen(true)}>UBAH</button>)}
        <Modal
          isOpen={modalIsOpen}
          shouldCloseOnOverlayClick={false}
          onRequestClose={() => setModalIsOpen(false)}
          ariaHideApp={false}
          style={customStyles}
          className="fixed mx-auto bg-white shadow-xl rounded-md p-5"
        >
          <div className="flex justify-end items-end">
            <button type="button" onClick={() => setModalIsOpen(false)}>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                <path fillRule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clipRule="evenodd" />
              </svg>
            </button>
          </div>
          <div className="flex justify-center items-center">
            <Image
              src="/img/logo/logo-with-text.png"
              width={50}
              height={70}
            />
          </div>
          <div>
            <h1 className="text-black text-2xl text-center font-semibold mt-2">Ubah Alamat</h1>
            <div className="flex justify-center items-center mt-3">
              <div className="border rounded-lg border-gray-300 bg-white w-5/6 h-8/12">
                <Formik
                  initialValues={initialValues}
                  onSubmit={(values) => handleOnSubmit(values)}
                  enableReinitialize
                >
                  {({
                    handleSubmit, handleChange, handleBlur,
                  }) => (
                    <form className="px-4 py-4" onSubmit={handleSubmit}>
                      <div className="text-sm text-black font-bold">Nama Penerima</div>
                      <input
                        className="border w-full h-9 rounded-md mb-2 text-xs px-3 outline-none focus:border-gray-350"
                        title="Name"
                        id="recipient"
                        name="recipient"
                        placeholder="Masukan Nama Penerima"
                        type="text"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        defaultValue={addressDetail.recipient}
                        required
                      />
                      <div className="text-sm text-black font-bold mt-2">Nomor Telepon</div>
                      <input
                        className="border w-full h-9 rounded-md mb-2 text-xs px-3 outline-none focus:border-gray-350"
                        title="phone"
                        id="phone"
                        name="phone"
                        placeholder="Masukan Nomor Telepon Anda"
                        type="text"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        defaultValue={addressDetail.phone}
                        required
                      />
                      <div className="text-sm text-black font-bold mt-2">Alamat</div>
                      <div className="text-xs text-gray-250">(Cth: Jalan Juang No.76, Kec. Kemang, Kota Bogor, Jawa Barat)</div>
                      <input
                        className="border w-full h-9 rounded-md mb-2 text-xs px-3 outline-none focus:border-gray-350"
                        title="address"
                        id="address"
                        name="address"
                        placeholder="Masukan Jalan, No. Rumah, Kecamatan, Kota/Kab.Provinsi"
                        type="text"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        defaultValue={addressDetail.address}
                        required
                      />
                      <div className="text-sm text-black font-bold mt-2">Kode Pos</div>
                      <input
                        className="border w-full h-9 rounded-md mb-2 text-xs px-3 outline-none focus:border-gray-350"
                        title="postalCode"
                        id="postalCode"
                        name="postalCode"
                        placeholder="Masukan Kode Post Alamat Anda"
                        type="number"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        defaultValue={addressDetail.postalCode}
                        required
                      />

                      <div className="pt-2 flex justify-center items-center gap-4">
                        <button
                          className="bg-white text-sm text-green-250 border-green-250 border rounded-lg font-bold py-2 w-44 h-auto hover:bg-green-250 hover:text-white"
                          type="button"
                          onClick={() => setModalIsOpen(false)}
                        >
                          Nanti Saja

                        </button>
                        <button className="bg-white text-sm text-green-250 border-green-250 border rounded-lg font-bold py-2 w-44 h-auto hover:bg-green-250 hover:text-white" type="submit">Ubah Alamat</button>
                      </div>
                    </form>
                  )}
                </Formik>
              </div>
            </div>

            <div className="text-sm text-black font-normal text-center mt-4">
              <div>021-14007</div>
              <div>asakarya.indonesia@gmail.com</div>
            </div>
          </div>
        </Modal>
      </div>
    </>
  );
}
