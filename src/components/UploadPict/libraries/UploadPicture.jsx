/* eslint-disable no-nested-ternary */
import { useFormikContext } from 'formik';
import { useState } from 'react';

export default function UploadPicture({ ...props }) {
  const { setFieldValue } = useFormikContext();
  const [preview, setPreview] = useState();

  const handleChange = (e) => {
    const { files } = e.target;
    if (files) {
      setPreview(files[0]);
      setFieldValue(props.name, files[0]);
    }
    e.target.value = '';
  };

  const handleRemove = () => {
    setFieldValue(props.name, null);
    setPreview(null);
  };

  return (
    <div>
      <label htmlFor={props.name} className="rounded-sm border text-sm h-6 mb-2 w-5/6 px-3 hover:border-gray-400 transition duration-300 cursor-pointer">
        {preview && preview.name.length > 10 ? (
          <span>
            {`${preview.name.substr(0, 10)}...`}
          </span>
        ) : (preview && preview.name.length <= 10
          ? <span>{preview.name}</span> : <span>Pilih Gambar</span>)}

        <input
          {...props}
          id={props.name}
          name={props.name}
          type="file"
          className="hidden"
          onChange={handleChange}
          accept=".png, .jpeg, .jpg"
        />
      </label>
      {preview && <button type="button" onClick={handleRemove}>x</button>}
    </div>
  );
}
