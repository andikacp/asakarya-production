import Image from 'next/image';

export default function Reward({ hideBase = false, ...item }) {
  return (
    <div className="flex flex-col border border-gray-150 h-96" style={{ height: 412, width: 280 }}>
      <div>
        {item.imgUrl ? <Image src={item.imgUrl} width="281" height="164" /> : <div className="flex justify-center items-center" style={{ width: 281, height: 164 }}> no image found</div>}

      </div>
      <div className="p-5">
        <div>
          Jumlah donasi
        </div>
        <div className={`${hideBase && 'hidden'}text-sm text-green-650 my-3`}>
          {
           // eslint-disable-next-line no-nested-ternary
           item.rewardTypeId === 0 ? ('Donasi sesuai keinginanmu!') : (item.rewardTypeId === 1 ? ('Rp. 100.000 - Rp. 249.999') : (item.rewardTypeId === 2 ? ('Rp. 250.000 - 499.999') : ('di atas Rp. 500.000')))
          }
        </div>
        <div>
          {item.rewardTypeId === 0 ? 'Terima kasih!' : 'Anda Mendapatkan' }
        </div>
        <div className="text-sm my-3">
          {item.item ? item.item : 'Sudah mendukung kami sebagai pelaku industri kreatif.'}
        </div>
      </div>
    </div>
  );
}
