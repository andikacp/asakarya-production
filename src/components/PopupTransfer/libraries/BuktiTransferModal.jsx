import React, { useState } from 'react';
import Image from 'next/image';
import Modal from 'react-modal';
import EmptyImage from '@/components/EmptyImage';

const customStyles = {
  content: {
    width: '600px',
    height: '500px',
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};

export default function BuktiTransferModal({ item, onConfirm, onReject }) {
  const [modalIsOpen, setModalIsOpen] = useState(false);

  const handleConfirm = () => {
    onConfirm();
    setModalIsOpen(false);
  };

  const handleReject = () => {
    onReject();
    setModalIsOpen(false);
  };

  const downloadImage = () => {
    window.open(item.paymentSlipUrl);
  };

  return (
    <>
      <div className="rounded-t">
        <button type="button" className="underline text-lg ml-3 mb-3 text-black hover:text-green-250" onClick={() => setModalIsOpen(true)}>Lihat Bukti Transfer</button>
        <Modal
          isOpen={modalIsOpen}
          shouldCloseOnOverlayClick={false}
          onRequestClose={() => setModalIsOpen(false)}
          ariaHideApp={false}
          style={customStyles}
        >
          <div className="flex justify-end items-end">
            <button type="button" onClick={() => setModalIsOpen(false)}>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                <path fillRule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clipRule="evenodd" />
              </svg>
            </button>
          </div>
          <div className="flex justify-center items-center">
            <Image
              src="/img/logo/logo-with-text.png"
              width={70}
              height={90}
            />
          </div>
          <div className="mt-4">
            <h1 className="text-black text-3xl text-center font-semibold">Bukti Transfer</h1>
            <div className="flex justify-center items-center mt-3">
              <div className="border rounded-lg border-gray-300 bg-white w-5/6 h-8/12">
                <div className="flex justify-center">
                  {item.paymentSlipUrl ? <Image width={400} height={300} src={`${item.paymentSlipUrl}`} className="object-contain" /> : <EmptyImage h={300} w={400} soruce="default" />}
                </div>
                <div className="flex justify-center items-center text-lg font-normal underline mb-5 ">
                  <button type="button" onClick={() => downloadImage()} className="hover:text-green-250 hover:underline">
                    Unduh Gambar
                  </button>
                </div>
                <div className="pt-2 flex justify-center items-center gap-4 mb-2">
                  {
                    item.status === 0 ? (
                      <>
                        <button
                          className="bg-white text-sm text-green-250 border-green-250 border rounded-lg font-semibold w-44 h-10"
                          type="button"
                          onClick={() => handleReject()}
                        >
                          Gagal Verifikasi
                        </button>
                        <button
                          className="bg-green-250 text-sm text-white border-none rounded-lg font-semibold w-44 h-10 hover:bg-green-550 transition duration-300"
                          type="button"
                          onClick={() => handleConfirm()}
                        >
                          Sukses Verifikasi
                        </button>
                      </>
                    ) : (
                      <button
                        className="bg-white text-sm text-green-250 border-green-250 border rounded-lg font-semibold w-44 h-10 hover:bg-green-250 hover:text-white transition duration-300"
                        type="button"
                        onClick={() => setModalIsOpen(false)}
                      >
                        Tutup
                      </button>
                    )
                  }

                </div>
              </div>
            </div>
            <div className="text-sm text-black font-normal text-center mt-16">
              <div>021-14007</div>
              <div>asakarya.indonesia@gmail.com</div>
            </div>
          </div>
        </Modal>
      </div>
    </>
  );
}
