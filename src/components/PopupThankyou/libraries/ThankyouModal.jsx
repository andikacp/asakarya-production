/* eslint-disable jsx-a11y/control-has-associated-label */
// import React, { useState } from 'react';
import Image from 'next/image';
import Modal from 'react-modal';

const customStyles = {
  content: {
    width: '630px',
    height: '535px',
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};

export default function Thankyou({ modalIsOpen, handleOnClose }) {
  return (
    <>
      <div className="rounded-t">
        <Modal
          isOpen={modalIsOpen}
          shouldCloseOnOverlayClick={false}
          onRequestClose={() => handleOnClose()}
          ariaHideApp={false}
          style={customStyles}
        >
          <div className="flex justify-end items-end">
            <button type="button" onClick={() => handleOnClose()}>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                <path fillRule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clipRule="evenodd" />
              </svg>
            </button>
          </div>
          <div className="flex justify-center items-center">
            <Image
              src="/img/logo/logo-with-text.png"
              width={80}
              height={110}
            />
          </div>
          <div className="mt-4">
            <h1 className="text-black text-4xl text-center font-semibold">Terima Kasih!</h1>
            <div className="text-center text-lg mx-32 mt-8">
              Kamu sudah berkontribusi untuk mendukung kemajuan
              Industri Kreatif di Indonesia
            </div>
            <div className="text-center text-lg mx-32 mt-6">
              Bersama AsaKarya kita dukung karya anak bangsa!
            </div>
            <div className="text-sm text-black font-normal text-center mt-10">
              <div>021-14007</div>
              <div>asakarya.indonesia@gmail.com</div>
            </div>
          </div>
        </Modal>
      </div>
    </>
  );
}
