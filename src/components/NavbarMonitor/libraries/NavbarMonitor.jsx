/* eslint-disable max-len */
// import { useState } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
// import { useUserDispatch } from '@/redux/reducers/user/slices';

export default function NavbarMonitor() {
  const router = useRouter();
  const { campaignId } = router.query;
  return (
    <div className="border-gray-150 border-b border-t w-full my-5">
      <div className="flex h-full mx-auto text-xl font-semibold gap-3 p-3">
        <div className="flex items-end justify-end space-x-3">
          <Link href={`/mycampaign/${campaignId}/donation`}>
            <a className={`${router.pathname.includes('/donation') && 'text-green-550 underline'} text-gray-900 hover:text-green-250 md:mx-4 my-0 focus:text-green-250 hover:underline focus:underline`}>Riwayat Donasi</a>
          </Link>
        </div>
        <div className="flex items-end justify-end space-x-3 ">
          <Link href={`/mycampaign/${campaignId}/income`}>
            <a className={`${router.pathname.includes('/income') && 'text-green-550 underline'} text-gray-900 hover:text-green-250 md:mx-4 my-0 focus:text-green-250 hover:underline focus:underline`}>Dana Masuk</a>
          </Link>
        </div>
        <div className="flex items-end justify-end space-x-3 ">
          <Link href={`/mycampaign/${campaignId}/reward`}>
            <a className={`${router.pathname.includes('/reward') && 'text-green-550 underline'} text-gray-900 hover:text-green-250 md:mx-4 my-0 focus:text-green-250 hover:underline focus:underline`}>Pengiriman Cenderamata</a>
          </Link>
        </div>
      </div>
    </div>
  );
}
