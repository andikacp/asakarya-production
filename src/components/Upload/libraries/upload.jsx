import { TrashIcon } from '@heroicons/react/outline';
import { useFormikContext } from 'formik';
import { useState } from 'react';
import { MdOutlineAddAPhoto } from 'react-icons/md';

export function UploadItem({ name, onRemove }) {
  return (
    <div className="text-sm border py-1 px-3 h-11 flex justify-between items-center rounded">
      <div className="flex-1">
        {name}
      </div>
      <div>
        <button onClick={() => onRemove()} type="button" className="hover:text-red-500">
          <TrashIcon className="w-4 h-4" />
        </button>
      </div>
    </div>
  );
}

export default function Upload({ ...props }) {
  const { setFieldValue } = useFormikContext();
  const [preview, setPreview] = useState();

  const handleChange = (e) => {
    const { files } = e.target;
    if (files) {
      setPreview(files[0]);
      setFieldValue(props.name, files[0]);
    }
    e.target.value = '';
  };

  const handleRemove = () => {
    setPreview(null);
  };

  return (
    <div className="cursor-pointer border-4 rounded-md border-dashed border-gray-250 p-3 text-center mx-28">
      <label htmlFor={props.name}>
        <div className="flex justify-center py-2">
          <MdOutlineAddAPhoto style={{ color: 'gray', fontSize: '30px' }} />
        </div>
        <span className="text-gray-350">Upload bukti pembayaran</span>
        <input
          {...props}
          id={props.name}
          name={props.name}
          type="file"
          className="hidden"
          onChange={handleChange}
          required={props.required}
        />
      </label>
      <div className="w-full space-y-1 py-2">
        {
          preview && (
            <UploadItem name={preview.name} onRemove={() => handleRemove()} />
          )
        }
      </div>
    </div>
  );
}
