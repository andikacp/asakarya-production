import Image from 'next/image';
import Link from 'next/link';
import { logout, name, profileImage } from 'src/helpers/auth';
import { useRouter } from 'next/router';

export default function Sidebar({ parent }) {
  const router = useRouter();
  const { campaignId } = router.query;
  const handleLogout = () => {
    logout();
    router.push('/');
  };
  const imgUrl = profileImage();

  const fullName = name();

  return (
    <aside className="w-1/4 flex flex-col justify-between text-xl h-auto p-5">
      <div className="flex flex-col">
        <div className="flex gap-2 border-b-2 border-gray-150 py-4 my-2">
          {imgUrl ? (
            <Image className=" object-cover w-8 h-8 rounded-full cursor-pointer" src={imgUrl} alt="Profile image" width="40" height="40" />
          ) : (
            <Image className=" object-cover w-8 h-8 rounded-full cursor-pointer" src="/img/profile/avatar.png" alt="Profile image" width="40" height="40" />
          )}
          <div className="font-semibold flex ml-5 items-center">{fullName}</div>
        </div>

        <div className="flex flex-col content-between gap-1 mt-2">
          {parent === 'profil'
          && (
          <>

            <div className="font-semibold">Akun Saya</div>
            <Link href="/profile">
              <div className={`${router.pathname.includes('/profile') && 'text-green-550 underline font-semibold'} hover:text-green-250 hover:underline cursor-pointer`}>Profil</div>
            </Link>
            <Link href="/address">
              <div className={`${router.pathname.includes('/address') && 'text-green-550 underline font-semibold'} hover:text-green-250 hover:underline cursor-pointer`}>Alamat</div>
            </Link>
            <Link href="/password">
              <div className={`${router.pathname.includes('/password') && 'text-green-550 underline font-semibold'} hover:text-green-250 hover:underline cursor-pointer`}>Ubah Password</div>
            </Link>
            <Link href="/creator-edit">
              <div className={`${router.pathname.includes('/edit') && 'text-green-550 underline font-semibold'} hover:text-green-250 hover:underline cursor-pointer`}>Info Kreator</div>
            </Link>

          </>
          ) }

          {parent === 'createCampaign'
           && (
           <>
             <div className="font-semibold">Buat Campaign</div>
             <Link href={router.pathname.includes('/create') ? '#' : `/mycampaign/${campaignId}/modify`}>
               <div className={`${router.pathname.includes('/modify') && 'text-green-550 underline font-semibold'}hover:text-green-250 hover:underline cursor-pointer`}>Informasi Detail</div>
             </Link>
             <Link href={router.pathname.includes('/create') ? '#' : `/mycampaign/${campaignId}/news`}>
               <div className={`${router.pathname.includes('/news') && 'text-green-550 underline font-semibold'}hover:text-green-250 hover:underline cursor-pointer`}>Kabar Terbaru</div>
             </Link>
             <Link href={router.pathname.includes('/create') ? '#' : `/mycampaign/${campaignId}/faqs`}>
               <div className={`${router.pathname.includes('/faqs') && 'text-green-550 underline font-semibold'}hover:text-green-250 hover:underline cursor-pointer`}>FAQ</div>
             </Link>
           </>
           )}
        </div>
      </div>
      {parent === 'profil' && (
        <div>
          <button type="button" onClick={handleLogout} className=" flex items-center w-24 gap-2">
            <Image src="/img/sidebar/logout.png" width="21" height="26" />
            <a className="hover:text-green-250 hover:underline">Keluar</a>
          </button>
        </div>
      )}

    </aside>
  );
}
