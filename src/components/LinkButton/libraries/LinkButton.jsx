import Link from 'next/link';

export default function LinkButton({ text, url }) {
  return (
    <Link href={url}>
      <button type="button" className="bg-green-250 font-semibold text-white h-12 w-1/4 rounded-lg items-center object-center transition duration-300 hover:bg-green-550">{text}</button>
    </Link>
  );
}
