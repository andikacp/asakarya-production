/* eslint-disable react/destructuring-assignment */

export default function ProgressBar({ detailed = true, ...campaign }) {
  const progress = (campaign.sumDonation / campaign.fundAmount) * 100;
  return (
    <>
      <div className="relative pt-1">
        <div className="flex mb-2 items-center justify-between">
          <div className="text-right" style={{ width: `${Math.round(progress)}%` }}>
            <span
              className={`font-semibold inline-block ${progress >= 100 ? 'text-green-250' : 'text-gray-225'} -mr-5`}
            >
              {parseFloat(progress).toFixed(2)}
              %
            </span>
          </div>
        </div>
        <div className="overflow-hidden h-2 text-xs flex rounded bg-gray-250">
          <div
            style={{ width: `${progress}%` }}
            className="
        shadow-none
        flex flex-col
        text-center
        whitespace-nowrap
        text-white
        justify-center
        bg-green-250
      "
          />
        </div>

        {/* detailed */}
        <div className={detailed ? 'flex justify-between text-sm text-gray-350' : 'hidden'}>
          <div>
            {' '}
            <span className="font-bold">{campaign.countDonation}</span>
            {' '}
            donasi
          </div>
          <div>
            {campaign.daysLeft}
            {' '}
            hari lagi
          </div>
        </div>
      </div>
    </>
  );
}
