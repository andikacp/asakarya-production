import { SearchIcon } from '@heroicons/react/outline';
import Image from 'next/image';
import { Formik } from 'formik';

export default function SearchBar({ handleOnSubmit }) {
  return (
    <>
      {' '}
      <div className="flex justify-center items-center gap-5">
        <div className="flex-grow sm:hidden md:inline">
          <Image src="/img/searchBar/left-search.png" width="236" height="212" alt="Left" />
        </div>
        <div className="flex flex-col flex-grow justify-center text-center p-8 sm:flex-grow">
          <h2 className="text-green-550 sm:text-2xl md:text-5xl">Ubah Mimpi jadi Asa Pasti</h2>
          <p className="my-3  sm:px-0 sm:text-lg md:px-32 md:text-xl">Rekan Asa, kami tahu kalian adalah orang baik</p>

          <Formik
            initialValues={{ query: '' }}
            onSubmit={handleOnSubmit}
          >
            {({
              handleSubmit,
              handleChange,
              handleBlur,
            }) => (
              <form onSubmit={handleSubmit} className="shadow flex my-3 p-1 border-2 border-gray-150 rounded" id="searchbar">
                <input className="w-full rounded p-2 text-xl focus:outline-none" onChange={handleChange} onBlur={handleBlur} type="text" placeholder="Cari project karya" name="query" id="query" />
                <button type="submit" className="bg-white w-auto flex justify-end items-center text-blue-500 p-2 hover:text-blue-400">
                  <SearchIcon className="w-5 text-gray-150" />
                </button>
              </form>
            )}

          </Formik>

        </div>
        <div className="flex-grow sm:hidden md:inline">
          <Image src="/img/searchBar/right-search.png" width="236" height="212" alt="Right" />
        </div>
      </div>

    </>

  );
}
