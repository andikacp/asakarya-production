/* eslint-disable max-len */
/* eslint-disable no-console */
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { FaBars } from 'react-icons/fa';
import { useState } from 'react';
import EmptyImage from '@/components/EmptyImage';
import { sessionUser, profileImage } from '@/helpers/auth';
import '@themesberg/flowbite';

// import './styles.css';

export default function NavbarToggle() {
  const [showNavbar, setShowNavbar] = useState(false);
  const router = useRouter();
  const token = sessionUser();
  const imgUrl = profileImage();

  const toggleNavbar = () => {
    setShowNavbar(!showNavbar);
  };

  return (
    <nav className="h-16 flex justify-start items-center border-b px-12 mb-4 sticky top-0 bg-white" style={{ zIndex: '20' }}>
      <div className="h-full container mx-auto flex justify-between items-center">
        <div className="flex items-center my-4 cursor-pointer">
          <Image width="183" height="54" src="/img/navbar/logo.png" alt="logo" onClick={() => router.push('/')} />
        </div>
        <div className="flex float-right">
          <button type="button" id="dropdownButton" data-dropdown-toggle="dropdown">
            <FaBars style={{ color: 'gray', fontSize: '20px' }} onClick={() => toggleNavbar()} />
          </button>
        </div>
        {/* inside dropdown */}
        {showNavbar ? (
          <div className="border-gray-250 border bg-white p-2 mt-24">
            <div className="flex flex-col text-left border-box flex-start items-center" id="dropdown">
              <div>
                <a href="/about" className={`${router.pathname.includes('/about') && 'text-green-550 underline'} text-lg sm:text-sm text-gray-900 font-bold hover:text-green-250 md:mx-4 md:my-0 focus:text-green-250 hover:underline focus:underline`}>Asakarya</a>
              </div>
              <div>
                <a href="/campaign" className={`${router.pathname.includes('/campaign') && 'text-green-550 underline'} text-lg sm:text-sm text-gray-900 font-bold hover:text-green-250 md:mx-4 md:my-0 focus:text-green-250 hover:underline focus:underline`}>Campaign</a>
              </div>
              <div>
                <a href="/galeri" className={`${router.pathname.includes('/galeri') && 'text-green-550 underline'} text-lg sm:text-sm text-gray-900 font-bold hover:text-green-250 md:mx-4 md:my-0 focus:text-green-250 hover:underline focus:underline`}>Galeri Karya</a>
              </div>
              {token ? (
                <div className="flex space-x-6 justify-between">
                  {imgUrl ? (
                    <Image className={`${router.pathname.includes('/profile') && 'border-green-550'} object-cover w-8 h-8 rounded-full cursor-pointer`} src={imgUrl} alt="Profile image" width="40" height="40" onClick={() => router.push('/profile')} />
                  ) : (
                    <Link href="/profile">
                      <div className="cursor-pointer flex items-center justify-center">
                        <EmptyImage w="40" h="40" source="profile" />
                      </div>
                    </Link>
                  )}
                </div>
              ) : (
                <a href="/login" className="text-lg text-gray-900 font-bold hover:text-green-250 md:mx-4 md:my-0 focus:text-green-250 hover:underline">Masuk</a>
              )}
            </div>
          </div>
        ) : null}
      </div>
    </nav>
  );
}

// Navbar.propTypes = {
//   onHideSidebar: PropTypes.func,
//   isHideSidebar: PropTypes.bool,
// };

// Navbar.defaultProps = {
//   onHideSidebar: () => { },
//   isHideSidebar: false,
// };

// export default Navbar;
