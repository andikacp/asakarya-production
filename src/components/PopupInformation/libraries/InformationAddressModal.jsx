import React, { useState } from 'react';
import Image from 'next/image';
import Modal from 'react-modal';

const customStyles = {
  content: {
    width: '730px',
    height: '570px',
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};

export default function Information({ detail, onConfirm, delivered }) {
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const handleConfirm = () => {
    onConfirm();
    setModalIsOpen(false);
  };
  return (
    <>
      <div className="rounded-t">
        <button type="button" className="underline text-xl mb-3 hover:text-green-250" onClick={() => setModalIsOpen(true)}>Detail Alamat</button>
        <Modal
          isOpen={modalIsOpen}
          shouldCloseOnOverlayClick={false}
          onRequestClose={() => setModalIsOpen(false)}
          ariaHideApp={false}
          style={customStyles}
        >
          <div className="flex justify-end items-end">
            <button type="button" onClick={() => setModalIsOpen(false)}>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                <path fillRule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clipRule="evenodd" />
              </svg>
            </button>
          </div>
          <div className="flex justify-center items-center">
            <Image
              src="/img/logo/logo-with-text.png"
              width={50}
              height={70}
            />
          </div>
          <div className="mt-2">
            <h1 className="text-black text-3xl text-center font-semibold">Informasi Alamat & Pengiriman</h1>
            <div className="flex justify-center items-center mt-3">
              <div className="border rounded-lg border-gray-300 bg-white w-5/6 h-8/12 py-4 px-6">
                <div className="text-lg font-semibold">Nama Penerima</div>
                <div className="text-lg">{detail.recipient}</div>
                <div className="text-lg font-semibold mt-3">Nomor Telepon</div>
                <div className="text-lg">{detail.phone}</div>
                <div className="text-lg font-semibold mt-3">Alamat</div>
                <div className="text-lg">{detail.address}</div>
                <div className="text-lg font-semibold mt-3">Kode Pos</div>
                <div className="text-lg">{detail.postalCode}</div>
                <div className="pt-2 flex justify-center items-center gap-4">
                  <button
                    className="bg-white text-lg text-green-250 border-green-250 border rounded-lg font-bold w-56 h-12"
                    type="button"
                    onClick={() => setModalIsOpen(false)}
                  >
                    Kembali

                  </button>
                  <button className={`bg-green-250 text-lg text-white border-green-250 border rounded-lg font-bold w-56 h-12 hover:bg-green-550 hover:text-white ${delivered && 'hidden'}`} type="button" onClick={handleConfirm}>
                    Tandai Sudah Dikirim
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="text-sm text-black font-normal text-center mt-4">
            <div>021-14007</div>
            <div>asakarya.indonesia@gmail.com</div>
          </div>
        </Modal>
      </div>
    </>
  );
}
