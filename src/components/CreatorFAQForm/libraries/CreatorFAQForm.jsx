import { Formik, Form } from 'formik';

export default function CreatorFAQForm({
  campaignId,
  onSubmit,
  loading,
  ...faqDetail
}) {
  const initialValuesEmpty = {
    question: '',
    answer: '',
    campaignId,
  };

  const initialValues = {
    question: faqDetail.question,
    answer: faqDetail.answer,
    id: faqDetail.id,
    campaignId,
  };

  return (
    <Formik
      initialValues={faqDetail.question ? initialValues : initialValuesEmpty}
      onSubmit={onSubmit}
      enableReinitialize
    >
      {({
        handleSubmit,
        handleChange,
        handleBlur,
      }) => (
        <Form onSubmit={handleSubmit} className="grid grid-cols-4">

          <div className="grid grid-cols-4 gap-3 p-3 mx-3 col-span-3">
            <div className="ml-3 my-auto">Question</div>
            <input
              id="question"
              name="question"
              type="text"
              placeholder="Masukkan pertanyaan"
              className="w-full px-4 border rounded-lg py-2 focus:border-green-250 col-span-3"
              onChange={handleChange}
              onBlur={handleBlur}
              defaultValue={faqDetail.question}
              required
            />

            <div className="ml-3 my-2">Answer</div>
            <textarea
              name="answer"
              id="answer"
              cols="40"
              rows="30"
              placeholder="Masukkan jawaban"
              className="px-4 py-2 border border-gray-150 rounded-lg col-span-3"
              onChange={handleChange}
              onBlur={handleBlur}
              defaultValue={faqDetail.answer}
              required
            />
          </div>
          <div className="col-span-1 border-l-2 my-4" />

          <div className="flex justify-center col-span-4">
            <button type="submit" className={`${loading ? 'bg-gray-150' : 'bg-green-250'} text-white font-semibold h-10 w-1/6 mt-3 rounded-lg transition duration-300 hover:bg-green-550`} disabled={!!loading}>{loading ? 'Mohon Tunggu' : 'Simpan'}</button>
          </div>

        </Form>
      )}

    </Formik>
  );
}
