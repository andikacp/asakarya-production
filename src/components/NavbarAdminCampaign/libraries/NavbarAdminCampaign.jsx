/* eslint-disable max-len */
import { useRouter } from 'next/router';
import Link from 'next/link';

export default function NavbarAdminCampaign() {
  const router = useRouter();
  const { campaignId } = router.query;
  return (
    <div className="border-gray-150 border-b border-t w-full my-5">
      <div className="flex h-full mx-auto text-xl font-semibold gap-3 p-3">
        <div className="flex items-end justify-end space-x-3">
          <Link href={`/admin/campaign/${campaignId}/detail`}>
            <a className={`${router.pathname.includes('/detail') && 'text-green-550 underline'} text-gray-900 hover:text-green-250 md:mx-4 my-0 focus:text-green-250 hover:underline focus:underline`}>Informasi Detail</a>
          </Link>
        </div>
        <div className="flex items-end justify-end space-x-3 ">
          <Link href={`/admin/campaign/${campaignId}/donation`}>
            <a className={`${router.pathname.includes('/donation') && 'text-green-550 underline'} text-gray-900 hover:text-green-250 md:mx-4 my-0 focus:text-green-250 hover:underline focus:underline`}>Histori Donasi</a>
          </Link>
        </div>
        <div className="flex items-end justify-end space-x-3 ">
          <Link href={`/admin/campaign/${campaignId}/disbursement`}>
            <a className={`${router.pathname.includes('/disbursement') && 'text-green-550 underline'} text-gray-900 hover:text-green-250 md:mx-4 my-0 focus:text-green-250 hover:underline focus:underline`}>Dana Terkirim</a>
          </Link>
        </div>
      </div>
    </div>
  );
}
