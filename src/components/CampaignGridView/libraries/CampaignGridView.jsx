import Link from 'next/link';
import Image from 'next/image';
import { ClockIcon } from '@heroicons/react/solid';
import ProgressBar from '@/components/ProgressBar';
import EmptyImage from '@/components/EmptyImage';

export default function CampaignGridView({ detailed = true, ...item }) {
  return (
    <div className="flex flex-col w-96 border border-gray-100 rounded-xl transition duration-300 hover:shadow-2xl ">
      <div>
        {item.imgUrl ? <Image src={item.imgUrl} width="392" height="252" className="object-cover bg-gray-50" /> : <EmptyImage h={252} w={392} source="campaign" />}
      </div>
      <div className="flex flex-col py-5 px-10 gap-3">

        <div className="text-green-250 text-sm flex justify-between">
          {item.categoryName}
          <div className={detailed ? 'flex text-sm text-gray-350 items-center my-1 gap-1' : 'hidden'}>
            <ClockIcon width={14} height={14} />
            <div>
              {item.daysLeft}
              {' '}
              hari tersisa
            </div>
          </div>
        </div>

        <h3 className="text-xl font-bold h-20">{item.title && item.title.length > 75 ? `${item.title.substr(0, 75)}...` : item.title}</h3>

        <div className="text-gray-350 h-20">
          <p>
            {item.description && item.description.length > 75 ? `${item.description.substr(0, 75)}...` : item.description}
          </p>
        </div>

        <div>
          <ProgressBar detailed={false} {...item} />
          <div className={detailed ? 'hidden' : 'flex text-sm text-gray-350 items-center my-1 gap-1'}>
            <ClockIcon width={14} height={14} />
            <div>
              {item.daysLeft}
              {' '}
              hari tersisa
            </div>
          </div>
        </div>

        <div className={detailed ? '' : 'hidden'}>
          <div className="text-sm">
            Terkumpul
          </div>
          <span className=" text-green-550 text-xl font-bold">
            Rp.
            {' '}
            {(item.sumDonation).toLocaleString('id-ID')}
            ,-
            {' '}
          </span>
          <span className="text-sm break-words">
            dari
            {' '}
            {(item.fundAmount).toLocaleString('id-ID')}
            ,-
          </span>
        </div>

        <div className={detailed ? 'hidden' : 'flex flex-col text-gray-350'}>
          <div className="text-sm">Terkumpul</div>
          <div className="text-gray-650">
            Rp.
            {' '}
            {(item.sumDonation).toLocaleString('id-ID')}
            ,-
            {' '}
          </div>
        </div>
        <div className={detailed ? 'flex gap-2 text-black text-sm' : 'hidden'}>
          <div>
            {item.profileImage ? <Image src={item.profileImage} width="40" height="40" className="object-cover bg-gray-50 rounded-md" /> : <EmptyImage w={40} h={40} source="profile" /> }
          </div>
          <div className="flex flex-col h-16">
            <div>
              {item.profileFullName}
            </div>
            <div>
              Lokasi Campaign :
              {' '}
              {item.location}
            </div>
          </div>
        </div>

        <Link href={`/campaign/${item.id}/detail`}>
          <button type="button" className={detailed ? 'bg-green-250 text-white h-12 mt-2 rounded-lg transition duration-300 hover:bg-green-550' : 'hidden'}>Dukung Karya Sekarang</button>
        </Link>
      </div>
    </div>
  );
}
