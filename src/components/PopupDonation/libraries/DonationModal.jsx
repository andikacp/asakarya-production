import React, { useState } from 'react';
import Image from 'next/image';
import Modal from 'react-modal';
import Link from 'next/link';
import { useRouter } from 'next/router';

const customStyles = {
  content: {
    width: '630px',
    height: '535px',
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};

export default function Donation() {
  const router = useRouter();
  const { campaignId } = router.query;
  const [modalIsOpen, setModalIsOpen] = useState(false);
  return (
    <>
      <div className="rounded-t flex justify-center items-center">
        <button type="button" className="bg-green-250 text-white h-12 w-1/2 rounded-lg mt-7 items-center object-center transition duration-300 hover:bg-green-550" onClick={() => setModalIsOpen(true)}>Donasi Sekarang</button>
        <Modal
          isOpen={modalIsOpen}
          shouldCloseOnOverlayClick={false}
          onRequestClose={() => setModalIsOpen(false)}
          ariaHideApp={false}
          style={customStyles}
        >
          <div className="flex justify-end items-end">
            <button type="button" onClick={() => setModalIsOpen(false)}>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                <path fillRule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clipRule="evenodd" />
              </svg>
            </button>
          </div>
          <div className="flex justify-center items-center">
            <Image
              src="/img/logo/logo-with-text.png"
              width={80}
              height={110}
            />
          </div>
          <div className="mt-4">
            <h1 className="text-black text-4xl text-center font-normal">Pilih Opsi Donasi</h1>
            <div className="flex items-center justify-center mt-8">
              <Link href={`/login?campaign=${campaignId}`}>
                <button type="button" className="bg-green-250 text-white w-72 h-16 text-xl rounded-xl font-normal mt-2 hover:bg-green-550 transition duration-300">Login & Dapatkan Paket</button>
              </Link>
            </div>
            <div className="flex items-center justify-center mt-4">
              <Link href={`/campaign/${campaignId}/donation`}>
                <button type="button" className="bg-white text-green-250 border-green-250 border-2 w-72 h-16 text-xl rounded-xl font-normal">Donasi Tanpa Login</button>
              </Link>
            </div>
            <div className="text-lg text-black font-normal text-center mt-10">
              <div>021-14007</div>
              <div>asakarya.indonesia@gmail.com</div>
            </div>
          </div>
        </Modal>
      </div>
    </>
  );
}
