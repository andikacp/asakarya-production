import { Formik, Form } from 'formik';
import UploadPict from '@/components/UploadPict';

export default function CreatorProfileForm({
  onSubmit, loading, ...creatorDetail
}) {
  const initialValuesEmpty = {
    organization: '',
    occupation: '',
    socialMedia: '',
    socialMediaAccount: '',
    bio: '',
    nik: '',
    ktpUrl: {},
    bankName: '',
    bankAccount: '',
    bankAccountName: '',
  };

  const initialValues = {
    organization: creatorDetail.organization,
    occupation: creatorDetail.occupation,
    socialMedia: creatorDetail.socialMedia,
    socialMediaAccount: creatorDetail.socialMediaAccount,
    bio: creatorDetail.bio,
    nik: creatorDetail.nik,
    ktpUrl: {},
    bankName: creatorDetail.bankName,
    bankAccount: creatorDetail.bankAccount,
    bankAccountName: creatorDetail.bankAccountName,
  };

  return (
    <Formik
      initialValues={creatorDetail.id ? initialValues : initialValuesEmpty}
      onSubmit={onSubmit}
      enableReinitialize
    >
      {({
        handleSubmit,
        setFieldValue,
        handleChange,
        handleBlur,
      }) => (
        <Form onSubmit={handleSubmit} className="mx-8">
          <div className="text-lg text-black font-bold my-2">Nama Kreator/Lembaga/Organisasi</div>
          <input
            onChange={handleChange}
            onBlur={handleBlur}
            className="border w-full h-9 rounded-md mb-2 text-xl px-3"
            title="organization"
            id="organization"
            name="organization"
            placeholder="Contoh: Asakarya Indonesia"
            defaultValue={creatorDetail.organization}
            type="text"
            required
          />
          <div className="text-lg text-black font-bold my-2">Pekerjaan</div>
          <input
            onChange={handleChange}
            onBlur={handleBlur}
            className="border w-full h-9 rounded-md mb-2 text-xl px-3"
            title="occupation"
            id="occupation"
            name="occupation"
            placeholder="Contoh: Konten Kreator & Fotografer"
            defaultValue={creatorDetail.occupation}
            type="text"
            required
          />
          <div className="text-lg text-black font-bold my-2">Akun Sosial Media Kamu</div>
          <div className="flex">
            <select
              defaultValue={creatorDetail.socialMedia}
              className="border h-9 rounded-md mb-2 text-xl px-3"
              id="socialMedia"
              name="socialMedia"
              onChange={handleChange}
              onBlur={handleBlur}
              required
            >
              <option value="default" disabled className="text-gray-150">Pilih SosMed</option>
              <option>Instagram</option>
              <option>Facebook</option>
              <option>Twitter</option>
              <option>Youtube</option>
              <option>Lainnya</option>
            </select>
            <input
              onChange={handleChange}
              onBlur={handleBlur}
              className="border w-full h-9 rounded-md mb-2 text-xl px-3"
              title="socialMediaAccount"
              id="socialMediaAccount"
              name="socialMediaAccount"
              placeholder="Contoh: asakarya.indonesia"
              defaultValue={creatorDetail.socialMediaAccount}
              type="text"
              required
            />
          </div>
          <div className="text-lg text-black font-bold my-2">Nomor Induk Kependudukan (NIK)</div>
          <input
            onChange={handleChange}
            onBlur={handleBlur}
            className="border w-full h-9 rounded-md mb-2 text-xl px-3"
            title="nik"
            id="nik"
            name="nik"
            placeholder="123456789"
            defaultValue={creatorDetail.nik}
            type="text"
            required
          />
          <div className="text-lg text-black font-bold my-2">Upload Foto KTP</div>
          <div className="border rounded-md h-9 flex justify-start items-center px-3">
            <UploadPict
              name="ktpUrl"
              accept=".jpg, .png, .jpeg"
              onChange={(event) => {
                setFieldValue('file', event.currentTarget.files[0]);
              }}
            />
          </div>
          <div className="text-lg text-black font-bold my-2">Nama Bank</div>
          <input
            onChange={handleChange}
            onBlur={handleBlur}
            className="border w-full h-9 rounded-md mb-2 text-xl px-3"
            title="bankName"
            id="bankName"
            name="bankName"
            placeholder="Contoh: Bank Central Asia (BCA)"
            defaultValue={creatorDetail.bankName}
            type="text"
            required
          />
          <div className="text-lg text-black font-bold my-2">Nama Pemilik Rekening</div>
          <input
            onChange={handleChange}
            onBlur={handleBlur}
            className="border w-full h-9 rounded-md mb-2 text-xl px-3"
            title="bankAccountName"
            id="bankAccountName"
            name="bankAccountName"
            placeholder="Contoh: Asa Karya"
            defaultValue={creatorDetail.bankAccountName}
            type="text"
            required
          />
          <div className="text-lg text-black font-bold my-2">Nomor Rekening</div>
          <input
            onChange={handleChange}
            onBlur={handleBlur}
            className="border w-full h-9 rounded-md mb-2 text-xl px-3"
            title="bankAccount"
            id="bankAccount"
            name="bankAccount"
            defaultValue={creatorDetail.bankAccount}
            placeholder="Contoh: 123456789"
            type="number"
            required
          />
          <div className="text-lg text-black font-bold my-2">Tentang Diri Kamu</div>
          <textarea
            onChange={handleChange}
            onBlur={handleBlur}
            className="border w-full h-32 rounded-md mb-2 text-xl px-3 flex justify-start"
            title="bio"
            id="bio"
            name="bio"
            placeholder="Ceritakan bidang atau aktivitas yang menggambarkan kamu"
            defaultValue={creatorDetail.bio}
            type="text"
            cols="30"
            rows="20"
            required
          />
          <div className="flex justify-center items-center my-6">
            <button type="submit" className="bg-green-250 text-white w-48 h-12 text-xl rounded-xl font-normal hover:bg-green-550 transition duration-300">
              {loading ? 'Mohon tunggu...' : 'Simpan'}
            </button>
          </div>
        </Form>
      )}
    </Formik>
  );
}
