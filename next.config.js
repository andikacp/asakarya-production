module.exports = {
  reactStrictMode: true,
  images: {
    domains: ['global-uploads.webflow.com', 'asakarya-testing.herokuapp.com', 'asakarya-api.herokuapp.com', 'i.ibb.co'],
  },
};
